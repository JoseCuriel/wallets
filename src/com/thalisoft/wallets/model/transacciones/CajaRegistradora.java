package com.thalisoft.wallets.model.transacciones;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import java.util.Date;

public class CajaRegistradora {

    private int idCaja;
    private Date FechaHoraApertura;
    private Date FechaHoraCierre;
    private int MontoBaseInicial;
    private int MontoAcumulaFinal;
    private Empleado cajeroApertura;
    private Empleado cajeroCierre;

    public CajaRegistradora() {
    }

    public int getIdCaja() {
        return idCaja;
    }

    public void setIdCaja(int idCaja) {
        this.idCaja = idCaja;
    }

    public Date getFechaHoraApertura() {
        return FechaHoraApertura;
    }

    public void setFechaHoraApertura(Date FechaHoraApertura) {
        this.FechaHoraApertura = FechaHoraApertura;
    }

    public Date getFechaHoraCierre() {
        return FechaHoraCierre;
    }

    public void setFechaHoraCierre(Date FechaHoraCierre) {
        this.FechaHoraCierre = FechaHoraCierre;
    }

    public int getMontoBaseInicial() {
        return MontoBaseInicial;
    }

    public void setMontoBaseInicial(int MontoBaseInicial) {
        this.MontoBaseInicial = MontoBaseInicial;
    }

    public int getMontoAcumulaFinal() {
        return MontoAcumulaFinal;
    }

    public void setMontoAcumulaFinal(int MontoAcumulaFinal) {
        this.MontoAcumulaFinal = MontoAcumulaFinal;
    }

    public Empleado getCajeroApertura() {
        return cajeroApertura;
    }

    public void setCajeroApertura(Empleado cajeroApertura) {
        this.cajeroApertura = cajeroApertura;
    }

    public Empleado getCajeroCierre() {
        return cajeroCierre;
    }

    public void setCajeroCierre(Empleado cajeroCierre) {
        this.cajeroCierre = cajeroCierre;
    }
}
