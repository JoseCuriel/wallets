package com.thalisoft.wallets.model.transacciones;


public class DenominacionMonetaria {
    
    private int idDenominacionMoneda;
    private String descripcionMoneda;
    private String imagenMoneda;
    private String tipoDenominacion;

    public DenominacionMonetaria() {
    }


    public int getIdDenominacionMoneda() {
        return idDenominacionMoneda;
    }

    public void setIdDenominacionMoneda(int idDenominacionMoneda) {
        this.idDenominacionMoneda = idDenominacionMoneda;
    }

    public String getDescripcionMoneda() {
        return descripcionMoneda;
    }

    public void setDescripcionMoneda(String descripcionMoneda) {
        this.descripcionMoneda = descripcionMoneda;
    }

    public String getImagenMoneda() {
        return imagenMoneda;
    }

    public void setImagenMoneda(String imagenMoneda) {
        this.imagenMoneda = imagenMoneda;
    }

    public String getTipoDenominacion() {
        return tipoDenominacion;
    }

    public void setTipoDenominacion(String tipoDenominacion) {
        this.tipoDenominacion = tipoDenominacion;
    }
    
    
}
