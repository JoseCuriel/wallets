package com.thalisoft.wallets.model.transacciones;

import com.thalisoft.wallets.util.database;
import java.util.ArrayList;
import java.util.List;

public class CajaRegistradoraDao extends database {

    public List<DenominacionMonetaria> LISTADO_DENOMINACION_MONETARIA(String key) {
        List<DenominacionMonetaria> listDenominacion = new ArrayList<>();
        DenominacionMonetaria dm;
        Object[][] rs = select("DENOMINACIONES_MONETARIAS", "denominaciones_monetarias.ID_DENOMINACION_MONEDA,\n"
                + "denominaciones_monetarias.DESCRIPCION_MONEDA,\n"
                + "denominaciones_monetarias.IMAGEN_DENOMINACION,\n"
                + "denominaciones_monetarias.TIPO_DENOMINACION", "denominaciones_monetarias.TIPO_DENOMINACION = '" + key + "'");
        if (rs.length > 0) {
            for (Object[] r : rs) {
                dm = new DenominacionMonetaria();
                dm.setIdDenominacionMoneda(Integer.parseInt(r[0].toString()));
                dm.setDescripcionMoneda(r[1].toString());
                dm.setImagenMoneda(r[2].toString());
                dm.setTipoDenominacion(r[3].toString());
                listDenominacion.add(dm);
            }
        }
        return listDenominacion;
    }

    public DenominacionMonetaria CONSULTA_DENOMINACION_MONETARIA(Object key) {
        DenominacionMonetaria dm = null;
        Object[][] rs = select("DENOMINACIONES_MONETARIAS", "denominaciones_monetarias.ID_DENOMINACION_MONEDA,\n"
                + "denominaciones_monetarias.DESCRIPCION_MONEDA,\n"
                + "denominaciones_monetarias.IMAGEN_DENOMINACION,\n"
                + "denominaciones_monetarias.TIPO_DENOMINACION", "denominaciones_monetarias.DESCRIPCION_MONEDA = '" + key + "'");
        if (rs.length > 0) {
            for (Object[] r : rs) {
                dm = new DenominacionMonetaria();
                dm.setIdDenominacionMoneda(Integer.parseInt(r[0].toString()));
                dm.setDescripcionMoneda(r[1].toString());
                dm.setImagenMoneda(r[2].toString());
                dm.setTipoDenominacion(r[3].toString());

            }
        }
        return dm;
    }

}
