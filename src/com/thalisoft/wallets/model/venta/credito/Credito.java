package com.thalisoft.wallets.model.venta.credito;

import com.thalisoft.wallets.model.maestros.cliente.Cliente;
import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import java.util.Date;

/**
 *
 * @author ThaliSoft
 */
public class Credito {

    private int numeroCredito;
    private Date fechaEmision;
    private Date fechaFinal;
    private int cantidadCuota;
    private String modalidadPago;
    private int totalVrCredito;
    private int totalVrCuotaInicial;
    private int totalVrSaldo;
    private int totalVrComision;
    private Date fechaHoraRegistro;
    private EstadoCredito estadoCredito;
    private Cliente cliente;
    private Empleado Cobrador;
    private Empleado empleadoRegistra;

    public int getNumeroCredito() {
        return numeroCredito;
    }

    public void setNumeroCredito(int numeroCredito) {
        this.numeroCredito = numeroCredito;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public int getCantidadCuota() {
        return cantidadCuota;
    }

    public void setCantidadCuota(int cantidadCuota) {
        this.cantidadCuota = cantidadCuota;
    }

    public String getModalidadPago() {
        return modalidadPago;
    }

    public void setModalidadPago(String modalidadPago) {
        this.modalidadPago = modalidadPago;
    }

    public int getTotalVrCredito() {
        return totalVrCredito;
    }

    public void setTotalVrCredito(int totalVrCredito) {
        this.totalVrCredito = totalVrCredito;
    }

    public int getTotalVrCuotaInicial() {
        return totalVrCuotaInicial;
    }

    public void setTotalVrCuotaInicial(int totalVrCuotaInicial) {
        this.totalVrCuotaInicial = totalVrCuotaInicial;
    }

    public int getTotalVrSaldo() {
        return totalVrSaldo;
    }

    public void setTotalVrSaldo(int totalVrSaldo) {
        this.totalVrSaldo = totalVrSaldo;
    }

    public int getTotalVrComision() {
        return totalVrComision;
    }

    public void setTotalVrComision(int totalVrComision) {
        this.totalVrComision = totalVrComision;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public EstadoCredito getEstadoCredito() {
        return estadoCredito;
    }

    public void setEstadoCredito(EstadoCredito estadoCredito) {
        this.estadoCredito = estadoCredito;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Empleado getCobrador() {
        return Cobrador;
    }

    public void setCobrador(Empleado Cobrador) {
        this.Cobrador = Cobrador;
    }

    public Empleado getEmpleadoRegistra() {
        return empleadoRegistra;
    }

    public void setEmpleadoRegistra(Empleado empleadoRegistra) {
        this.empleadoRegistra = empleadoRegistra;
    }

}
