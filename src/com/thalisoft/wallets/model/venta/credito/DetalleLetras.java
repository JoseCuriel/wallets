package com.thalisoft.wallets.model.venta.credito;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import java.util.Date;

/**
 *
 * @author ThaliSoft
 */
public class DetalleLetras {

    private int idDetalleLetra;
    private int numeroLetra;
    private Date fechaPago;
    private int totalVrLetra;
    private int totalVrSaldo;
    private int totalVrComision;
    private Date fechaHoraRegistro;
    private Credito credito;
    private Empleado empleadoRegistra;
    private int estadoLetra;

    public int getIdDetalleLetra() {
        return idDetalleLetra;
    }

    public void setIdDetalleLetra(int idDetalleLetra) {
        this.idDetalleLetra = idDetalleLetra;
    }

    public int getNumeroLetra() {
        return numeroLetra;
    }

    public void setNumeroLetra(int numeroLetra) {
        this.numeroLetra = numeroLetra;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public int getTotalVrLetra() {
        return totalVrLetra;
    }

    public void setTotalVrLetra(int totalVrLetra) {
        this.totalVrLetra = totalVrLetra;
    }

    public int getTotalVrSaldo() {
        return totalVrSaldo;
    }

    public void setTotalVrSaldo(int totalVrSaldo) {
        this.totalVrSaldo = totalVrSaldo;
    }

    public int getTotalVrComision() {
        return totalVrComision;
    }

    public void setTotalVrComision(int totalVrComision) {
        this.totalVrComision = totalVrComision;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Credito getCredito() {
        return credito;
    }

    public void setCredito(Credito credito) {
        this.credito = credito;
    }

    public Empleado getEmpleadoRegistra() {
        return empleadoRegistra;
    }

    public void setEmpleadoRegistra(Empleado empleadoRegistra) {
        this.empleadoRegistra = empleadoRegistra;
    }

    public int getEstadoLetra() {
        return estadoLetra;
    }

    public void setEstadoLetra(int estadoLetra) {
        this.estadoLetra = estadoLetra;
    }

}
