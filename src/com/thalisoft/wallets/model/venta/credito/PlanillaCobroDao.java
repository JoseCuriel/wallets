package com.thalisoft.wallets.model.venta.credito;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.database;

/**
 *
 * @author ThaliSoft
 */
public class PlanillaCobroDao extends database {

    public boolean CRUD_PLANILLA_COBRO(Object[] values) {
        return EJECUTAR_SP("CRUD_PLANILLA_COBRO", values);
    }

    public boolean CREATE_DETALLE_PLANILLA_COBRO(Object[] values) {
        return EJECUTAR_SP("CREATE_DETALLE_PLANILLA_COBRO", values);
    }

    public PlanillaCobro CONSULTA_PLANILLA_COBRO(Object key) {
        PlanillaCobro planillaCobro = null;
        Object[][] rs = SELECT_SP("SELECT_PLANILLA_COBRO", key);
        if (rs.length > 0) {
            planillaCobro = new PlanillaCobro();
            planillaCobro.setNumPlanilla(Integer.parseInt(rs[0][0].toString()));
            planillaCobro.setFechaDespacho(DateUtil.getDateTime(rs[0][1]));
            planillaCobro.setGestorCobro(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][2]));
            planillaCobro.setFechaHoraRegistro(DateUtil.getDateTime(rs[0][3]));
            planillaCobro.setEmpleadoRegistra(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][4]));
        }
        return planillaCobro;
    }

    public Object[][] DETALLE_PLANILLA_COBRO(Object key) {
        return SELECT_SP("SELECT_PLANILLA_COBRO", key);
    }

    public String NUMERO_PLANILLA_COBRO() {
        return select("planillacobros", "AGREGAR_CEROS_LEFT(ifnull(max(planillacobros.idPlanillaCobro) + 1, 1))", null)[0][0].toString();
    }

    public boolean ELIMINAR_CLIENTE_PLANILLA(Object key) {
        return Delete("detalle_planilla_cobros", "iddetalleplanilla = " + key + "");
    }

    public boolean ELIMINAR_PLANILLA_COBRO(Object key) {
        return Delete("planillacobros", "idplanillaCobro = " + key + "");
    }

    public boolean CLIENTE_PLANILLA_COBRO(String idCliente, String numPlanilla) {
        Object[][] rs = select("detalle_planilla_cobros", "detalle_planilla_cobros.cliente", "detalle_planilla_cobros.planilla = " + numPlanilla + " "
                + "AND detalle_planilla_cobros.cliente = " + idCliente + "");
        return rs.length <= 0;
    }

    public void CONFIRMAR_VERIFICACION_ARTICULO(Object valueAt) {
        update("detalle_planilla_cobros", "verificado = 1", "iddetalleplanilla = "+valueAt);
    }
}

/*SELECT  FROM
 detalle_planilla_cobros
 WHERE
 detalle_planilla_cobros.planilla = 1 AND
 detalle_planilla_cobros.cliente = 1
 */
