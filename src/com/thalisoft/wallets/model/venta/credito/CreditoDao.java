package com.thalisoft.wallets.model.venta.credito;

import com.thalisoft.wallets.model.maestros.cliente.ClienteDao;
import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.database;

/**
 *
 * @author ThaliSoft
 */
public class CreditoDao extends database {

    Edicion edicion = new Edicion();

    public boolean CRUD_CREDITO(Object[] values) {
        return EJECUTAR_SP("CRUD_CREDITO", values);
    }

    public boolean CREATE_DETALLE_CREDITO(Object[] values) {
        return EJECUTAR_SP("CREATE_DETALLE_CREDITO", values);
    }

    public boolean CREATE_PLAN_PAGO_LETRA(Object[] values) {
        return EJECUTAR_SP("CREATE_PLAN_PAGO_LETRA", values);
    }

    public boolean BORRAR_PLAN_PAGO_CREDITO(Object key) {
        return Delete("detalleletras", "detalleletras.credito = " + key + "");
    }

    public boolean BORRAR_DETALLE_CREDITO(Object key) {
        return Delete("detallecreditos", "detallecreditos.iddetalleCreditos = " + key + "");
    }

    public Credito CONSULTA_CREDITO(Object key) {
        Credito credito = null;
        Object[][] rs = SELECT_SP("SELECT_CREDITO", key);
        if (rs.length > 0) {
            credito = new Credito();
            credito.setNumeroCredito(edicion.toNumeroEntero(rs[0][0].toString()));
            credito.setFechaEmision(DateUtil.getDateTime(rs[0][1]));
            credito.setFechaFinal(DateUtil.getDateTime(rs[0][2]));
            credito.setCantidadCuota(edicion.toNumeroEntero(rs[0][3].toString()));
            credito.setModalidadPago(rs[0][4].toString());
            credito.setTotalVrCredito(edicion.toNumeroEntero(rs[0][5].toString()));
            credito.setTotalVrCuotaInicial(edicion.toNumeroEntero(rs[0][6].toString()));
            credito.setTotalVrSaldo(edicion.toNumeroEntero(rs[0][7].toString()));
            credito.setTotalVrComision(edicion.toNumeroEntero(rs[0][8].toString()));
            credito.setFechaHoraRegistro(DateUtil.getDateTime(rs[0][9]));
            credito.setEstadoCredito(SELECT_ESTADO_CREDITO("4," + rs[0][10]));
            credito.setCliente(new ClienteDao().CONSULTAR_CLIENTE("1," + rs[0][11]));
            credito.setCobrador(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][12]));
            credito.setEmpleadoRegistra(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][13]));
        }
        return credito;
    }

    public EstadoCredito SELECT_ESTADO_CREDITO(Object key) {
        EstadoCredito estadoCredito = null;
        Object[][] rs = SELECT_SP("SELECT_CREDITO", key);
        if (rs.length > 0) {
            estadoCredito = new EstadoCredito();
            estadoCredito.setIdEstadoCredito(edicion.toNumeroEntero(rs[0][0].toString()));
            estadoCredito.setDescripcion(rs[0][1].toString());
        }
        return estadoCredito;
    }

    public Object[][] CREDITOS_CLIENTE(Object key) {
        return SELECT_SP("SELECT_CREDITO", key);
    }

    public Object[][] DETALLE_CREDITO(Object key) {
        return SELECT_SP("SELECT_CREDITO", key);
    }

    public String NUMERO_CREDITO() {
        return SELECT_SP("SELECT_CREDITO", "1,1")[0][0].toString();
    }

    public Object[][] PLAN_DE_PAGO_CREDITO(Object key) {
        return SELECT_SP("SELECT_CREDITO", key);
    }

    public Object[][] RELACION_DE_CREDITOS(Object key) {
        return SELECT_SP("SELECT_ESTADISTICAS_CREDITOS", key);
    }

    public DetalleLetras CONSULTA_LETRA_CREDITO(Object key) {
        DetalleLetras letras = null;
        Object[][] rs = SELECT_SP("SELECT_CREDITO", key);
        if (rs.length > 0) {
            letras = new DetalleLetras();
            letras.setIdDetalleLetra(edicion.toNumeroEntero(rs[0][0].toString()));
            letras.setNumeroLetra(edicion.toNumeroEntero(rs[0][1].toString()));
            letras.setFechaPago(DateUtil.getDateTime(rs[0][2]));
            letras.setTotalVrLetra(edicion.toNumeroEntero(rs[0][3].toString()));
            letras.setTotalVrSaldo(edicion.toNumeroEntero(rs[0][4].toString()));
            letras.setTotalVrComision(edicion.toNumeroEntero(rs[0][5].toString()));
            letras.setFechaHoraRegistro(DateUtil.getDateTime(rs[0][6]));
            letras.setCredito(CONSULTA_CREDITO("2," + rs[0][7]));
            letras.setEmpleadoRegistra(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][8]));
            letras.setEstadoLetra(edicion.toNumeroEntero(rs[0][9].toString()));
        }
        return letras;
    }

    public Object[][] RELACION_PAGOS_CREDITO(Object values) {
        return SELECT_SP("SELECT_ESTADISTICAS_CREDITOS", values);
    }

    public boolean ESTADO_RETIRAR_ARTICULO_DEL_CREDITO(Object key) {
        return update("creditos", "estadosCredito = 6", "idcreditos = " + key);
    }

    public boolean MARCAR_OBSERVACION_CLIENTE_RETIRA_CREDITO(Object textObservacion, String idCliente) {
        return update("clientes", "observaciones = upper(" + textObservacion + "), perfilCliente = 1" , " identificacion = " + idCliente + "");
    }
    
    public boolean CREATE_CONSECUTIVO_CREDITO(Object[] values){
        return EJECUTAR_SP("create_consecutivo_credito", values);
    }
}
