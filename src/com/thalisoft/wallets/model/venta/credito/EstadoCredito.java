package com.thalisoft.wallets.model.venta.credito;

/**
 *
 * @author ThaliSoft
 */
public class EstadoCredito {
    private int idEstadoCredito;
    private String descripcion;

    public int getIdEstadoCredito() {
        return idEstadoCredito;
    }

    public void setIdEstadoCredito(int idEstadoCredito) {
        this.idEstadoCredito = idEstadoCredito;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
