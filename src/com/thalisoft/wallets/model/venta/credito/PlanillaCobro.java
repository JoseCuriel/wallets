package com.thalisoft.wallets.model.venta.credito;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import java.util.Date;

/**
 *
 * @author ThaliSoft
 */
public class PlanillaCobro {
    private int numPlanilla;
    private Date fechaDespacho;
    private Empleado gestorCobro;
    private Date fechaHoraRegistro;
    private Empleado empleadoRegistra;

    public int getNumPlanilla() {
        return numPlanilla;
    }

    public void setNumPlanilla(int numPlanilla) {
        this.numPlanilla = numPlanilla;
    }

    public Date getFechaDespacho() {
        return fechaDespacho;
    }

    public void setFechaDespacho(Date fechaDespacho) {
        this.fechaDespacho = fechaDespacho;
    }

    public Empleado getGestorCobro() {
        return gestorCobro;
    }

    public void setGestorCobro(Empleado gestorCobro) {
        this.gestorCobro = gestorCobro;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Empleado getEmpleadoRegistra() {
        return empleadoRegistra;
    }

    public void setEmpleadoRegistra(Empleado empleadoRegistra) {
        this.empleadoRegistra = empleadoRegistra;
    }
    
    
}
