package com.thalisoft.wallets.model.cartera.pago;

import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.database;

/**
 *
 * @author ThaliSoft
 */
public class PagosClienteDao extends database {

    public boolean CREATE_PAGO_CLIENTE(Object[] values) {
        return EJECUTAR_SP("CRUD_PAGOS_CREDITO", values);
    }

    public PagosCliente SELECT_PAGOS_CLIENTE(Object key) {
        PagosCliente pagosCliente = null;
        Edicion edicion = new Edicion();
        Object[][] rs = SELECT_SP("SELECT_PAGOS_CREDITO", key);
        if (rs.length > 0) {
            pagosCliente = new PagosCliente();
            pagosCliente.setIdPagosCliente(edicion.toNumeroEntero(rs[0][0].toString()));
            pagosCliente.setFechaEmision(DateUtil.getDateTime(rs[0][1]));
            pagosCliente.setTotalVrLetra(edicion.toNumeroEntero(rs[0][2].toString()));
            pagosCliente.setTotalVrAbono(edicion.toNumeroEntero(rs[0][3].toString()));
            pagosCliente.setTotalVrSaldo(edicion.toNumeroEntero(rs[0][4].toString()));
            pagosCliente.setTotalVrComision(edicion.toNumeroEntero(rs[0][5].toString()));
            pagosCliente.setIdLetra(edicion.toNumeroEntero(rs[0][6].toString()));
            pagosCliente.setFechaHoraRegistro(DateUtil.getDateTime(rs[0][7]));
            pagosCliente.setEmpleadoRegistra(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][8]));
            pagosCliente.setRecaudador(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][9]));
        }
        return pagosCliente;
    }

    public Object[][] DETALLE_PAGOS_CREDITO(Object key) {
        return SELECT_SP("SELECT_PAGOS_CREDITO", key);
    }

    public String NUMERO_PAGOS_CREDITO() {
        return SELECT_SP("SELECT_PAGOS_CREDITO", "1,1")[0][0].toString();
    }

    public Object[][] DETALLE_LIQUIDACION_PAGOS_CREDITO(Object key) {
        return SELECT_SP("SELECT_ESTADISTICAS_CREDITOS", key);
    }

    public void LIQUIDAR_PAGOS_CREDITO(Object key) {
        SELECT_SP("SELECT_ESTADISTICAS_CREDITOS", key);
    }

    public Object[][] DETALLE_RECAUDO_PAGOS_CREDITO(Object key) {
        return SELECT_SP("SELECT_ESTADISTICAS_CREDITOS", key);
    }
    
    public Object[][] LETRAS_PENDIENTES_CREDITO(Object ID_KEY) {
        return SELECT_SP("SELECT_CREDITO", "10, " + ID_KEY);
    }
}
