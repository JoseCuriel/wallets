package com.thalisoft.wallets.model.cartera.pago;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import java.util.Date;

/**
 *
 * @author ThaliSoft
 */
public class PagosCliente {
    private int idPagosCliente;
    private Date fechaEmision;
    private int totalVrLetra;
    private int totalVrAbono;
    private int totalVrSaldo;
    private int totalVrComision;
    private Date fechaHoraRegistro;
    private int idLetra;
    private Empleado empleadoRegistra;
    private Empleado recaudador;

    public int getIdPagosCliente() {
        return idPagosCliente;
    }

    public void setIdPagosCliente(int idPagosCliente) {
        this.idPagosCliente = idPagosCliente;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public int getTotalVrLetra() {
        return totalVrLetra;
    }

    public void setTotalVrLetra(int totalVrLetra) {
        this.totalVrLetra = totalVrLetra;
    }

    public int getTotalVrAbono() {
        return totalVrAbono;
    }

    public void setTotalVrAbono(int totalVrAbono) {
        this.totalVrAbono = totalVrAbono;
    }

    public int getTotalVrSaldo() {
        return totalVrSaldo;
    }

    public void setTotalVrSaldo(int totalVrSaldo) {
        this.totalVrSaldo = totalVrSaldo;
    }

    public int getTotalVrComision() {
        return totalVrComision;
    }

    public void setTotalVrComision(int totalVrComision) {
        this.totalVrComision = totalVrComision;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public int getIdLetra() {
        return idLetra;
    }

    public void setIdLetra(int idLetra) {
        this.idLetra = idLetra;
    }

    public Empleado getEmpleadoRegistra() {
        return empleadoRegistra;
    }

    public void setEmpleadoRegistra(Empleado empleadoRegistra) {
        this.empleadoRegistra = empleadoRegistra;
    }

    public Empleado getRecaudador() {
        return recaudador;
    }

    public void setRecaudador(Empleado recaudador) {
        this.recaudador = recaudador;
    }
    
    
}
