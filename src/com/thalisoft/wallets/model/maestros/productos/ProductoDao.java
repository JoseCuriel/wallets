package com.thalisoft.wallets.model.maestros.productos;

import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.database;
import java.util.ArrayList;
import java.util.List;

public class ProductoDao extends database {

    Edicion edicion = new Edicion();
    EmpleadoDao empleado;

    public boolean EJECUTAR_CRUD(Object[] key) {
        return EJECUTAR_SP("CRUD_PRODUCTO", key);
    }

    public Producto READ_PRODUCTO(Object key) {
        Producto producto = null;
        Object[][] rs = SELECT_SP("SELECT_PRODUCTO", key);
        if (rs.length > 0) {
            empleado = new EmpleadoDao();
            producto = new Producto();
            producto.setId_producto(edicion.toNumeroEntero(rs[0][0].toString()));
            producto.setReferencia(rs[0][1].toString());
            producto.setDescripcion(rs[0][2].toString());
            producto.setStrock(edicion.toNumeroEntero(rs[0][3].toString()));
            producto.setCosto_und(edicion.toNumeroEntero(rs[0][4].toString()));
            producto.setPrecio_venta(edicion.toNumeroEntero(rs[0][5].toString()));
            producto.setUtilidad(edicion.toNumeroEntero(rs[0][6].toString()));
            producto.setCuotaInicial(edicion.toNumeroEntero(rs[0][7].toString()));
            producto.setModelo(rs[0][8].toString());
            producto.setColor(rs[0][9].toString());
            producto.setMarcar(rs[0][10].toString());
            producto.setProveedor(edicion.toNumeroEntero(rs[0][11].toString()));
            producto.setImagen(rs[0][12].toString());
            producto.setFechahoraingreso(DateUtil.getDateTime(rs[0][13]));
            producto.setEmpleado(empleado.CONSULTAR_EMPLEADO(rs[0][14]));
            producto.setCategoria(new CategoriaDao().SELECT_SUBCATEGORIA("3, "+rs[0][15]));
        }
        return producto;
    }

    public Object[][] LISTADO_DE_PRODUCTOS() {
        Object parametro = 2 + "," + 0;
        Object[][] rs = SELECT_SP("SELECT_PRODUCTO", parametro);
        if (rs.length > 0) {
            return rs;
        }
        return null;
    }

    public List<Producto> LISTA_PRODUCTOS() {
        List<Producto> list = null;
        Producto producto;
        Object[][] rs = LISTADO_DE_PRODUCTOS();
        if (rs.length > 0) {
            list = new ArrayList<>();
            for (Object[] r : rs) {
                empleado = new EmpleadoDao();
                producto = new Producto();
                producto.setId_producto(edicion.toNumeroEntero(r[0].toString()));
                producto.setReferencia(r[1].toString());
                producto.setDescripcion(r[2].toString());
                producto.setStrock(edicion.toNumeroEntero(r[3].toString()));
                producto.setCosto_und(edicion.toNumeroEntero(r[4].toString()));
                producto.setPrecio_venta(edicion.toNumeroEntero(r[5].toString()));
                producto.setUtilidad(edicion.toNumeroEntero(r[6].toString()));
                producto.setCuotaInicial(edicion.toNumeroEntero(r[7].toString()));
                producto.setModelo(r[8].toString());
                producto.setColor(r[9].toString());
                producto.setMarcar(r[10].toString());
                producto.setProveedor(edicion.toNumeroEntero(r[11].toString()));
                producto.setImagen(r[12].toString());
                producto.setFechahoraingreso(DateUtil.getDateTime(r[13]));
                producto.setEmpleado(empleado.CONSULTAR_EMPLEADO(r[14]));
                producto.setCategoria(new CategoriaDao().SELECT_SUBCATEGORIA("3, " + r[15]));
                list.add(producto);
            }

        }
        return list;
    }

    public String NUMERO_FICHA_PRODUCTO() {
        Object parametro = 3 + "," + 0;
        Object[][] rs = SELECT_SP("SELECT_PRODUCTO", parametro);
        if (rs.length > 0) {
            return rs[0][0].toString();
        }
        return null;
    }
}
