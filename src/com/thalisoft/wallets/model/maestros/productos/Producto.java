package com.thalisoft.wallets.model.maestros.productos;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import java.util.Date;

public class Producto {

    private int id_producto;
    private String referencia;
    private String descripcion;
    private int strock;
    private int costo_und;
    private int precio_venta;
    private int utilidad;
    private int cuotaInicial;
    private String modelo;
    private String color;
    private String marcar;
    private int proveedor;
    private String imagen;
    private SubCategoria categoria;
    private Date fechahoraingreso;
    private Empleado empleado;

    

    public Producto() {
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStrock() {
        return strock;
    }

    public void setStrock(int strock) {
        this.strock = strock;
    }

    public int getCosto_und() {
        return costo_und;
    }

    public void setCosto_und(int costo_und) {
        this.costo_und = costo_und;
    }

    public int getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(int precio_venta) {
        this.precio_venta = precio_venta;
    }

    public int getUtilidad() {
        return utilidad;
    }

    public void setUtilidad(int utilidad) {
        this.utilidad = utilidad;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Date getFechahoraingreso() {
        return fechahoraingreso;
    }

    public void setFechahoraingreso(Date fechahoraingreso) {
        this.fechahoraingreso = fechahoraingreso;
    }

    public int getCuotaInicial() {
        return cuotaInicial;
    }

    public void setCuotaInicial(int cuotaInicial) {
        this.cuotaInicial = cuotaInicial;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarcar() {
        return marcar;
    }

    public void setMarcar(String marcar) {
        this.marcar = marcar;
    }

    public int getProveedor() {
        return proveedor;
    }

    public void setProveedor(int proveedor) {
        this.proveedor = proveedor;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public SubCategoria getCategoria() {
        return categoria;
    }

    public void setCategoria(SubCategoria categoria) {
        this.categoria = categoria;
    }

}
