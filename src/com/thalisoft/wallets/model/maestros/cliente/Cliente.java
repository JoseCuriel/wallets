package com.thalisoft.wallets.model.maestros.cliente;


import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import java.io.Serializable;
import java.util.Date;

public class Cliente implements Serializable {

    private int idcliente;
    private String identificacion;
    private String nombres;
    private String apellidos;
    private String telefono1;
    private String telefono2;
    private String direccion1;
    private String direccion2;
    private String email;
    private String nombreReferencia;
    private String telefonoReferencia;
    private String nombreConyugue;
    private String telefonoConyugue;
    private Empleado empleadoRegistra;
    private Date fechahoraingreso;
    private Empleado empleadoModifica;
    private Date fechahoraModifica;
    private String ocupacionCliente;
    private String tipoVivienda;

    public Cliente(int idcliente, String identificacion, String nombres, String apellidos, String telefono1, String telefono2, String direccion1, String direccion2, String email, String nombreReferencia, String telefonoReferencia, String nombreConyugue, String telefonoConyugue, Empleado empleadoRegistra, Date fechahoraingreso, Empleado empleadoModifica, Date fechahoraModifica, String ocupacionCliente, String tipoVivienda) {
        this.idcliente = idcliente;
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.direccion1 = direccion1;
        this.direccion2 = direccion2;
        this.email = email;
        this.nombreReferencia = nombreReferencia;
        this.telefonoReferencia = telefonoReferencia;
        this.nombreConyugue = nombreConyugue;
        this.telefonoConyugue = telefonoConyugue;
        this.empleadoRegistra = empleadoRegistra;
        this.fechahoraingreso = fechahoraingreso;
        this.empleadoModifica = empleadoModifica;
        this.fechahoraModifica = fechahoraModifica;
        this.ocupacionCliente = ocupacionCliente;
        this.tipoVivienda = tipoVivienda;
    }
    
    public Cliente() {
    }
    
    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

   
    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDireccion1() {
        return direccion1;
    }

    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }

    public String getDireccion2() {
        return direccion2;
    }

    public void setDireccion2(String direccion2) {
        this.direccion2 = direccion2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Empleado getEmpleadoRegistra() {
        return empleadoRegistra;
    }

    public void setEmpleadoRegistra(Empleado empleadoRegistra) {
        this.empleadoRegistra = empleadoRegistra;
    }

    public Date getFechahoraingreso() {
        return fechahoraingreso;
    }

    public void setFechahoraingreso(Date fechahoraingreso) {
        this.fechahoraingreso = fechahoraingreso;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombreReferencia() {
        return nombreReferencia;
    }

    public void setNombreReferencia(String nombreReferencia) {
        this.nombreReferencia = nombreReferencia;
    }

    public String getTelefonoReferencia() {
        return telefonoReferencia;
    }

    public void setTelefonoReferencia(String telefonoReferencia) {
        this.telefonoReferencia = telefonoReferencia;
    }

    public String getNombreConyugue() {
        return nombreConyugue;
    }

    public void setNombreConyugue(String nombreConyugue) {
        this.nombreConyugue = nombreConyugue;
    }

    public String getTelefonoConyugue() {
        return telefonoConyugue;
    }

    public void setTelefonoConyugue(String telefonoConyugue) {
        this.telefonoConyugue = telefonoConyugue;
    }

    public Empleado getEmpleadoModifica() {
        return empleadoModifica;
    }

    public void setEmpleadoModifica(Empleado empleadoModifica) {
        this.empleadoModifica = empleadoModifica;
    }

    public Date getFechahoraModifica() {
        return fechahoraModifica;
    }

    public void setFechahoraModifica(Date fechahoraModifica) {
        this.fechahoraModifica = fechahoraModifica;
    }

    public String getOcupacionCliente() {
        return ocupacionCliente;
    }

    public void setOcupacionCliente(String ocupacionCliente) {
        this.ocupacionCliente = ocupacionCliente;
    }

    public String getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

}
