package com.thalisoft.wallets.model.maestros.cliente;

import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.database;

public class ClienteDao extends database {

    Edicion edicion = new Edicion();
    EmpleadoDao EmployedDAO;

    public boolean CRUD_CLIENTE(Object[] key) {
        return EJECUTAR_SP("CRUD_CLIENTE", key);
    }

    public Cliente CONSULTAR_CLIENTE(Object key) {
        Cliente cliente = null;
        Object[][] rs = SELECT_SP("SELECT_CLIENTE", key);
        if (rs.length > 0) {
            EmployedDAO = new EmpleadoDao();
            cliente = new Cliente();
            cliente.setIdcliente(edicion.toNumeroEntero(rs[0][0].toString()));
            cliente.setNombres(rs[0][1].toString());
            cliente.setApellidos(rs[0][2].toString());
            cliente.setIdentificacion(rs[0][3].toString());
            cliente.setTelefono1(rs[0][4].toString());
            cliente.setTelefono2(rs[0][5].toString());
            cliente.setDireccion1(rs[0][6].toString());
            cliente.setDireccion2(rs[0][7].toString());
            cliente.setEmail(rs[0][8].toString());
            cliente.setNombreReferencia(rs[0][9].toString());
            cliente.setTelefonoReferencia(rs[0][10].toString());
            cliente.setNombreConyugue(rs[0][11].toString());
            cliente.setTelefonoConyugue(rs[0][12].toString());
            cliente.setFechahoraingreso(DateUtil.getDateTime(rs[0][13]));
            cliente.setEmpleadoRegistra(EmployedDAO.CONSULTAR_EMPLEADO(rs[0][14]));
            cliente.setEmpleadoModifica(EmployedDAO.CONSULTAR_EMPLEADO(rs[0][15]));
            cliente.setFechahoraModifica(DateUtil.getDateTime(rs[0][16]));
            cliente.setOcupacionCliente(rs[0][17].toString());
            cliente.setTipoVivienda(rs[0][18].toString());
        }

        return cliente;
    }

    public Object[][] LISTADO_CLIENTES() {
        Object parametros = 2 + "," + 0 + "";
        Object[][] rs = SELECT_SP("SELECT_CLIENTE", parametros);
        return rs;
    }

    public void LISTADO_CORREOS_CLIENTES() {
        Object parametros = 4 + "," + 0 + "";
        SELECT_SP("SELECT_CLIENTE", parametros);
    }

    public String NUMERO_FICHA() {
        Object parametros = 3 + "," + 0 + "";
        Object[][] rs = SELECT_SP("SELECT_CLIENTE", parametros);
        return rs[0][0].toString();
    }
}
