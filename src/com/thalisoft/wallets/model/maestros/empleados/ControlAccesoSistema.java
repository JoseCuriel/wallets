package com.thalisoft.wallets.model.maestros.empleados;


public class ControlAccesoSistema {
    
   private int IdControlAcceso;
   private String DescripcionAcceso;
   private String NombreClase;

    public int getIdControlAcceso() {
        return IdControlAcceso;
    }

    public void setIdControlAcceso(int IdControlAcceso) {
        this.IdControlAcceso = IdControlAcceso;
    }

    public String getDescripcionAcceso() {
        return DescripcionAcceso;
    }

    public void setDescripcionAcceso(String DescripcionAcceso) {
        this.DescripcionAcceso = DescripcionAcceso;
    }

    public String getNombreClase() {
        return NombreClase;
    }

    public void setNombreClase(String NombreClase) {
        this.NombreClase = NombreClase;
    }
   
   
    
}
