package com.thalisoft.wallets.model.maestros.empleados.privilegios;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import java.util.Date;


public class PrivilegioEmpleado {
    
    private int idPrivilegioEmpleado;
    private ListadoPrivilegio privilegioSistema;
    private Empleado empleadoPrivilegio;
    private int estadoPrivilegio;
    private Date fechaHoraRegistro;
    private Empleado empleadoRegistra;
    private Date fechaHoraModifica;
    private Empleado empleadoModifica;
    private String columnaModifica;

    public int getIdPrivilegioEmpleado() {
        return idPrivilegioEmpleado;
    }

    public void setIdPrivilegioEmpleado(int idPrivilegioEmpleado) {
        this.idPrivilegioEmpleado = idPrivilegioEmpleado;
    }

    public ListadoPrivilegio getPrivilegioSistema() {
        return privilegioSistema;
    }

    public void setPrivilegioSistema(ListadoPrivilegio privilegioSistema) {
        this.privilegioSistema = privilegioSistema;
    }

    public Empleado getEmpleadoPrivilegio() {
        return empleadoPrivilegio;
    }

    public void setEmpleadoPrivilegio(Empleado empleadoPrivilegio) {
        this.empleadoPrivilegio = empleadoPrivilegio;
    }

    public int getEstadoPrivilegio() {
        return estadoPrivilegio;
    }

    public void setEstadoPrivilegio(int estadoPrivilegio) {
        this.estadoPrivilegio = estadoPrivilegio;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Empleado getEmpleadoRegistra() {
        return empleadoRegistra;
    }

    public void setEmpleadoRegistra(Empleado empleadoRegistra) {
        this.empleadoRegistra = empleadoRegistra;
    }

    public Date getFechaHoraModifica() {
        return fechaHoraModifica;
    }

    public void setFechaHoraModifica(Date fechaHoraModifica) {
        this.fechaHoraModifica = fechaHoraModifica;
    }

    public Empleado getEmpleadoModifica() {
        return empleadoModifica;
    }

    public void setEmpleadoModifica(Empleado empleadoModifica) {
        this.empleadoModifica = empleadoModifica;
    }

    public String getColumnaModifica() {
        return columnaModifica;
    }

    public void setColumnaModifica(String columnaModifica) {
        this.columnaModifica = columnaModifica;
    }
    
    
    
}
