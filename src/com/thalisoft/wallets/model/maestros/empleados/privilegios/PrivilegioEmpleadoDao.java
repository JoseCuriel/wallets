package com.thalisoft.wallets.model.maestros.empleados.privilegios;

import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.util.database;
import java.util.ArrayList;
import java.util.List;

public class PrivilegioEmpleadoDao extends database {

    public Object[][] CARGAR_LISTADO_PRIVILEGIOS_PREDETERMINADOS(Object IDEMPLEADO) {
        return SELECT_SP("SELECT_PRIVILEGIOS_SISTEMA", "2," + IDEMPLEADO);
    }

    public List<ListadoPrivilegio> LISTA_DE_PRIVILEGIOS_DEL_SISTEMA() {
        ListadoPrivilegio lp;
        List<ListadoPrivilegio> list = new ArrayList<>();
        Object[][] rs = SELECT_SP("SELECT_PRIVILEGIOS_SISTEMA", "1,0");
        if (rs.length > 0) {
            for (Object[] r : rs) {
                lp = new ListadoPrivilegio();
                lp.setIdPrivilegioSistema(Integer.parseInt(r[0].toString()));
                lp.setDescripcionPrivilegio(r[1].toString());
                lp.setClaseRelacionada(r[2].toString());
                lp.setCategoriaOperativa(r[3].toString());
                lp.setFechaHoraIngreso(DateUtil.getDateTime(r[4]));
                lp.setEmpleadoRegistra(new EmpleadoDao().CONSULTAR_EMPLEADO(r[5]));
                list.add(lp);
            }
        }
        return list;
    }

    public boolean PERMITIR_DENEGAR_PRIVILEGIO(int Permitir_Denegar, int idPrivilegio) {
        return update("privilegios_empleados", "ESTADOPRIVILEGIO = " + Permitir_Denegar + ","
                + "privilegios_empleados.fechahoramodifica = now(), "
                + "privilegios_empleados.empleadomodifica = " + Variables_Gloabales.EMPLEADO.getIdentificacion() + "",
                "idprivilegioempleado = " + idPrivilegio);
    }

    public boolean ELIMINAR_PRIVILEGIO_EMPLEADO(int IDPrivilegioEmpleado) {
        return Delete("privilegios_empleados", "idprivilegioempleado = " + IDPrivilegioEmpleado);
    }

    public boolean CREATE_PRIVILEGIO_EMPLEADO(Object[] dataValues) {
        return EJECUTAR_SP("CREATE_PRIVILEGIO_EMPLEADO", dataValues);
    }

    public boolean VERIFICAR_PRIVILEGIO_EMPLEADO(String clase, String IdEmpleado) {
        boolean verificacion = false;
        Object[][] rs = select("privilegios_empleados ,\n"
                + "listado_privilegios", "privilegios_empleados.estadoprivilegio",
                "privilegios_empleados.privilegio = listado_privilegios.IDPRIVILEGIO AND\n"
                + "listado_privilegios.CLASE_RELACIONADA = '"+clase+"' AND\n"
                + "privilegios_empleados.empleado = '"+IdEmpleado+"'");
        if (rs.length > 0) {
            if (Integer.parseInt(rs[0][0].toString()) == 1) {
                verificacion = true;
            }
        }
        return verificacion;
    }
}
