package com.thalisoft.wallets.model.maestros.empleados.privilegios;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import java.util.Date;


public class ListadoPrivilegio {
    private int idPrivilegioSistema;
    private String DescripcionPrivilegio;
    private String ClaseRelacionada;
    private String categoriaOperativa;
    private Date fechaHoraIngreso;
    private Empleado EmpleadoRegistra;

    public int getIdPrivilegioSistema() {
        return idPrivilegioSistema;
    }

    public void setIdPrivilegioSistema(int idPrivilegioSistema) {
        this.idPrivilegioSistema = idPrivilegioSistema;
    }

    public String getDescripcionPrivilegio() {
        return DescripcionPrivilegio;
    }

    public void setDescripcionPrivilegio(String DescripcionPrivilegio) {
        this.DescripcionPrivilegio = DescripcionPrivilegio;
    }

    public String getClaseRelacionada() {
        return ClaseRelacionada;
    }

    public void setClaseRelacionada(String ClaseRelacionada) {
        this.ClaseRelacionada = ClaseRelacionada;
    }

    public Date getFechaHoraIngreso() {
        return fechaHoraIngreso;
    }

    public void setFechaHoraIngreso(Date fechaHoraIngreso) {
        this.fechaHoraIngreso = fechaHoraIngreso;
    }

    public Empleado getEmpleadoRegistra() {
        return EmpleadoRegistra;
    }

    public void setEmpleadoRegistra(Empleado EmpleadoRegistra) {
        this.EmpleadoRegistra = EmpleadoRegistra;
    }

    public String getCategoriaOperativa() {
        return categoriaOperativa;
    }

    public void setCategoriaOperativa(String categoriaOperativa) {
        this.categoriaOperativa = categoriaOperativa;
    }
    
    
    
}
