package com.thalisoft.wallets.main.controller;

import com.thalisoft.wallets.model.maestros.empleados.privilegios.PrivilegioEmpleadoDao;
import com.thalisoft.wallets.util.Edicion;
import static com.thalisoft.wallets.util.Metodos.Obtener_Estado_Formulario;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.vista.informes.FormRelacionRecaudo;
import com.thalisoft.wallets.vista.maestros.cliente.FormCliente;
import com.thalisoft.wallets.vista.maestros.cliente.FormListarClientes;
import com.thalisoft.wallets.vista.maestros.empleado.FormEmpleado;
import com.thalisoft.wallets.vista.maestros.empleado.FormListarEmpleados;
import com.thalisoft.wallets.vista.maestros.producto.FormListaProductos;
import com.thalisoft.wallets.vista.main.Contenedor;
import com.thalisoft.wallets.vista.transaciones.FormCajaRegistradora;
import com.thalisoft.wallets.vista.venta.credito.FormCredito;
import com.thalisoft.wallets.vista.venta.credito.FormPlanillaCobroCredito;
import com.thalisoft.wallets.vista.venta.credito.pagos.FormConsultaLetraCliente;
import com.thalisoft.wallets.vista.venta.credito.pagos.FormPagoCredito;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDesktopPane;
import javax.swing.SwingWorker;

public class ControllerContenedor extends SwingWorker<Object, Object> {

    private static Contenedor contenedor;
    Edicion edicion = new Edicion();

    public static Contenedor getContenedor() {
        return contenedor;
    }

    //MENU MAESTROS
    FormEmpleado formEmpleado;
    FormListarEmpleados formListarEmpleados;
    FormCliente formCliente;
    FormListarClientes formListarClientes;
    FormListaProductos formListaProductos;

    //MENU VENTAS
    FormCredito formCredito;
    //MENU CARTERA
    FormConsultaLetraCliente formConsultaLetraCliente;
    FormPagoCredito formPagoCredito;
    FormPlanillaCobroCredito formPlanillaCobroCredito;

    //MENU INFORMES
    FormRelacionRecaudo formRelacionRecaudo;

    //MENU TRANSACCIONES
    FormCajaRegistradora formCajaRegistradora;
    
    private static JDesktopPane jDesktopPane1;

    PrivilegioEmpleadoDao privilegioAcceso = new PrivilegioEmpleadoDao();

    public ControllerContenedor() {
        Go();
    }

    public Contenedor Go() {

        if (contenedor == null) {

            contenedor = new Contenedor();
            contenedor.setVisible(true);

            contenedor.JM_INGRESO_EGRESO1.setVisible(false);
            contenedor.JM_FacturaVenta.setVisible(false);
            contenedor.JM_Relacionventas.setVisible(false);

            Obtener_Eventos_De_SubMenu(contenedor.JM_GESTIONARCREDITO);
            Obtener_Eventos_De_SubMenu(contenedor.JM_RELAC_RECAUDO);
            Obtener_Eventos_De_SubMenu(contenedor.JM_Relacionventas);
            Obtener_Eventos_De_SubMenu(contenedor.JM_Empleado);
            Obtener_Eventos_De_SubMenu(contenedor.JM_Clientes);
            Obtener_Eventos_De_SubMenu(contenedor.JM_Producto);
            Obtener_Eventos_De_SubMenu(contenedor.JM_FacturaVenta);
            Obtener_Eventos_De_SubMenu(contenedor.JM_Relacionventas);
            Obtener_Eventos_De_SubMenu(contenedor.JM_CREDITO_CLIENTE);
            Obtener_Eventos_De_SubMenu(contenedor.JM_PAGOS_CREDITO);
            Obtener_Eventos_De_SubMenu(contenedor.JM_PLANILLA_COBRO);
            Obtener_Eventos_De_SubMenu(contenedor.JM_APERTURA_CIERRE_CAJA);

            if (Variables_Gloabales.EMPLEADO != null) {
                contenedor.JM_Profile.setText(Variables_Gloabales.EMPLEADO.getNombres() + " "
                        + "" + Variables_Gloabales.EMPLEADO.getApellidos());
            }

        } else {
            contenedor.setVisible(true);
        }
        return contenedor;
    }

    private void Obtener_Eventos_De_SubMenu(javax.swing.JMenuItem SubMenu) {
        SubMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evento) {
                try {
                    Obtener_Resultado_Click(evento);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(ControllerContenedor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
    }

    private void Obtener_Resultado_Click(java.awt.event.ActionEvent evt) throws PropertyVetoException {
        switch (evt.getActionCommand()) {

            case "EMPLEADO":
                if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormListarEmpleados", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
                    if (Obtener_Estado_Formulario(this.formListarEmpleados, Contenedor.Panel_Contenedor)) {

                        formListarEmpleados = new FormListarEmpleados();
                        formListarEmpleados.show();
                        Contenedor.Panel_Contenedor.add(formListarEmpleados);
                        java.awt.Dimension Tamaño_Panel = Contenedor.Panel_Contenedor.getSize();
                        java.awt.Dimension Tamaño_InternalFrame = formListarEmpleados.getSize();
                        formListarEmpleados.setLocation((Tamaño_Panel.width - Tamaño_InternalFrame.width) / 2,
                                (Tamaño_Panel.height - Tamaño_InternalFrame.height) / 2);
                    } else {
                        formListarEmpleados.setIcon(false);
                    }
                } else {
                    edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                            + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
                }

                break;

            case "CLIENTES":
                if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormListarClientes", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
                    if (Obtener_Estado_Formulario(this.formListarClientes, Contenedor.Panel_Contenedor)) {

                        formListarClientes = new FormListarClientes();

                        formListarClientes.show();
                        Contenedor.Panel_Contenedor.add(formListarClientes);
                        java.awt.Dimension Tamaño_Panel = Contenedor.Panel_Contenedor.getSize();
                        java.awt.Dimension Tamaño_InternalFrame = formListarClientes.getSize();
                        formListarClientes.setLocation((Tamaño_Panel.width - Tamaño_InternalFrame.width) / 2,
                                (Tamaño_Panel.height - Tamaño_InternalFrame.height) / 2);
                    } else {
                        formListarClientes.setIcon(false);
                    }
                } else {
                    edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                            + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
                }
                break;

            case "PRODUCTOS":
                if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormListaProductos", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
                    if (Obtener_Estado_Formulario(this.formListaProductos, Contenedor.Panel_Contenedor)) {
                        formListaProductos = new FormListaProductos();
                        formListaProductos.show();
                        Contenedor.Panel_Contenedor.add(formListaProductos);
                        Edicion.CENTRAR_VENTANA(formListaProductos);
                    } else {
                        formListaProductos.setIcon(false);
                    }
                } else {
                    edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                            + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
                }
                break;

            case "Gestionar Credito":
                if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormCredito", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
                    if (Obtener_Estado_Formulario(this.formCredito, Contenedor.Panel_Contenedor)) {
                        formCredito = new FormCredito();
                        formCredito.show();
                        Contenedor.Panel_Contenedor.add(formCredito);
                        Edicion.CENTRAR_VENTANA(formCredito);
                    } else {
                        formCredito.setIcon(false);
                    }

                } else {
                    edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                            + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
                }

                break;

            case "CREDITOS CLIENTE":
                if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormConsultaLetraCliente", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
                    if (Obtener_Estado_Formulario(this.formConsultaLetraCliente, Contenedor.Panel_Contenedor)) {
                        formConsultaLetraCliente = new FormConsultaLetraCliente();
                        formConsultaLetraCliente.show();
                        Contenedor.Panel_Contenedor.add(formConsultaLetraCliente);
                        Edicion.CENTRAR_VENTANA(formConsultaLetraCliente);
                    } else {
                        formConsultaLetraCliente.setIcon(false);
                    }
                } else {
                    edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                            + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
                }

                break;

            case "PAGOS CREDITO":
                if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormPagoCredito", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
                    if (Obtener_Estado_Formulario(this.formPagoCredito, Contenedor.Panel_Contenedor)) {
                        formPagoCredito = new FormPagoCredito();
                        formPagoCredito.show();
                        Contenedor.Panel_Contenedor.add(formPagoCredito);
                        Edicion.CENTRAR_VENTANA(formPagoCredito);
                    } else {
                        formPagoCredito.setIcon(false);
                    }
                } else {
                    edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                            + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
                }
                break;

            case "formRecaudo":
                if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormRelacionRecaudo", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
                    if (Obtener_Estado_Formulario(this.formRelacionRecaudo, Contenedor.Panel_Contenedor)) {
                        formRelacionRecaudo = new FormRelacionRecaudo();
                        formRelacionRecaudo.show();
                        Contenedor.Panel_Contenedor.add(formRelacionRecaudo);
                    } else {
                        formRelacionRecaudo.setIcon(false);
                    }
                } else {
                    edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                            + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
                }
                break;

            case "FORMPLANILLACOBRO":
                if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormPlanillaCobroCredito", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
                    if (Obtener_Estado_Formulario(this.formPlanillaCobroCredito, Contenedor.Panel_Contenedor)) {
                        formPlanillaCobroCredito = new FormPlanillaCobroCredito();
                        formPlanillaCobroCredito.show();
                        Contenedor.Panel_Contenedor.add(formPlanillaCobroCredito);
                    } else {
                        formPlanillaCobroCredito.setIcon(false);
                    }
                } else {
                    edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                            + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
                }
                break;
                
                 case "formCajaRegistradora":
                if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormCajaRegistradora", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
                    if (Obtener_Estado_Formulario(this.formCajaRegistradora, Contenedor.Panel_Contenedor)) {
                        formCajaRegistradora = new FormCajaRegistradora();
                        formCajaRegistradora.show();
                        Contenedor.Panel_Contenedor.add(formCajaRegistradora);
                    } else {
                        formCajaRegistradora.setIcon(false);
                    }
                } else {
                    edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                            + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
                }
                break;

            case "Salir":
                System.exit(0);
        }

    }

    public static JDesktopPane getjDesktopPane1() {
        jDesktopPane1 = Contenedor.Panel_Contenedor;
        return jDesktopPane1;
    }

    @Override
    protected Object doInBackground() throws Exception {

        Go();

        return this;
    }

}
