package com.thalisoft.wallets.util.report;

import com.thalisoft.wallets.model.maestros.cliente.ClienteDao;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.database;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class Manager_Report extends database {

    static String RUTA_REPORTE = "C:\\ThaliSoftWalletsData\\reportes\\";
    public String NOMBRE_REPORTE;
    public String NOMBRE_DEL_ARCHIVO_EXPORTADO;
    public HashMap PARAMETROS = new HashMap();
    public String DIRECTORIO_POR_DEFECTO;
    JasperReport masterReport = null;

    Edicion edicion = new Edicion();

    public Manager_Report() {
    }

    
     public void GESTIONAR_CREDITO(Object key) {
        String reporte = "credito cliente.jasper";
        try {
            masterReport = (JasperReport) JRLoader.loadObject(RUTA_REPORTE.concat(reporte));
            PARAMETROS.put("ID_CREDITO", key);
            edicion.Lanzador_rpt_vista_previa(masterReport, PARAMETROS);
        } catch (Exception j) {
            edicion.mensajes(3, "NO SE PUEDE LANZAR el reporte " + reporte + ".\n" + j);
            System.out.println("Mensaje de Error:" + j);
        }
    }
     
       public void COMPROBANTE_DE_PAGO_CREDITO(Object key) {
        String reporte = "Comprobante de Pago Credito.jasper";
        try {
            masterReport = (JasperReport) JRLoader.loadObject(RUTA_REPORTE.concat(reporte));
            PARAMETROS.put("ID_COMPROBANTE", key);
            edicion.Lanzador_rpt_vista_previa(masterReport, PARAMETROS);
        } catch (Exception j) {
            edicion.mensajes(3, "NO SE PUEDE LANZAR el reporte " + reporte + ".\n" + j);
            System.out.println("Mensaje de Error:" + j);
        }
    }
     
    public void RELACION_DE_CREDITOS(Object[] key) {
        String reporte = "relacion de creditos.jasper";
        try {
            masterReport = (JasperReport) JRLoader.loadObject(RUTA_REPORTE.concat(reporte));
            PARAMETROS.put("fecha1", key[0]);
            PARAMETROS.put("fecha2", key[1]);
            PARAMETROS.put("ID_KEY", key[2]);
            PARAMETROS.put("UsuarioConsulta", key[3]);
            edicion.Lanzador_rpt_vista_previa(masterReport, PARAMETROS);
        } catch (Exception j) {
            edicion.mensajes(3, "NO SE PUEDE LANZAR el reporte " + reporte + ".\n" + j);
            System.out.println("Mensaje de Error:" + j);
        }
    }
    
    public void RELACION_DE_PAGOS_CREDITOS(Object[] key) {
        String reporte = "Relacion de Pagos.jasper";
        try {
            masterReport = (JasperReport) JRLoader.loadObject(RUTA_REPORTE.concat(reporte));
            PARAMETROS.put("FECHA1", key[0]);
            PARAMETROS.put("FECHA2", key[1]);
            PARAMETROS.put("ID_KEY", key[2]);
            PARAMETROS.put("Usuario_Consulta", key[3]);
            edicion.Lanzador_rpt_vista_previa(masterReport, PARAMETROS);
        } catch (Exception j) {
            edicion.mensajes(3, "NO SE PUEDE LANZAR el reporte " + reporte + ".\n" + j);
            System.out.println("Mensaje de Error:" + j);
        }
    }
    
      public void COMPROBANTE_DE_LIQUIDACION_DE_LETRAS_PARA_GESTOR_DE_COBRO(Object[] key) {
        String reporte = "Comprobante de Liquidacion.jasper";
        try {
            masterReport = (JasperReport) JRLoader.loadObject(RUTA_REPORTE.concat(reporte));
            PARAMETROS.put("FECHA1", key[0]);
            PARAMETROS.put("FECHA2", key[1]);
            PARAMETROS.put("RECAUDADOR", key[2]);
            PARAMETROS.put("UserSearch", key[3]);
//            System.out.println("parametros: " + PARAMETROS);
            edicion.Lanzador_rpt_vista_previa(masterReport, PARAMETROS);
        } catch (Exception j) {
            edicion.mensajes(3, "NO SE PUEDE LANZAR el reporte " + reporte + ".\n" + j);
            System.out.println("Mensaje de Error:" + j);
        }
    }
    
     public void DATOS_CLIENTE_PARA_PEGAR_A_LETRA(Object[] key) {
        String reporte = "Datos Cliente.jasper";
        try {
            masterReport = (JasperReport) JRLoader.loadObject(RUTA_REPORTE.concat(reporte));
            PARAMETROS.put("ID_CLIENTE", key[0]);
            PARAMETROS.put("ID_CREDITO", key[1]);
            edicion.Lanzador_rpt_vista_previa(masterReport, PARAMETROS);
        } catch (Exception j) {
            edicion.mensajes(3, "NO SE PUEDE LANZAR el reporte " + reporte + ".\n" + j);
            System.out.println("Mensaje de Error:" + j);
        }
    }
    
      public void RELACION_DE_RECAUDO_GESTORES_DE_COBRO(Object[] key) {
        String reporte = "Relacion de Recaudo.jasper";
        try {
            masterReport = (JasperReport) JRLoader.loadObject(RUTA_REPORTE.concat(reporte));
            PARAMETROS.put("FECHA1", key[0]);
            PARAMETROS.put("FECHA2", key[1]);
            edicion.Lanzador_rpt_vista_previa(masterReport, PARAMETROS);
        } catch (Exception j) {
            edicion.mensajes(3, "NO SE PUEDE LANZAR el reporte " + reporte + ".\n" + j);
            System.out.println("Mensaje de Error:" + j);
        }
    }
      
       public void PLANILLA_DE_COBRO(Object key) {
        String reporte = "Planilla de Cobros.jasper";
        try {
            masterReport = (JasperReport) JRLoader.loadObject(RUTA_REPORTE.concat(reporte));
            PARAMETROS.put("NUM_PLANILLA", key);
            edicion.Lanzador_rpt_vista_previa(masterReport, PARAMETROS);
        } catch (Exception j) {
            edicion.mensajes(3, "NO SE PUEDE LANZAR el reporte " + reporte + ".\n" + j);
            System.out.println("Mensaje de Error:" + j);
        }
    }
       public void LISTADO_EMAIL(String archivo) {
        try {
            File objetofile = new File(archivo);
            if (objetofile.exists()) {
                objetofile.delete();
            }
            ClienteDao dao = new ClienteDao();
            dao.LISTADO_CORREOS_CLIENTES();
            Desktop.getDesktop().open(objetofile);

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
