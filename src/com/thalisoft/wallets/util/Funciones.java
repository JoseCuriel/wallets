package com.thalisoft.wallets.util;

import java.util.ArrayList;

/**
 *
 * @author Thali
 */
public class Funciones {

    // Función para pasar un ArrayList de Integer a Integer[]
    public static Integer[] FnGetArrayList(ArrayList<Integer> alDatos) {
        // Declare el Array a Retornar
        Integer[] arrResult = new Integer[alDatos.size()];

        // Ciclo para pasar los datos
        for (int iIndex = 0; iIndex < alDatos.size(); iIndex++) {
            // Pasa el Dato del ArrayList al Array
            arrResult[iIndex] = alDatos.get(iIndex);
        }

        // Retorna el Array
        return arrResult;
    }

}
