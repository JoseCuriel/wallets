/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thalisoft.wallets.util;

import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Thali
 */
public class ColorearCeldaPlanillaCobro extends DefaultTableCellRenderer {

    public int valorCambio;
    public ColorearCeldaPlanillaCobro() {
        
    }
    
    
    
     @Override
    public Component getTableCellRendererComponent(JTable table, 
            Object value, 
            boolean isSelected, 
            boolean hasFocus, 
            int row,
            int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.
        
        // Verifica si debe iluminarse
       if (valorCambio == 1)
       {
           // Le coloca el Color Rojo
           this.setBackground(Color.ORANGE);
       }
       else
       {
           // Le coloca el color Naranja
           this.setForeground(Color.BLACK);
       }
       
       // Coloca un padding a la izquierda del dato
       setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
              
        
        return this;
    }
    
}
