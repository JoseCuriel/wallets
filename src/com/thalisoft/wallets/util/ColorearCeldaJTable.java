package com.thalisoft.wallets.util;

import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Thali
 */
public class ColorearCeldaJTable extends DefaultTableCellRenderer {

     public Integer[] arrIntRowsIluminados;
     
    @Override
    public Component getTableCellRendererComponent(JTable table, 
            Object value, 
            boolean isSelected, 
            boolean hasFocus, 
            int row,
            int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.
        
        // Verifica si debe iluminarse
       if (FnBoolRowIluminated(row))
       {
           // Le coloca el Color Rojo
           this.setBackground(Color.RED);
       }
       else
       {
           // Le coloca el color Naranja
           this.setForeground(Color.BLACK);
       }
       
       // Coloca un padding a la izquierda del dato
       setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
              
        
        return this;
    }
    
    
     // Función que va a validar si debe iluminarse el Row
    private boolean FnBoolRowIluminated(int iRow)
    {
        // Variable de Resultado
        boolean bResult = false;
        
        // Ciclo para revisar el Arreglo de Rows a iluminarse
        for (Integer arrIntRowsIluminado : arrIntRowsIluminados) {
            // Verifica si corresponde
            if (arrIntRowsIluminado == iRow) {
                // Si lo debe iluminar
                bResult=true;
                
                // Sale del Ciclo
                break;
            }
        }
        
        // Devuelve el Resultado
        return bResult;
    }       


}
