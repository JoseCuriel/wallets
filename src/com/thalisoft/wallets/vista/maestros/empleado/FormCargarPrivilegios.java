package com.thalisoft.wallets.vista.maestros.empleado;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.model.maestros.empleados.privilegios.ListadoPrivilegio;
import com.thalisoft.wallets.model.maestros.empleados.privilegios.PrivilegioEmpleado;
import com.thalisoft.wallets.model.maestros.empleados.privilegios.PrivilegioEmpleadoDao;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Variables_Gloabales;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FormCargarPrivilegios extends javax.swing.JInternalFrame {

    Edicion edicion = new Edicion();
    PrivilegioEmpleado privilegioEmpleado;
    PrivilegioEmpleadoDao peDao;
    EmpleadoDao empleadoDao = new EmpleadoDao();
    Object empleado, ACCESO;
    int PERMITIR_DENEGAR_ACCESO;

    public FormCargarPrivilegios(Object empleado) {
        this.empleado = empleado;
        peDao = new PrivilegioEmpleadoDao();
        initComponents();
        CARGAR_EMPLEADO(empleado);
        Edicion.CENTRAR_VENTANA(this);
        CARGAR_PRIVILEGIOS_PREDETERMINADOS(empleado);
        LISTAR_PRIVILEGIOS();
        ACCIONES_FORMULARIO();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        JMENU_PRIVILEGIO = new javax.swing.JMenuItem();
        JMenu_Eliminar = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        LB_EMPLEADO = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_PRIVILEGIOS = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        comboPrivilegio = new javax.swing.JComboBox<String>();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        JMENU_PRIVILEGIO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JMENU_PRIVILEGIOActionPerformed(evt);
            }
        });
        jPopupMenu1.add(JMENU_PRIVILEGIO);

        JMenu_Eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/delete.png"))); // NOI18N
        JMenu_Eliminar.setText("ELIMINAR PRIVILEGIO");
        JMenu_Eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JMenu_EliminarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(JMenu_Eliminar);

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Privilegios Del Sistema Para Empleado");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PRIVILEGIOS DEL SISTEMA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18), new java.awt.Color(255, 102, 0))); // NOI18N

        jLabel1.setText("EMPLEADO");

        LB_EMPLEADO.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        TB_PRIVILEGIOS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "ID. PRIVILEGIO", "DESCRIPCION", "ACCESO", "FECHA HORA REG.", "EMPLEADO REG.", "F.H.ULT.MODIFICACION", "EMP. MODIFICA"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_PRIVILEGIOS.setComponentPopupMenu(jPopupMenu1);
        TB_PRIVILEGIOS.setRowHeight(22);
        jScrollPane1.setViewportView(TB_PRIVILEGIOS);
        if (TB_PRIVILEGIOS.getColumnModel().getColumnCount() > 0) {
            TB_PRIVILEGIOS.getColumnModel().getColumn(0).setMaxWidth(70);
            TB_PRIVILEGIOS.getColumnModel().getColumn(1).setMinWidth(150);
            TB_PRIVILEGIOS.getColumnModel().getColumn(4).setMinWidth(150);
            TB_PRIVILEGIOS.getColumnModel().getColumn(5).setMinWidth(100);
            TB_PRIVILEGIOS.getColumnModel().getColumn(6).setMinWidth(150);
        }

        jLabel3.setText("SELECCIONAR PRIVILEGIOS");

        comboPrivilegio.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/Add.png"))); // NOI18N
        jButton1.setText("AÑADIR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton3.setText("SALIR");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/securedata.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(52, 52, 52)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboPrivilegio, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(93, 93, 93)
                                .addComponent(LB_EMPLEADO, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1046, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(LB_EMPLEADO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                                    .addComponent(comboPrivilegio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton1)))
                        .addGap(37, 37, 37)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            if (comboPrivilegio.getSelectedItem().toString().isEmpty()) {
                edicion.mensajes(1, "por favor selecciona un privilegio de la lista.");
                return;
            }

            for (int i = 0; i < TB_PRIVILEGIOS.getRowCount(); i++) {
                Object DescPrivilegio = TB_PRIVILEGIOS.getValueAt(i, 1);
                if (DescPrivilegio.equals(comboPrivilegio.getSelectedItem())) {
                    edicion.mensajes(1, "el privilegio " + DescPrivilegio + " se encuentra en la lista.");
                    return;
                }
            }

            Object[] valuePrivilegio = {"'" + comboPrivilegio.getSelectedItem() + "'", "'" + empleado + "'", Variables_Gloabales.EMPLEADO.getIdentificacion()};
            if (peDao.CREATE_PRIVILEGIO_EMPLEADO(valuePrivilegio)) {
                CARGAR_PRIVILEGIOS_PREDETERMINADOS(empleado);
            }
        } catch (Exception e) {
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void JMenu_EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JMenu_EliminarActionPerformed
        // TODO add your handling code here:
        try {
            int si_no = (int) edicion.msjQuest(1, "estas seguro que deseas eliminar el privilegio?");
            if (si_no == 0) {
                if (peDao.ELIMINAR_PRIVILEGIO_EMPLEADO(edicion.toNumeroEntero(TB_PRIVILEGIOS.getValueAt(TB_PRIVILEGIOS.getSelectedRow(), 0).toString()))) {
                    CARGAR_PRIVILEGIOS_PREDETERMINADOS(empleado);
                }
            }
        } catch (Exception e) {
            edicion.mensajes(3, "por favor selecciona un privilegio.");
        }

    }//GEN-LAST:event_JMenu_EliminarActionPerformed

    private void JMENU_PRIVILEGIOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JMENU_PRIVILEGIOActionPerformed
        // TODO add your handling code here:
        try {
            int sino = (int) edicion.msjQuest(1, "estas seguro que deseas " + ACCESO + " el ACCESO a " + TB_PRIVILEGIOS.getValueAt(TB_PRIVILEGIOS.getSelectedRow(), 1) + "?");
            if (sino == 0) {
                if (peDao.PERMITIR_DENEGAR_PRIVILEGIO(PERMITIR_DENEGAR_ACCESO,
                        edicion.toNumeroEntero(TB_PRIVILEGIOS.getValueAt(TB_PRIVILEGIOS.getSelectedRow(), 0).toString()))) {
                    CARGAR_PRIVILEGIOS_PREDETERMINADOS(empleado);
                }
            }

        } catch (Exception e) {
        }
    }//GEN-LAST:event_JMENU_PRIVILEGIOActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem JMENU_PRIVILEGIO;
    private javax.swing.JMenuItem JMenu_Eliminar;
    private javax.swing.JLabel LB_EMPLEADO;
    private javax.swing.JTable TB_PRIVILEGIOS;
    private javax.swing.JComboBox<String> comboPrivilegio;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    private void CARGAR_PRIVILEGIOS_PREDETERMINADOS(Object empleado) {
        edicion.llenarTabla(TB_PRIVILEGIOS, peDao.CARGAR_LISTADO_PRIVILEGIOS_PREDETERMINADOS(empleado));
    }

    private void LISTAR_PRIVILEGIOS() {
        comboPrivilegio.removeAllItems();
        comboPrivilegio.addItem("");
        for (ListadoPrivilegio LISTA_DE_PRIVILEGIOS_DEL_SISTEMA : peDao.LISTA_DE_PRIVILEGIOS_DEL_SISTEMA()) {
            comboPrivilegio.addItem(LISTA_DE_PRIVILEGIOS_DEL_SISTEMA.getDescripcionPrivilegio());
        }
    }

    private void CARGAR_EMPLEADO(Object empleado) {
        Empleado e = empleadoDao.CONSULTAR_EMPLEADO(empleado);
        LB_EMPLEADO.setText(e.getNombres() + " " + e.getApellidos());
    }

    private void ACCIONES_FORMULARIO() {
        TB_PRIVILEGIOS.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                int row = TB_PRIVILEGIOS.getSelectedRow();
                System.out.println(TB_PRIVILEGIOS.getValueAt(row, 2));
                try {

                    if (TB_PRIVILEGIOS.getValueAt(row, 2).equals("PERMITIDO")) {
                        JMENU_PRIVILEGIO.setText("DENEGAR ACCESO");
                        JMENU_PRIVILEGIO.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/lock.png")));
                        PERMITIR_DENEGAR_ACCESO = 0;
                        ACCESO = "DENEGAR";
                    } else {
                        JMENU_PRIVILEGIO.setText("PERMITIR ACCESO");
                        JMENU_PRIVILEGIO.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/unlocked.png")));
                        PERMITIR_DENEGAR_ACCESO = 1;
                        ACCESO = "PERMITIR";
                    }
                } catch (Exception error) {
                    JMENU_PRIVILEGIO.setText("SELECCIONA UN EMPLEADO");
                    System.out.println("" + error);
                }

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        });
    }

}
