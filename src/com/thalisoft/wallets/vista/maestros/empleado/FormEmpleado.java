package com.thalisoft.wallets.vista.maestros.empleado;

import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.util.CambiaFormatoTexto;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.util.report.Manager_Report;

public class FormEmpleado extends javax.swing.JInternalFrame {

    Empleado empleado = null;
    EmpleadoDao empleadoDao;
    Edicion edicion = new Edicion();
    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();
    FormListarEmpleados formListarEmpleados;
    int ROL_EMPLEADO;

    FormEmpleado(FormListarEmpleados aThis) {
        empleadoDao = new EmpleadoDao();
        formListarEmpleados = aThis;
        initComponents();
        Edicion.CENTRAR_VENTANA(this);
        txtidficha.setText(empleadoDao.NUMERO_FICHA());
        CARGAR_ROLES_EMPLEADO();
        CARGAR_CARGOS_EMPLEADO();
        LIMITES_ROL_EMPLEADO();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtidficha = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtidentificacion = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtapellidos = new javax.swing.JTextField();
        txtnombres = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txttelefono = new javax.swing.JTextField();
        txtpassword = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtsalario = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtPorcComision = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtestado = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        JDateFechaingreso = new com.toedter.calendar.JDateChooser();
        jPanel2 = new javax.swing.JPanel();
        jButton6 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        comboCargo = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        comboRoles = new javax.swing.JComboBox();

        setClosable(true);
        setIconifiable(true);
        setTitle("Empleado");

        jPanel1.setBackground(new java.awt.Color(255, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 255), 4, true), "FICHA DE EMPLEADO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 0, 18))); // NOI18N

        jLabel2.setText("ID. FICHA");

        txtidficha.setEditable(false);
        txtidficha.setHorizontalAlignment(javax.swing.JTextField.TRAILING);

        jLabel3.setText("No. DE IDENTIFICACIÓN");

        jLabel4.setText("APELLIDOS");

        jLabel5.setText("NOMBRES");

        jLabel6.setText("DIRECCIÓN");

        jLabel7.setText("TELEFONO");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/id-card.png"))); // NOI18N

        jLabel8.setText("CLAVE DE ACCESO");

        jLabel9.setText("CARGO");

        jLabel10.setText("SALARIO BASICO");

        txtsalario.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtsalario.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtsalario.setText("$ 0");

        jLabel11.setText("% COMISION");

        txtPorcComision.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtPorcComision.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtPorcComision.setText("0%");
        txtPorcComision.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPorcComisionActionPerformed(evt);
            }
        });

        jLabel12.setText("ESTADO ACTUAL");

        txtestado.setEnabled(false);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel13.setText("FECHA DE REGISTRO EN SISTEMA");

        JDateFechaingreso.setDateFormatString("dd MMM yyyy hh:mm:ss");
        JDateFechaingreso.setEnabled(false);
        JDateFechaingreso.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N

        jPanel2.setBackground(new java.awt.Color(255, 204, 204));

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton6.setToolTipText("Salir");
        jButton6.setBorder(null);
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/cancel.png"))); // NOI18N
        jButton4.setToolTipText("Cancelar");
        jButton4.setBorder(null);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/shift-change.png"))); // NOI18N
        btnModificar.setToolTipText("Actualizar");
        btnModificar.setBorder(null);
        btnModificar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnModificar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/save.png"))); // NOI18N
        jButton2.setToolTipText("Guardar");
        jButton2.setBorder(null);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/book.png"))); // NOI18N
        jButton7.setToolTipText("Consultar Ficha");
        jButton7.setBorder(null);
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton7.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/add-page.png"))); // NOI18N
        jButton1.setToolTipText("Nueva Ficha");
        jButton1.setBorder(null);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE)
                    .addComponent(btnModificar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel14.setText("ROL DEL SISTEMA");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7))
                                .addGap(4, 4, 4)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtidficha, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtidentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtapellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabel5))
                                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(156, 156, 156)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addGap(33, 33, 33)
                                        .addComponent(txtpassword, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(6, 6, 6)
                        .addComponent(txtnombres, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(156, 156, 156)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(41, 41, 41)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtsalario, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)
                                    .addComponent(comboCargo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel11))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtPorcComision, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(comboRoles, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addGap(42, 42, 42)
                                .addComponent(txtestado, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(JDateFechaingreso, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel2)
                                .addGap(17, 17, 17)
                                .addComponent(jLabel3)
                                .addGap(9, 9, 9)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(9, 9, 9)
                                .addComponent(jLabel6)
                                .addGap(12, 12, 12)
                                .addComponent(jLabel7))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtidficha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addComponent(txtidentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtapellidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(3, 3, 3)
                                        .addComponent(jLabel5)))
                                .addGap(6, 6, 6)
                                .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabel8))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(txtpassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(txtnombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(comboCargo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14)
                        .addComponent(comboRoles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtsalario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtPorcComision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel12))
                    .addComponent(txtestado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel13))
                    .addComponent(JDateFechaingreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        nuevo();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (validarEmpleado() != false) {
            int SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas registrar el empleado?");
            if (SI_NO == 0) {
                if (empleadoDao.CONSULTAR_EMPLEADO(cargarEmpleado().getIdentificacion()) == null) {
                    if (empleadoDao.CRUD_EMPLEADO(datosEmpleado(0)) != false) {
                        formListarEmpleados.llenar_listado();
                        edicion.mensajes(2, "empleado registrado correctamente");
                    }
                } else {
                    edicion.mensajes(1, "el empleado se encuentra registrado.");
                }
            }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        if (validarEmpleado() != false) {
            int SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas modificar el empleado?");
            if (SI_NO == 0) {
                if (empleadoDao.CONSULTAR_EMPLEADO(cargarEmpleado().getIdusuario()) != null) {
                    if (empleadoDao.CRUD_EMPLEADO(datosEmpleado(1)) != false) {
                        formListarEmpleados.llenar_listado();
                        edicion.mensajes(2, "empleado actualizado correctamente");
                    }
                } else {
                    edicion.mensajes(1, "el empleado no se  encuentra registrado.");
                }
            }
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if (validarEmpleado() != false) {
            int SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas cancelar el contrato del empleado?");
            if (SI_NO == 0) {
                if (empleadoDao.CONSULTAR_EMPLEADO(cargarEmpleado().getIdentificacion()) != null) {
                    if (empleadoDao.CRUD_EMPLEADO(datosEmpleado(2)) != false) {
                        formListarEmpleados.llenar_listado();
                        edicion.mensajes(2, "el contrado del empleado hasido cancelado");
                    }
                } else {
                    edicion.mensajes(1, "el empleado no se  encuentra registrado.");
                }
            }
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        Object idempleado = edicion.msjQuest(2, "ingresa el numero de identificacion o de ficha del empleado.");
        consulta_empleado(idempleado);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void txtPorcComisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPorcComisionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPorcComisionActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser JDateFechaingreso;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox comboCargo;
    private javax.swing.JComboBox comboRoles;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtPorcComision;
    private javax.swing.JTextField txtapellidos;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtestado;
    private javax.swing.JTextField txtidentificacion;
    private javax.swing.JTextField txtidficha;
    private javax.swing.JTextField txtnombres;
    private javax.swing.JPasswordField txtpassword;
    private javax.swing.JTextField txtsalario;
    private javax.swing.JTextField txttelefono;
    // End of variables declaration//GEN-END:variables

    private Empleado cargarEmpleado() {
        return empleado = new Empleado(edicion.toNumeroEntero(txtidficha.getText()),
                txtidentificacion.getText(), txtapellidos.getText(), txtnombres.getText(),
                "'" + txttelefono.getText() + "'", "'" + txtdireccion.getText().toUpperCase() + "'",
                "'" + txtpassword.getText() + "'", "'" + comboCargo.getSelectedItem() + "'", ROL_EMPLEADO,
                edicion.toNumeroEntero(txtsalario.getText()), edicion.toNumeroEntero(txtPorcComision.getText()), "1", null);
    }

    private boolean validarEmpleado() {

        if (cargarEmpleado().getIdentificacion() == null | cargarEmpleado().getIdentificacion().isEmpty()) {
            edicion.mensajes(1, "por favor ingresa el numero de identidad del empleado.");
            txtidentificacion.grabFocus();
            return false;
        }

        if (cargarEmpleado().getApellidos() == null | cargarEmpleado().getApellidos().isEmpty()) {
            edicion.mensajes(1, "por favor ingresa los apellidos del empleado.");
            txtapellidos.grabFocus();
            return false;
        }
        if (cargarEmpleado().getNombres() == null | cargarEmpleado().getNombres().isEmpty()) {
            edicion.mensajes(1, "por favor ingresa los nombres del empleado.");
            txtnombres.grabFocus();
            return false;
        }

        return true;
    }

    private Object[] datosEmpleado(int opcion) {
        Empleado empl = cargarEmpleado();
        Object[] key = new Object[13];
        if (opcion == 0) {
            key[0] = 0;
        }
        if (opcion == 1) {
            key[0] = 1;
        }
        if (opcion == 2) {
            key[0] = 2;
        }
        key[1] = empl.getIdentificacion();
        key[2] = "'" + empl.getApellidos().toUpperCase() + "'";
        key[3] = "'" + empl.getNombres().toUpperCase() + "'";
        key[4] = empl.getDireccion();
        key[5] = empl.getTelefono();
        key[6] = empl.getPassword();
        key[7] = empl.getCargo();
        key[8] = empl.getSalariobasico();
        key[9] = empl.getPorcComision();
        key[10] = empl.getIdusuario();
        key[11] = "'" + comboRoles.getSelectedItem() + "'";
        key[12] = Variables_Gloabales.EMPLEADO.getIdentificacion();
        return key;
    }

    private void LOAD_EMPLOYED_IN_COMPONENT() {
        txtidficha.setText(formatoTexto.numerico(empleado.getIdusuario()));
        txtidentificacion.setText(empleado.getIdentificacion());
        txtapellidos.setText(empleado.getApellidos());
        txtnombres.setText(empleado.getNombres());
        txtdireccion.setText(empleado.getDireccion());
        txttelefono.setText(empleado.getTelefono());
        comboCargo.setSelectedItem(empleado.getCargo());
        txtsalario.setText("$ " + formatoTexto.numerico(empleado.getSalariobasico()));
        txtPorcComision.setText(formatoTexto.numerico(empleado.getPorcComision()) + "%");
        txtestado.setText(empleado.getEstado());
        JDateFechaingreso.setDate(empleado.getFechahoraingreso());
    }

    private void nuevo() {
        empleado = new Empleado();
        LOAD_EMPLOYED_IN_COMPONENT();
        txtidficha.setText(empleadoDao.NUMERO_FICHA());
    }

    public void consulta_empleado(Object key) {
        empleado = empleadoDao.CONSULTAR_EMPLEADO(key);
        if (empleado != null) {
            LOAD_EMPLOYED_IN_COMPONENT();
        } else {
            edicion.mensajes(1, "el empleado no se encuentra registrado.");
        }
    }

    private void LIMITES_ROL_EMPLEADO() {
        boolean limite;
        if (Variables_Gloabales.EMPLEADO.getRolSistema() == 1) {
            limite = true;
        } else {
            limite = false;
        }

        btnModificar.setVisible(limite);
        txtPorcComision.setEditable(limite);
        txtsalario.setEditable(limite);
    }

    private void CARGAR_ROLES_EMPLEADO() {

        comboRoles.removeAllItems();
        comboRoles.addItem(null);
        for (Object[] LISTADO_ROLES_EMPLEADO : empleadoDao.LISTADO_ROLES_EMPLEADO(Variables_Gloabales.EMPLEADO.getRolSistema())) {
            comboRoles.addItem(LISTADO_ROLES_EMPLEADO[1]);
        }

    }

    private void CARGAR_CARGOS_EMPLEADO() {
        comboCargo.removeAllItems();
        comboCargo.addItem(null);
        for (Object[] LISTADO_CARGOS_EMPLEADO : empleadoDao.LISTADO_CARGOS_EMPLEADO()) {
            comboCargo.addItem(LISTADO_CARGOS_EMPLEADO[1]);
        }
    }

}
