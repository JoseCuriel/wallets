package com.thalisoft.wallets.vista.maestros.empleado;

import com.thalisoft.wallets.main.controller.ControllerContenedor;
import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.model.maestros.empleados.privilegios.PrivilegioEmpleadoDao;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.util.report.Manager_Report;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;

public class FormListarEmpleados extends javax.swing.JInternalFrame {

    Edicion edicion = new Edicion();
    EmpleadoDao Edao;
    Manager_Report report = new Manager_Report();

    PrivilegioEmpleadoDao privilegioAcceso = new PrivilegioEmpleadoDao();

    public FormListarEmpleados() {
        Edao = new EmpleadoDao();
        initComponents();
        llenar_listado();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        JMenu_Privilegios = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_LISTADO_EMPLEADO = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        LB_EMP_REG = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtTotalSalario = new javax.swing.JTextField();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/shift-change.png"))); // NOI18N
        jMenuItem1.setText("MODIFICAR");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);
        jPopupMenu1.add(jSeparator1);

        JMenu_Privilegios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/privilegios.png"))); // NOI18N
        JMenu_Privilegios.setText("PRIVILEGIOS DEL SISTEMA");
        JMenu_Privilegios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JMenu_PrivilegiosActionPerformed(evt);
            }
        });
        jPopupMenu1.add(JMenu_Privilegios);

        setIconifiable(true);
        setTitle("Lista de Empleados");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 5, true), "EMPLEADOS REGISTRADOS", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 0, 14))); // NOI18N

        TB_LISTADO_EMPLEADO.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        TB_LISTADO_EMPLEADO.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No. Ficha", "Identificacion", "Apellidos y Nombres", "Direccion", "Telefonos", "Cargo", "Salario", "% Comisión", "Contrato"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_LISTADO_EMPLEADO.setComponentPopupMenu(jPopupMenu1);
        TB_LISTADO_EMPLEADO.setRowHeight(22);
        TB_LISTADO_EMPLEADO.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(TB_LISTADO_EMPLEADO);
        if (TB_LISTADO_EMPLEADO.getColumnModel().getColumnCount() > 0) {
            TB_LISTADO_EMPLEADO.getColumnModel().getColumn(0).setMaxWidth(75);
            TB_LISTADO_EMPLEADO.getColumnModel().getColumn(2).setMinWidth(150);
            TB_LISTADO_EMPLEADO.getColumnModel().getColumn(3).setMinWidth(200);
        }

        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/add-employed.png"))); // NOI18N
        jButton1.setToolTipText("Añadir Empleado");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton3.setToolTipText("Salir");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("EMPLEADOS REGISTRADOS:");

        LB_EMP_REG.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LB_EMP_REG.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LB_EMP_REG.setText("0");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("TOTAL SALARIOS:");

        txtTotalSalario.setEditable(false);
        txtTotalSalario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtTotalSalario.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtTotalSalario.setText("$ 0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 941, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(283, 283, 283)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LB_EMP_REG, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(txtTotalSalario, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(LB_EMP_REG)
                    .addComponent(jLabel3)
                    .addComponent(txtTotalSalario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormEmpleado", Variables_Gloabales.EMPLEADO.getIdentificacion())) {
            JInternalFrame ji = validador.getJInternalFrame(FormEmpleado.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormEmpleado(this);
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormEmpleado.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormEmpleado", Variables_Gloabales.EMPLEADO.getIdentificacion())) {
            FormEmpleado fe = new FormEmpleado(this);
            JInternalFrame ji = validador.getJInternalFrame(FormEmpleado.class.getName());
            if (ji == null || ji.isClosed()) {
                fe.consulta_empleado(TB_LISTADO_EMPLEADO.getValueAt(TB_LISTADO_EMPLEADO.getSelectedRow(), 0));
                ji = fe;
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormEmpleado.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void JMenu_PrivilegiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JMenu_PrivilegiosActionPerformed
        // TODO add your handling code here:
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormCargarPrivilegios", Variables_Gloabales.EMPLEADO.getIdentificacion())) {

            JInternalFrame ji = validador.getJInternalFrame(FormCargarPrivilegios.class.getName());
            if (ji == null || ji.isClosed()) {
                ji = new FormCargarPrivilegios(TB_LISTADO_EMPLEADO.getValueAt(TB_LISTADO_EMPLEADO.getSelectedRow(), 1));
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormCargarPrivilegios.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormCargarPrivilegios.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_JMenu_PrivilegiosActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem JMenu_Privilegios;
    private javax.swing.JLabel LB_EMP_REG;
    private javax.swing.JTable TB_LISTADO_EMPLEADO;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField txtTotalSalario;
    // End of variables declaration//GEN-END:variables

    public void llenar_listado() {
        edicion.llenarTabla(TB_LISTADO_EMPLEADO, Edao.LISTADO_EMPLEADOS());
        edicion.calcula_total(TB_LISTADO_EMPLEADO, LB_EMP_REG, txtTotalSalario, 6);
    }

    
}



 

class validador {

    public static java.util.HashMap<String, javax.swing.JInternalFrame> jIframes = new java.util.HashMap<>();

    public static void addJIframe(String key, javax.swing.JInternalFrame jiframe) {
        jIframes.put(key, jiframe);
    }

    public static javax.swing.JInternalFrame getJInternalFrame(String key) {
        return jIframes.get(key);
    }
}
