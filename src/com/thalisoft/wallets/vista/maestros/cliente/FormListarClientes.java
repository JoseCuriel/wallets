package com.thalisoft.wallets.vista.maestros.cliente;

import com.thalisoft.wallets.main.controller.ControllerContenedor;
import com.thalisoft.wallets.model.maestros.cliente.ClienteDao;
import com.thalisoft.wallets.model.maestros.empleados.privilegios.PrivilegioEmpleadoDao;
import com.thalisoft.wallets.util.ColorearCeldaJTable;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Funciones;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.util.report.Manager_Report;
import com.thalisoft.wallets.vista.maestros.empleado.FormEmpleado;
import com.thalisoft.wallets.vista.venta.credito.FormCredito;
import com.thalisoft.wallets.vista.venta.credito.FormPlanillaCobroCredito;
import com.thalisoft.wallets.vista.venta.credito.pagos.FormConsultaLetraCliente;
import com.thalisoft.wallets.vista.venta.credito.pagos.FormPagoCredito;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.TableRowSorter;

public class FormListarClientes extends javax.swing.JInternalFrame {

    Edicion edicion = new Edicion();
    ClienteDao Cdao;
    ClienteDao clienteDao;
    Manager_Report report = new Manager_Report();
    int opcionFiltro = 2;
    private TableRowSorter trsFiltro;
    FormCredito formCredito;
    FormPagoCredito formPagoCredito;
    FormConsultaLetraCliente formConsultaLetraCliente;
    FormPlanillaCobroCredito formPlanillaCobroCredito;
    PrivilegioEmpleadoDao privilegioAcceso = new PrivilegioEmpleadoDao();

    public FormListarClientes() {
        Cdao = new ClienteDao();
        initComponents();
        ACIONES_FORMULARIO();
    }

    public FormListarClientes(FormCredito formCredito, FormPagoCredito formPagoCredito,
            FormConsultaLetraCliente formConsultaLetraCliente, FormPlanillaCobroCredito formPlanillaCobroCredito) {
        this.formCredito = formCredito;
        this.formPagoCredito = formPagoCredito;
        this.formConsultaLetraCliente = formConsultaLetraCliente;
        this.formPlanillaCobroCredito = formPlanillaCobroCredito;
        Cdao = new ClienteDao();
        initComponents();
        ACIONES_FORMULARIO();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_LISTADO_CLIENTES = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        TXTFILTRO = new javax.swing.JTextField();
        radioapenomrs = new javax.swing.JRadioButton();
        radionumid = new javax.swing.JRadioButton();
        radioDireccion = new javax.swing.JRadioButton();
        radioTelefono = new javax.swing.JRadioButton();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/shift-change.png"))); // NOI18N
        jMenuItem1.setText("MODIFICAR");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        setIconifiable(true);
        setTitle("Lista de Clientes");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 5, true), "CLIENTES REGISTRADOS", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 1, 24))); // NOI18N

        TB_LISTADO_CLIENTES.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        TB_LISTADO_CLIENTES.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "No. Ficha", "Identificacion", "Nombres & Apellidos", "Telefonos", "Direccion", "Observaciones"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_LISTADO_CLIENTES.setComponentPopupMenu(jPopupMenu1);
        TB_LISTADO_CLIENTES.setRowHeight(22);
        TB_LISTADO_CLIENTES.getTableHeader().setReorderingAllowed(false);
        TB_LISTADO_CLIENTES.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TB_LISTADO_CLIENTESMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TB_LISTADO_CLIENTES);
        if (TB_LISTADO_CLIENTES.getColumnModel().getColumnCount() > 0) {
            TB_LISTADO_CLIENTES.getColumnModel().getColumn(0).setMaxWidth(50);
            TB_LISTADO_CLIENTES.getColumnModel().getColumn(2).setMinWidth(250);
            TB_LISTADO_CLIENTES.getColumnModel().getColumn(4).setMinWidth(250);
            TB_LISTADO_CLIENTES.getColumnModel().getColumn(5).setMinWidth(200);
        }

        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/add-employed.png"))); // NOI18N
        jButton1.setToolTipText("Añadir Cliente");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton3.setToolTipText("Salir");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "FILTRAR POR:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial Narrow", 0, 14))); // NOI18N

        buttonGroup1.add(radioapenomrs);
        radioapenomrs.setSelected(true);
        radioapenomrs.setText("NOMBRES & APELLIDOS");
        radioapenomrs.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radioapenomrsItemStateChanged(evt);
            }
        });

        buttonGroup1.add(radionumid);
        radionumid.setText("No. DE IDENTIDAD");
        radionumid.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radionumidItemStateChanged(evt);
            }
        });

        buttonGroup1.add(radioDireccion);
        radioDireccion.setText("DIRECCIÓN");
        radioDireccion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radioDireccionItemStateChanged(evt);
            }
        });

        buttonGroup1.add(radioTelefono);
        radioTelefono.setText("TELEFONO");
        radioTelefono.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radioTelefonoItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(TXTFILTRO, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(radioapenomrs)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radionumid)
                        .addGap(18, 18, 18)
                        .addComponent(radioDireccion)))
                .addGap(18, 18, 18)
                .addComponent(radioTelefono)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioapenomrs)
                    .addComponent(radionumid)
                    .addComponent(radioDireccion)
                    .addComponent(radioTelefono))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(TXTFILTRO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 988, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormCliente", Variables_Gloabales.EMPLEADO.getIdentificacion())) {

            JInternalFrame ji = validador.getJInternalFrame(FormCliente.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormCliente(this);
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormCliente.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormCliente", Variables_Gloabales.EMPLEADO.getIdentificacion())) {
            FormCliente fe = new FormCliente(this);
            JInternalFrame ji = validador.getJInternalFrame(FormCliente.class.getName());
            if (ji == null || ji.isClosed()) {
                fe.consulta_cliente("1," + TB_LISTADO_CLIENTES.getValueAt(TB_LISTADO_CLIENTES.getSelectedRow(), 0));
                ji = fe;
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormCliente.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormCliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void TB_LISTADO_CLIENTESMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TB_LISTADO_CLIENTESMouseClicked
        // TODO add your handling code here:
        try {
            if (evt.getClickCount() == 2) {
                Object IdentificaCliente = TB_LISTADO_CLIENTES.getValueAt(TB_LISTADO_CLIENTES.getSelectedRow(), 1);

                if (formCredito != null) {
                    formCredito.CONSULTAR_CLIENTE(IdentificaCliente);
                    dispose();
                }

                if (formPagoCredito != null) {
                    formPagoCredito.CARGAR_FORM_LETRAS_CLIENTE(TB_LISTADO_CLIENTES.getValueAt(TB_LISTADO_CLIENTES.getSelectedRow(), 1).toString());
                    dispose();
                }
                if (formConsultaLetraCliente != null) {
                    formConsultaLetraCliente.CONSULTA_CLIENTE(TB_LISTADO_CLIENTES.getValueAt(TB_LISTADO_CLIENTES.getSelectedRow(), 1).toString());
                    dispose();
                }

                if (formPlanillaCobroCredito != null) {
                    if (formPlanillaCobroCredito.CONSULTA_CLIENTE == 1) {
                        formPlanillaCobroCredito.CARGAR_FORM_LETRAS_CLIENTE(TB_LISTADO_CLIENTES.getValueAt(TB_LISTADO_CLIENTES.getSelectedRow(), 1).toString());
                    } else {
                        formPlanillaCobroCredito.CLIENTE = Cdao.CONSULTAR_CLIENTE("1," + TB_LISTADO_CLIENTES.getValueAt(TB_LISTADO_CLIENTES.getSelectedRow(), 1).toString());
                        formPlanillaCobroCredito.MOSTRAR_DATOS_PLANILLA();
                    }
                    dispose();
                }
            }
        } catch (Exception e) {
            System.out.println("error: " + e);
        }
    }//GEN-LAST:event_TB_LISTADO_CLIENTESMouseClicked

    private void radioapenomrsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioapenomrsItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            opcionFiltro = 2;
            TXTFILTRO.setText(null);
            TXTFILTRO.requestFocus();
        }
    }//GEN-LAST:event_radioapenomrsItemStateChanged

    private void radionumidItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radionumidItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            opcionFiltro = 1;
            TXTFILTRO.setText(null);
            TXTFILTRO.requestFocus();
        }
    }//GEN-LAST:event_radionumidItemStateChanged

    private void radioDireccionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioDireccionItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            opcionFiltro = 4;
            TXTFILTRO.setText(null);
            TXTFILTRO.requestFocus();
        }
    }//GEN-LAST:event_radioDireccionItemStateChanged

    private void radioTelefonoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioTelefonoItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            opcionFiltro = 3;
            TXTFILTRO.setText(null);
            TXTFILTRO.requestFocus();
        }
    }//GEN-LAST:event_radioTelefonoItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TB_LISTADO_CLIENTES;
    private javax.swing.JTextField TXTFILTRO;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JRadioButton radioDireccion;
    private javax.swing.JRadioButton radioTelefono;
    private javax.swing.JRadioButton radioapenomrs;
    private javax.swing.JRadioButton radionumid;
    // End of variables declaration//GEN-END:variables

    public void llenar_listado() {
        edicion.llenarTabla(TB_LISTADO_CLIENTES, Cdao.LISTADO_CLIENTES());
        ColorearCeldaJTable xCelda = new ColorearCeldaJTable();
        ArrayList<Integer> alPorSustituir = new ArrayList<>();
        for (int i = 0; i < TB_LISTADO_CLIENTES.getRowCount(); i++) {
            if (TB_LISTADO_CLIENTES.getValueAt(i, 5) != "") {
                alPorSustituir.add(i);
            }
        }

        /// Asigna los Rows por Surtir para que los ilumine
        xCelda.arrIntRowsIluminados = Funciones.FnGetArrayList(alPorSustituir);
        xCelda.setHorizontalAlignment(SwingConstants.LEADING);
        TB_LISTADO_CLIENTES.setDefaultRenderer(Object.class, xCelda);
    }

    private void ACIONES_FORMULARIO() {
        Edicion.CENTRAR_VENTANA(this);
        llenar_listado();
        TXTFILTRO.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(final KeyEvent e) {
                String cadenafiltra = (TXTFILTRO.getText());
                TXTFILTRO.setText(cadenafiltra.toUpperCase());
                validate();
                filtro();
            }

            private void filtro() {
                trsFiltro.setRowFilter(RowFilter.regexFilter(TXTFILTRO.getText(), opcionFiltro));
            }
        });
        trsFiltro = new TableRowSorter(TB_LISTADO_CLIENTES.getModel());
        TB_LISTADO_CLIENTES.setRowSorter(trsFiltro);
        TXTFILTRO.grabFocus();
    }
}

class validador {

    public static java.util.HashMap<String, javax.swing.JInternalFrame> jIframes = new java.util.HashMap<>();

    public static void addJIframe(String key, javax.swing.JInternalFrame jiframe) {
        jIframes.put(key, jiframe);
    }

    public static javax.swing.JInternalFrame getJInternalFrame(String key) {
        return jIframes.get(key);
    }
}
