package com.thalisoft.wallets.vista.maestros.producto;

import com.thalisoft.wallets.model.maestros.productos.Categoria;
import com.thalisoft.wallets.model.maestros.productos.CategoriaDao;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Variables_Gloabales;
import static com.thalisoft.wallets.vista.maestros.producto.FormProducto.comboCategoria;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FormCategoria extends javax.swing.JInternalFrame {

    CategoriaDao dao;
    Edicion edicion = new Edicion();
    Categoria categoria;

    public FormCategoria() {
        dao = new CategoriaDao();
        initComponents();
        edicion.llenarTabla(tb_categoria, dao.CARGAR_LISTA_CATEGORIAS("1,1"));
        Edicion.CENTRAR_VENTANA(this);
        tb_categoria.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getClickCount() == 1) {
                    int row = tb_categoria.rowAtPoint(e.getPoint());
                    txtidcategoria.setText(tb_categoria.getValueAt(row, 0).toString());
                    txtdescripcioncategoria.setText(tb_categoria.getValueAt(row, 1).toString());

                    CARGAR_SUBCATEGORIAS(row);
                }

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtidcategoria = new javax.swing.JTextField();
        txtdescripcioncategoria = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tb_categoria = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbsubcategoria = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txtdescripcionsubcat = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/delete.png"))); // NOI18N
        jMenuItem1.setText("ELIMINAR");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        setClosable(true);
        setIconifiable(true);
        setTitle("Categoria");

        jPanel1.setBackground(new java.awt.Color(153, 255, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CATEGORIAS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 36))); // NOI18N

        jLabel1.setText("ID. CATEGORIA");

        jLabel3.setText("DESCRIPCION");

        txtidcategoria.setEditable(false);
        txtidcategoria.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtidcategoria.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtidcategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtidcategoriaActionPerformed(evt);
            }
        });

        tb_categoria.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "ID.CATEGORIA", "DESCRIPCION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tb_categoria.getTableHeader().setReorderingAllowed(false);
        tb_categoria.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tb_categoriaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tb_categoria);
        if (tb_categoria.getColumnModel().getColumnCount() > 0) {
            tb_categoria.getColumnModel().getColumn(0).setMaxWidth(60);
        }

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SUBCATEGORIAS", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        tbsubcategoria.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "ID.CATEGORIA", "DESCRIPCION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbsubcategoria.setComponentPopupMenu(jPopupMenu1);
        tbsubcategoria.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tbsubcategoria);
        if (tbsubcategoria.getColumnModel().getColumnCount() > 0) {
            tbsubcategoria.getColumnModel().getColumn(0).setMaxWidth(60);
        }

        jLabel2.setText("DESCRIPCION");

        txtdescripcionsubcat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdescripcionsubcatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addGap(37, 37, 37)
                .addComponent(txtdescripcionsubcat, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtdescripcionsubcat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(31, 31, 31)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(66, Short.MAX_VALUE)))
        );

        jToolBar1.setBackground(new java.awt.Color(153, 255, 153));
        jToolBar1.setRollover(true);

        jButton1.setBackground(new java.awt.Color(153, 255, 153));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/file.png"))); // NOI18N
        jButton1.setText("Nuevo");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setBackground(new java.awt.Color(153, 255, 153));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/save.png"))); // NOI18N
        jButton2.setText("Guardar");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jButton3.setBackground(new java.awt.Color(153, 255, 153));
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton3.setText("Salir");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel3))
                            .addGap(42, 42, 42)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtidcategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtdescripcioncategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(222, 222, 222))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtidcategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtdescripcioncategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtidcategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtidcategoriaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtidcategoriaActionPerformed

    private void tb_categoriaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tb_categoriaKeyPressed
        // TODO add your handling code here:

    }//GEN-LAST:event_tb_categoriaKeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        NUEVA_CATEGORIA();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtdescripcionsubcatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdescripcionsubcatActionPerformed
        // TODO add your handling code here:
        REGISTRAR_SUBCATEGORIAS();
    }//GEN-LAST:event_txtdescripcionsubcatActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        REGISTRAR_CATEGORIA();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        ELIMINAR_SUBCATEGORIA();
    }//GEN-LAST:event_jMenuItem1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTable tb_categoria;
    private javax.swing.JTable tbsubcategoria;
    private javax.swing.JTextField txtdescripcioncategoria;
    private javax.swing.JTextField txtdescripcionsubcat;
    private javax.swing.JTextField txtidcategoria;
    // End of variables declaration//GEN-END:variables

    private void CARGAR_SUBCATEGORIAS(int row) {
        edicion.llenarTabla(tbsubcategoria, this.dao.CARGAR_LISTA_CATEGORIAS("2," + tb_categoria.getValueAt(row, 0)));
    }

    private void NUEVA_CATEGORIA() {
        txtidcategoria.setText(null);
        txtdescripcioncategoria.setText(null);
        txtdescripcionsubcat.setText(null);
        edicion.limpiar_tablas(tbsubcategoria);
    }

    private void REGISTRAR_CATEGORIA() {
        if (validarFormCategoria() != false) {
            Object[] value = {0, "'" + txtdescripcioncategoria.getText() + "'", Variables_Gloabales.EMPLEADO.getIdentificacion()};
            if (dao.CRUD_CATEGORIA(value) != false) {
                categoria = dao.SELECT_CATEGORIA("0,'" + txtdescripcioncategoria.getText() + "'");
                txtidcategoria.setText(edicion.AGREGAR_CEROS_LEFT(categoria.getIdCategoria()));

                edicion.llenarTabla(tb_categoria, dao.CARGAR_LISTA_CATEGORIAS("1,1"));
                comboCategoria.removeAllItems();
                comboCategoria.addItem(null);
                for (Categoria LISTADO_DE_CATEGORIAS : dao.LISTADO_DE_CATEGORIAS()) {
                    comboCategoria.addItem(LISTADO_DE_CATEGORIAS.getDescripcion());
                }
            }
        }
    }

    

    private void REGISTRAR_SUBCATEGORIAS() {
        Object[] value = {0, "'" + txtdescripcionsubcat.getText() + "'",
            Variables_Gloabales.EMPLEADO.getIdentificacion(), edicion.toNumeroEntero(txtidcategoria.getText())};
        if (dao.SELECT_CATEGORIA("0,'" + txtdescripcioncategoria.getText() + "'") != null) {
            if (dao.CRUD_SUBCATEGORIA(value) != false) {
                edicion.llenarTabla(tbsubcategoria, dao.CARGAR_LISTA_CATEGORIAS("2," + txtidcategoria.getText()));
                txtdescripcionsubcat.setText(null);
            }
        } else {
            edicion.mensajes(2, "la categoria aun no hasido registrada.");
        }

    }

    private void ELIMINAR_SUBCATEGORIA() {
        int row = tbsubcategoria.getSelectedRow();
        int rs = (int) edicion.msjQuest(1, "estas seguro que deseas eliminar la subcategoria " + tbsubcategoria.getValueAt(row, 1));
        if (rs == 0) {
            Object[] value = {1, "'" + tbsubcategoria.getValueAt(row, 1) + "'", null, null};
            if (dao.CRUD_SUBCATEGORIA(value)) {
                edicion.llenarTabla(tbsubcategoria, dao.CARGAR_LISTA_CATEGORIAS("2," + txtidcategoria.getText()));
            }
        }
    }

    private boolean validarFormCategoria() {
        
        for (int i = 0; i < tb_categoria.getRowCount(); i++) {
            Object descripcion = tb_categoria.getValueAt(i, 1);
            
            if (txtdescripcioncategoria.getText().equals(descripcion)) {
                edicion.mensajes(1, "la categoria: "+descripcion+" se encuentra registrada.");
                return false;
            }
        }
        
        return true;
    }
}
