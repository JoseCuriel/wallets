package com.thalisoft.wallets.vista.transaciones;

import com.thalisoft.wallets.model.transacciones.CajaRegistradoraDao;
import com.thalisoft.wallets.model.transacciones.DenominacionMonetaria;
import com.thalisoft.wallets.util.CambiaFormatoTexto;
import com.thalisoft.wallets.util.Edicion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JButton;

public class FormCajaRegistradora extends javax.swing.JInternalFrame implements ActionListener {

    Edicion edicion = new Edicion();
    CajaRegistradoraDao cajaDao;
    DenominacionMonetaria dm;
    JButton button;
    private final Map mapDenominacion = new HashMap();

    public FormCajaRegistradora() {
        cajaDao = new CajaRegistradoraDao();
        initComponents();
        Edicion.CENTRAR_VENTANA(this);
        LISTAR_DENOMINACIONES_MONETARIAS_BILLETES();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNumeroArqueo = new javax.swing.JTextField();
        FechaArpertura = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtCajeroApertura = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtMontoBaseInicial = new javax.swing.JTextField();
        JPanelDenominaciones = new javax.swing.JPanel();
        JPanelBilletes = new javax.swing.JPanel();
        JPanelMonedas = new javax.swing.JPanel();
        FechaCierre = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        txtCajeroCierra = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setTitle("Caja Registradora");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CAJA", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 24))); // NOI18N

        jLabel1.setText("N° de Arqueo");

        jLabel2.setText("Fecha de Apertura");

        jLabel3.setText("CAJERO APERTURA");

        txtCajeroApertura.setEditable(false);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("TOTAL MONTO BASE INICIAL");

        txtMontoBaseInicial.setEditable(false);
        txtMontoBaseInicial.setBackground(new java.awt.Color(255, 204, 51));
        txtMontoBaseInicial.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtMontoBaseInicial.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoBaseInicial.setText("$ 0");
        txtMontoBaseInicial.setBorder(null);

        JPanelDenominaciones.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Denominaciones Monetarias", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        JPanelBilletes.setBorder(javax.swing.BorderFactory.createTitledBorder("BILLETES"));
        JPanelBilletes.setLayout(new java.awt.GridLayout(3, 0));

        JPanelMonedas.setBorder(javax.swing.BorderFactory.createTitledBorder("MONEDAS"));
        JPanelMonedas.setLayout(new java.awt.GridLayout(4, 2));

        javax.swing.GroupLayout JPanelDenominacionesLayout = new javax.swing.GroupLayout(JPanelDenominaciones);
        JPanelDenominaciones.setLayout(JPanelDenominacionesLayout);
        JPanelDenominacionesLayout.setHorizontalGroup(
            JPanelDenominacionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPanelDenominacionesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JPanelBilletes, javax.swing.GroupLayout.PREFERRED_SIZE, 414, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPanelMonedas, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        JPanelDenominacionesLayout.setVerticalGroup(
            JPanelDenominacionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPanelDenominacionesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPanelDenominacionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(JPanelMonedas, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
                    .addComponent(JPanelBilletes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel5.setText("Fecha de Cierre");

        txtCajeroCierra.setEditable(false);

        jLabel6.setText("CAJERO CIERRE");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNumeroArqueo, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(FechaArpertura, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(FechaCierre, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel6))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCajeroCierra)
                                    .addComponent(txtCajeroApertura)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(txtMontoBaseInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(JPanelDenominaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNumeroArqueo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(FechaArpertura, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(FechaCierre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtCajeroApertura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtCajeroCierra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtMontoBaseInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(JPanelDenominaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser FechaArpertura;
    private com.toedter.calendar.JDateChooser FechaCierre;
    private javax.swing.JPanel JPanelBilletes;
    private javax.swing.JPanel JPanelDenominaciones;
    private javax.swing.JPanel JPanelMonedas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtCajeroApertura;
    private javax.swing.JTextField txtCajeroCierra;
    private javax.swing.JTextField txtMontoBaseInicial;
    private javax.swing.JTextField txtNumeroArqueo;
    // End of variables declaration//GEN-END:variables
Icon iconoMoneda;

    private void LISTAR_DENOMINACIONES_MONETARIAS_BILLETES() {

        JPanelBilletes.removeAll();
        JPanelBilletes.repaint();
        for (DenominacionMonetaria denominacionMonetaria : cajaDao.LISTADO_DENOMINACION_MONETARIA("BILLETE")) {

            button = new JButton(" \n" + CambiaFormatoTexto.numerico(
                    Float.parseFloat(denominacionMonetaria.getDescripcionMoneda())));
            button.setActionCommand(denominacionMonetaria.getDescripcionMoneda());
            iconoMoneda = new javax.swing.ImageIcon("C://ThaliSoftWalletsData//image//divisas//" + denominacionMonetaria.getImagenMoneda());
            button.setSize(iconoMoneda.getIconHeight(), iconoMoneda.getIconWidth());
            button.setIcon(iconoMoneda); // NOI18N
            JPanelBilletes.add(button);
            JPanelBilletes.validate();
            mapDenominacion.put(denominacionMonetaria.getDescripcionMoneda(), button);
            button.addActionListener(this);

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Iterator it = mapDenominacion.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String itm = entry.getKey().toString();
            if (itm.equals(comando)) {
//                dm = cajaDao.CONSULTA_DENOMINACION_MONETARIA(comando);
                ACUMULAR_CANTIDAD_MONETARIA(comando);
            }
        }
    }
    int acumularIngeso = 0;

    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();

    private void ACUMULAR_CANTIDAD_MONETARIA(String cntIngresada) {
        try {
            Object CntIngreso = edicion.msjQuest(2, "INGRESE LA CANTIDAD monetaria  ");
            if (CntIngreso != null) {
                if (edicion.toNumeroEntero(CntIngreso.toString()) > 0) {
                    int valorIngreso = edicion.toNumeroEntero(CntIngreso.toString()) * edicion.toNumeroEntero(cntIngresada);
                    acumularIngeso = acumularIngeso + valorIngreso;
                    txtMontoBaseInicial.setText("$ " + formatoTexto.numerico(acumularIngeso));
                }
            }

        } catch (Exception e) {
            edicion.mensajes(1, "por favor solo ingrese valores numericos sin '.' ni espacios u otro caracter especial.");
        }
    }
}
