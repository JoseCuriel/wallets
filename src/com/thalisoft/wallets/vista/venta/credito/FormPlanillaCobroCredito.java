package com.thalisoft.wallets.vista.venta.credito;

import com.thalisoft.wallets.main.controller.ControllerContenedor;
import com.thalisoft.wallets.model.maestros.cliente.Cliente;
import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.model.maestros.empleados.privilegios.PrivilegioEmpleadoDao;
import com.thalisoft.wallets.model.venta.credito.PlanillaCobro;
import com.thalisoft.wallets.model.venta.credito.PlanillaCobroDao;
import com.thalisoft.wallets.util.CambiaFormatoTexto;
import com.thalisoft.wallets.util.ColorearCeldaPlanillaCobro;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.util.report.Manager_Report;
import com.thalisoft.wallets.vista.maestros.cliente.FormListarClientes;
import com.thalisoft.wallets.vista.venta.credito.pagos.FormConsultaLetraCliente;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ThaliSoft
 */
public class FormPlanillaCobroCredito extends javax.swing.JInternalFrame {

    public String ARTICULOS;
    public String LETRAS;
    public int VALOR_LETRAS;
    public Cliente CLIENTE;
    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();

    PlanillaCobro planilla;
    PlanillaCobroDao planillaDao;

    Edicion edicion = new Edicion();
    EmpleadoDao recaudadorDao;
    Empleado RECAUDADOR;
    public int CONSULTA_CLIENTE = 0;
    private TableRowSorter trsFiltro;
    PrivilegioEmpleadoDao privilegioAcceso = new PrivilegioEmpleadoDao();

    public FormPlanillaCobroCredito() {
        recaudadorDao = new EmpleadoDao();
        planillaDao = new PlanillaCobroDao();
        initComponents();
        CARGAR_RECAUDADORES();
        Edicion.CENTRAR_VENTANA(this);
        ACCIONES_FML_PLANILLA();
        JD_FECHA_DESPACHO.setDate(DateUtil.newDateTime());
        txtnumPlanilla.setText(planillaDao.NUMERO_PLANILLA_COBRO());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_PLANILLA_COBRO = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtnumPlanilla = new javax.swing.JTextField();
        JD_FECHA_DESPACHO = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        comboRecaudador = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        txtCliente = new javax.swing.JTextField();
        txtDirCliente = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtLetras = new javax.swing.JTextField();
        txtValorLetras = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        jButton7 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        txtArticulo = new javax.swing.JTextField();
        JCheck_Cliente_Credito = new javax.swing.JCheckBox();
        jButton2 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        LB_CNT_CLIENTES = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        TXTTOTAL = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtFiltro = new javax.swing.JTextField();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/delete.png"))); // NOI18N
        jMenuItem1.setText("ELIMINAR");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        setClosable(true);
        setIconifiable(true);
        setTitle("Planilla de Cobros");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PLANILLA DE COBROS", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18))); // NOI18N

        TB_PLANILLA_COBRO.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        TB_PLANILLA_COBRO.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "ID. PLANILLA", "CLIENTE", "DIRECCIÓN", "ARTICULOS", "LETRAS", "VALOR TOTAL", "VERIFICADO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_PLANILLA_COBRO.setComponentPopupMenu(jPopupMenu1);
        TB_PLANILLA_COBRO.setRowHeight(22);
        TB_PLANILLA_COBRO.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(TB_PLANILLA_COBRO);
        if (TB_PLANILLA_COBRO.getColumnModel().getColumnCount() > 0) {
            TB_PLANILLA_COBRO.getColumnModel().getColumn(0).setMaxWidth(50);
            TB_PLANILLA_COBRO.getColumnModel().getColumn(1).setMinWidth(250);
            TB_PLANILLA_COBRO.getColumnModel().getColumn(2).setMinWidth(200);
            TB_PLANILLA_COBRO.getColumnModel().getColumn(3).setMinWidth(250);
        }

        jLabel1.setText("FECHA DE DESPACHO");

        jLabel2.setText("No. PLANILLA");

        txtnumPlanilla.setEditable(false);
        txtnumPlanilla.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtnumPlanilla.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        JD_FECHA_DESPACHO.setDateFormatString("EEEE dd MMMM yyyy");
        JD_FECHA_DESPACHO.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel3.setText("GESTOR DE COBRO");

        comboRecaudador.setBackground(new java.awt.Color(208, 160, 46));
        comboRecaudador.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        comboRecaudador.setForeground(new java.awt.Color(255, 51, 51));

        jLabel4.setText("CLIENTE");

        txtCliente.setEditable(false);
        txtCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtDirCliente.setEditable(false);
        txtDirCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel5.setText("DIRECCION");

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("LETRAS");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("VALOR");

        txtLetras.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtValorLetras.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtValorLetras.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtValorLetras.setText("$ 0");

        jToolBar1.setRollover(true);

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/add-page.png"))); // NOI18N
        jButton7.setFocusable(false);
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton7.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton7);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/printer.png"))); // NOI18N
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Back.png"))); // NOI18N
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Next.png"))); // NOI18N
        jButton5.setFocusable(false);
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton5.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton6.setFocusable(false);
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton6);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/team.png"))); // NOI18N
        jButton1.setToolTipText("SELECCIONAR CLIENTE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel8.setText("ARTICULO(s)");

        txtArticulo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        JCheck_Cliente_Credito.setText("CONSULTAR CLIENTE Y CREDITO");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/Add.png"))); // NOI18N
        jButton2.setText("AGREGAR LETRA(s)");
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtnumPlanilla, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JD_FECHA_DESPACHO, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDirCliente)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboRecaudador, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 26, Short.MAX_VALUE))
                    .addComponent(txtArticulo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLetras, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtValorLetras, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(JCheck_Cliente_Credito, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(comboRecaudador, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtnumPlanilla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtDirCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(3, 3, 3))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(JD_FECHA_DESPACHO, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(4, 4, 4)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtLetras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtValorLetras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(JCheck_Cliente_Credito)
                                .addGap(6, 6, 6)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(1, 1, 1)
                .addComponent(jButton2))
        );

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("CLIENTES AGREGADOS");

        LB_CNT_CLIENTES.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LB_CNT_CLIENTES.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LB_CNT_CLIENTES.setText("0");

        jLabel10.setText("TOTAL: ");

        TXTTOTAL.setEditable(false);
        TXTTOTAL.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        TXTTOTAL.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        TXTTOTAL.setText("$ 0");

        jLabel11.setText("FILTRAR:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 36, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(65, 65, 65))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(LB_CNT_CLIENTES, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel11)
                                .addGap(18, 18, 18)
                                .addComponent(txtFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TXTTOTAL, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(174, 174, 174))))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LB_CNT_CLIENTES, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(TXTTOTAL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11)
                        .addComponent(txtFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormListarClientes", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
            JInternalFrame ji = validador.getJInternalFrame(FormListarClientes.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormListarClientes(null, null, null, this);
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormListarClientes.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormListarClientes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        GUARDAR_LETRA_PLANILLA();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        int numeroPlanilla = edicion.toNumeroEntero(txtnumPlanilla.getText()) - 1;
        if (numeroPlanilla > 0) {
            CONSULTA_PLANILLA_COBRO(numeroPlanilla);
        }

    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        int numeroPlanilla = edicion.toNumeroEntero(txtnumPlanilla.getText()) + 1;
        CONSULTA_PLANILLA_COBRO(numeroPlanilla);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        int sino = (int) edicion.msjQuest(1, "estas seguro que deseas realizar una nueva planilla de cobro?");
        if (sino == 0) {
            txtnumPlanilla.setText(planillaDao.NUMERO_PLANILLA_COBRO());
            JD_FECHA_DESPACHO.setDate(DateUtil.newDateTime());
            comboRecaudador.setSelectedItem(null);
            txtCliente.setText(null);
            txtDirCliente.setText(null);
            txtLetras.setText(null);
            txtValorLetras.setText("$ 0");
            txtArticulo.setText(null);
            edicion.limpiar_tablas(TB_PLANILLA_COBRO);
            edicion.calcula_total(TB_PLANILLA_COBRO, LB_CNT_CLIENTES, TXTTOTAL, 5);
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        int sino = (int) edicion.msjQuest(1, "estas seguro que desea eliminar el cliente de la planilla?");
        if (sino == 0) {
            int row = TB_PLANILLA_COBRO.getSelectedRow();
            if (planillaDao.ELIMINAR_CLIENTE_PLANILLA(TB_PLANILLA_COBRO.getValueAt(row, 0))) {
                CONSULTA_PLANILLA_COBRO(txtnumPlanilla.getText());
                if (TB_PLANILLA_COBRO.getRowCount() < 1) {
                    if (planillaDao.ELIMINAR_PLANILLA_COBRO(txtnumPlanilla.getText())) {
                        edicion.mensajes(1, "debido aque la planilla quedo sin clientes. esta fue eliminada del sistema.");
                    }
                }
            }
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        Manager_Report report = new Manager_Report();
        report.PLANILLA_DE_COBRO(txtnumPlanilla.getText());
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox JCheck_Cliente_Credito;
    private com.toedter.calendar.JDateChooser JD_FECHA_DESPACHO;
    private javax.swing.JLabel LB_CNT_CLIENTES;
    private javax.swing.JTable TB_PLANILLA_COBRO;
    private javax.swing.JTextField TXTTOTAL;
    private javax.swing.JComboBox comboRecaudador;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField txtArticulo;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtDirCliente;
    private javax.swing.JTextField txtFiltro;
    private javax.swing.JTextField txtLetras;
    private javax.swing.JTextField txtValorLetras;
    private javax.swing.JTextField txtnumPlanilla;
    // End of variables declaration//GEN-END:variables

    public void CARGAR_FORM_LETRAS_CLIENTE(String idCliente) {
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormConsultaLetraCliente", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
            JInternalFrame ji = validador.getJInternalFrame(FormConsultaLetraCliente.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormConsultaLetraCliente(null, idCliente, this);
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormConsultaLetraCliente.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormConsultaLetraCliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }

    public void MOSTRAR_DATOS_PLANILLA() {
        txtCliente.setText(CLIENTE.getNombres() + " " + CLIENTE.getApellidos() + " - Tel: " + CLIENTE.getTelefono1());
        txtDirCliente.setText(CLIENTE.getDireccion1());
        txtLetras.setText(LETRAS);
        txtValorLetras.setText("$ " + formatoTexto.numerico(VALOR_LETRAS));
        txtArticulo.setText(ARTICULOS);
    }

    private void CARGAR_RECAUDADORES() {
        comboRecaudador.removeAllItems();
        comboRecaudador.addItem(null);
        for (Empleado Lista_Cobradores : recaudadorDao.LISTAR_EMPLEADOS_POR_CARGO("7, 'COBRADOR'")) {
            comboRecaudador.addItem(Lista_Cobradores.getNombres() + " " + Lista_Cobradores.getApellidos());
        }

    }

    void ACCIONES_FML_PLANILLA() {

        TB_PLANILLA_COBRO.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = TB_PLANILLA_COBRO.rowAtPoint(e.getPoint());
                if (e.getClickCount() == 2) {
                    planillaDao.CONFIRMAR_VERIFICACION_ARTICULO(TB_PLANILLA_COBRO.getValueAt(row, 0));
                    edicion.llenarTabla(TB_PLANILLA_COBRO, planillaDao.DETALLE_PLANILLA_COBRO("2," + txtnumPlanilla.getText()));
                    ColorearCeldaPlanillaCobro ccpc = new ColorearCeldaPlanillaCobro();
                    for (int i = 0; i < TB_PLANILLA_COBRO.getRowCount(); i++) {
                        if (TB_PLANILLA_COBRO.getValueAt(i, 6) == "SI") {
                            ccpc.valorCambio = 1;
                            TB_PLANILLA_COBRO.setDefaultRenderer(Object.class, ccpc);
                        }
                    }
                }
            }
        });

        txtFiltro.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(final KeyEvent e) {
                String cadenafiltra = (txtFiltro.getText());
                txtFiltro.setText(cadenafiltra.toUpperCase());
                repaint();
                filtro();
                edicion.calcula_total(TB_PLANILLA_COBRO, LB_CNT_CLIENTES, TXTTOTAL, 5);
            }

            private void filtro() {
                trsFiltro.setRowFilter(RowFilter.regexFilter(txtFiltro.getText(), 1));
            }
        });
        trsFiltro = new TableRowSorter(TB_PLANILLA_COBRO.getModel());
        TB_PLANILLA_COBRO.setRowSorter(trsFiltro);
        txtFiltro.grabFocus();

        comboRecaudador.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    RECAUDADOR = recaudadorDao.CONSULTAR_EMPLEADO(comboRecaudador.getSelectedItem());
                }
            }
        }
        );

        txtValorLetras.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                txtValorLetras.setText("$ " + formatoTexto.numerico(edicion.toNumeroEntero(txtValorLetras.getText())));
            }
        }
        );

        JCheck_Cliente_Credito.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (JCheck_Cliente_Credito.isSelected()) {
                    CONSULTA_CLIENTE = 1;
                } else {
                    CONSULTA_CLIENTE = 0;
                    ARTICULOS = null;
                    LETRAS = null;
                    VALOR_LETRAS = 0;
                }
            }
        });

    }

    private Object[] DATOS_PLANILLA_COBRO() {
        Object[] data = new Object[4];
        data[0] = edicion.toNumeroEntero(txtnumPlanilla.getText());
        data[1] = "'" + edicion.formatearFechaSQL(JD_FECHA_DESPACHO.getDate()) + "'";
        data[2] = RECAUDADOR.getIdentificacion();
        data[3] = Variables_Gloabales.EMPLEADO.getIdentificacion();
        return data;
    }

    private Object[] DATOS_DETALLE_PLANILLA_COBRO() {
        Object[] data = {edicion.toNumeroEntero(txtnumPlanilla.getText()), CLIENTE.getIdentificacion(),
            "'" + txtLetras.getText() + "'", edicion.toNumeroEntero(txtValorLetras.getText()),
            Variables_Gloabales.EMPLEADO.getIdentificacion(), "'" + txtArticulo.getText() + "'"};
        return data;
    }

    private boolean CONSULTA_PLANILLA_COBRO(Object numPlanilla) {
        planilla = planillaDao.CONSULTA_PLANILLA_COBRO("1," + numPlanilla);
        if (planilla != null) {
            txtnumPlanilla.setText(edicion.AGREGAR_CEROS_LEFT(planilla.getNumPlanilla()));
            JD_FECHA_DESPACHO.setDate(planilla.getFechaDespacho());
            comboRecaudador.setSelectedItem(planilla.getGestorCobro().getNombres() + " " + planilla.getGestorCobro().getApellidos());
            edicion.llenarTabla(TB_PLANILLA_COBRO, planillaDao.DETALLE_PLANILLA_COBRO("2, " + planilla.getNumPlanilla()));
            edicion.calcula_total(TB_PLANILLA_COBRO, LB_CNT_CLIENTES, TXTTOTAL, 5);
            return true;
        }
        return false;
    }

    private boolean VALIDAR_PLANILLA_COBRO() {

        if (comboRecaudador.getSelectedItem() == "" | comboRecaudador.getSelectedItem() == null) {
            edicion.mensajes(1, "seleccione por favor un gestor de cobro.");
            return false;
        }

        if (txtCliente.getText().isEmpty() | txtCliente.getText() == null) {
            edicion.mensajes(1, "seleccione por favor un cliente precionando el boton de consulta cliente.");
            return false;
        }

        if (txtLetras.getText().isEmpty() | txtLetras.getText() == null) {
            edicion.mensajes(1, "especifique por favor las letras que se van a cobrar.");
            txtLetras.grabFocus();
            return false;
        }

        if (edicion.toNumeroEntero(txtValorLetras.getText()) < 1) {
            edicion.mensajes(1, "el valor de las letras debe ser mayor a cero (0).");
            txtValorLetras.grabFocus();
            return false;
        }

        if (txtArticulo.getText().isEmpty() | txtArticulo.getText() == null) {
            edicion.mensajes(1, "especifique por favor el o los articulos.");
            txtArticulo.grabFocus();
            return false;
        }

        return true;
    }

    public void GUARDAR_LETRA_PLANILLA() {
        if (VALIDAR_PLANILLA_COBRO() != false) {

            /* if (planillaDao.CLIENTE_PLANILLA_COBRO(CLIENTE.getIdentificacion(), txtnumPlanilla.getText()) == false) {
             edicion.mensajes(1, "el cliente seleccionado se encuentra registrado en esta planilla.");
             } else {*/
            if (CONSULTA_PLANILLA_COBRO(txtnumPlanilla.getText()) == false) {
                if (planillaDao.CRUD_PLANILLA_COBRO(DATOS_PLANILLA_COBRO())) {
                    planillaDao.CREATE_DETALLE_PLANILLA_COBRO(DATOS_DETALLE_PLANILLA_COBRO());
                }
            } else {
                planillaDao.CREATE_DETALLE_PLANILLA_COBRO(DATOS_DETALLE_PLANILLA_COBRO());
            }
            CONSULTA_PLANILLA_COBRO(txtnumPlanilla.getText());
            txtCliente.setText(null);
            txtDirCliente.setText(null);
            txtLetras.setText(null);
            txtValorLetras.setText("$ 0");
            txtArticulo.setText(null);
//            }
        }
    }
}
