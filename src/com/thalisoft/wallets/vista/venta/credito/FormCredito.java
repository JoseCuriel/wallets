package com.thalisoft.wallets.vista.venta.credito;

import com.thalisoft.wallets.main.controller.ControllerContenedor;
import com.thalisoft.wallets.model.maestros.cliente.Cliente;
import com.thalisoft.wallets.model.maestros.cliente.ClienteDao;
import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.model.maestros.empleados.privilegios.PrivilegioEmpleadoDao;
import com.thalisoft.wallets.model.maestros.productos.Producto;
import com.thalisoft.wallets.model.maestros.productos.ProductoDao;
import com.thalisoft.wallets.model.venta.credito.Credito;
import com.thalisoft.wallets.model.venta.credito.CreditoDao;
import com.thalisoft.wallets.util.CambiaFormatoTexto;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.util.report.Manager_Report;
import com.thalisoft.wallets.vista.maestros.cliente.FormCliente;
import com.thalisoft.wallets.vista.maestros.cliente.FormListarClientes;
import com.thalisoft.wallets.vista.maestros.producto.FormListaProductos;
import com.thalisoft.wallets.vista.maestros.producto.FormProducto;
import com.thalisoft.wallets.vista.venta.credito.pagos.FormPagoCredito;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;

/**
 *
 * @author ThaliSoft
 */
public class FormCredito extends javax.swing.JInternalFrame {

    Edicion edicion = new Edicion();
    Manager_Report report = new Manager_Report();
    Credito credito;
    CreditoDao creditoDao;
    Empleado empleado;
    EmpleadoDao empleadoDao;
    Cliente cliente;
    ClienteDao clienteDao;

    Producto producto;
    ProductoDao productoDao;
    boolean ESTADO_BLOQUEO_COMPONET = false;
    PrivilegioEmpleadoDao privilegioAcceso = new PrivilegioEmpleadoDao();

    public FormCredito() {
        empleadoDao = new EmpleadoDao();
        creditoDao = new CreditoDao();
        clienteDao = new ClienteDao();
        productoDao = new ProductoDao();

        initComponents();
        CARGAR_COBRADORES();
        ACCIONES_FML_CREDITO();
        NUEVO_CREDITO();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jPopupMenu2 = new javax.swing.JPopupMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        JPanel_DatosCredito = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNumeroCredito = new javax.swing.JTextField();
        JD_FechaEmision = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        comboModoPago = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        comboCobrador = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtIdentidadCliente = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtNomApeCliente = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtTelCliente = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtDirCliente = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        txtEstadoCredito = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        JS_Garantia = new javax.swing.JSpinner();
        jLabel22 = new javax.swing.JLabel();
        txtTelCobrador = new javax.swing.JTextField();
        JD_FechaInicioPago = new com.toedter.calendar.JDateChooser();
        jLabel26 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_HISTORIAL_CREDITOS = new javax.swing.JTable();
        jLabel25 = new javax.swing.JLabel();
        LB_CANT_CREDITOS = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txtTotalVrHistCredito = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txtTotalVrHistSaldo = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TB_DETALLE_CREDITO = new javax.swing.JTable();
        btnProducto = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        txtReferencia = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtDescProducto = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtCntProducto = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtPrecioVentaUnd = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtCtaIncialUnd = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        LB_CNT_ARTICULOS = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        LB_EMP_REGISTRA_CREDITO = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtTotalVrCredito = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtTotalVrCtaInicial = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtTotalVrSaldo = new javax.swing.JTextField();
        txtTotalVrComision = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        TB_PLAN_PAGO = new javax.swing.JTable();
        js_Cnt_Letras = new javax.swing.JSpinner();
        txtVrLetra = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jToolBar2 = new javax.swing.JToolBar();
        btnSavedPlanPago = new javax.swing.JButton();
        btnAddCuota = new javax.swing.JButton();
        btn_prog_cta_inicial = new javax.swing.JButton();
        btnEliminaPlanPago = new javax.swing.JButton();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/delete.png"))); // NOI18N
        jMenuItem1.setText("ELIMINAR");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/printer.png"))); // NOI18N
        jMenuItem2.setText("IMPRIMIR CREDITO");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu2.add(jMenuItem2);

        setIconifiable(true);
        setTitle("Credito");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "GESTOR DE CREDITO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 1, 20))); // NOI18N

        JPanel_DatosCredito.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS DEL CREDITO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 0, 14))); // NOI18N
        JPanel_DatosCredito.setForeground(new java.awt.Color(255, 255, 255));
        JPanel_DatosCredito.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jLabel1.setText("NUMERO CREDITO");
        JPanel_DatosCredito.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 37, 199, 27));

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jLabel2.setText("FECHA EMISIÓN");
        JPanel_DatosCredito.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 70, 199, 29));

        txtNumeroCredito.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtNumeroCredito.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        JPanel_DatosCredito.add(txtNumeroCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(219, 35, 106, -1));

        JD_FechaEmision.setDateFormatString("EEEE dd MMM yyyy");
        JD_FechaEmision.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        JPanel_DatosCredito.add(JD_FechaEmision, new org.netbeans.lib.awtextra.AbsoluteConstraints(219, 70, 199, 29));

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jLabel3.setText("FECHA DE INICIO PAGOS");
        JPanel_DatosCredito.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 100, -1, 29));

        jLabel5.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jLabel5.setText("MODALIDAD DE PAGO");
        JPanel_DatosCredito.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 140, 180, 27));

        comboModoPago.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        comboModoPago.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "MENSUAL", "BIMENSUAL", "TRIMESTRAL" }));
        JPanel_DatosCredito.add(comboModoPago, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 140, 204, -1));

        jLabel6.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jLabel6.setText("GESTOR DE COBRO");
        JPanel_DatosCredito.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 162, 30));

        comboCobrador.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        JPanel_DatosCredito.add(comboCobrador, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 120, 220, -1));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CLIENTE", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.BELOW_TOP, new java.awt.Font("Consolas", 1, 16), new java.awt.Color(204, 51, 0))); // NOI18N
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("No. IDENTIFICACION");
        jPanel5.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 33, 143, -1));

        txtIdentidadCliente.setFont(new java.awt.Font("Arial", 1, 15)); // NOI18N
        txtIdentidadCliente.setToolTipText("DIGITA EL # DE DOCUMENTO Y PRESIONA \"ENTER\"");
        txtIdentidadCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdentidadClienteActionPerformed(evt);
            }
        });
        jPanel5.add(txtIdentidadCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 53, 130, -1));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("NOMBRES & APELLIDOS");
        jPanel5.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(171, 33, 230, -1));

        txtNomApeCliente.setEditable(false);
        txtNomApeCliente.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jPanel5.add(txtNomApeCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(161, 53, 280, -1));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("TELEFONO");
        jPanel5.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 88, 143, -1));

        txtTelCliente.setEditable(false);
        txtTelCliente.setFont(new java.awt.Font("Arial", 1, 15)); // NOI18N
        jPanel5.add(txtTelCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 108, 143, -1));

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("DIRECCION");
        jPanel5.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(163, 83, 260, -1));

        txtDirCliente.setEditable(false);
        txtDirCliente.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jPanel5.add(txtDirCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 109, 350, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/team.png"))); // NOI18N
        jButton1.setToolTipText("CLICK PARA CONSULTAR CLIENTES");
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 30, 60, 60));

        JPanel_DatosCredito.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 210, 516, 139));

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setLayout(new java.awt.GridLayout(3, 2));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/add-page.png"))); // NOI18N
        jButton3.setText("NUEVO CREDITO");
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton3);

        jButton12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/reload.png"))); // NOI18N
        jButton12.setText("MODIFICAR CREDITO");
        jButton12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton12.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton12);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/folder.png"))); // NOI18N
        jButton4.setText("RELACION DE CREDITOS");
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton4);

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/la-lucha-contra-la-caja-registradora-icono-4028-48.png"))); // NOI18N
        jButton8.setText("PAGOS CREDITO");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton8);

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/cancel.png"))); // NOI18N
        jButton7.setText("REFINANCIAR O RETIRAR CREDITO");
        jButton7.setToolTipText("REFINANCIAR O RETIRAR CREDITO");
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton7.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton7);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton6.setText("SALIR");
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton6);

        JPanel_DatosCredito.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(538, 210, 350, -1));

        jToolBar1.setRollover(true);
        jToolBar1.setToolTipText("ANTERIOR CREDITO ");

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/investigacion-icono-8491-48.png"))); // NOI18N
        jButton9.setToolTipText("CONSULTAR POR NUMERO DE CREDITO");
        jButton9.setFocusable(false);
        jButton9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton9.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton9);

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Back.png"))); // NOI18N
        jButton10.setToolTipText("CREDITO ANTERIOR");
        jButton10.setFocusable(false);
        jButton10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton10.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton10);

        jButton11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Next.png"))); // NOI18N
        jButton11.setToolTipText("SIGUIENTE CREDITO");
        jButton11.setFocusable(false);
        jButton11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton11.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton11);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/imprimir-icono-3650-32.png"))); // NOI18N
        jButton5.setToolTipText("IMPRIMIR CREDITO");
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton5.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/tablet-with-price-tag.png"))); // NOI18N
        jButton14.setToolTipText("DATOS CLIENTE PARA PEGAR A LA LETRA");
        jButton14.setFocusable(false);
        jButton14.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton14.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton14);

        JPanel_DatosCredito.add(jToolBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(335, 24, 250, 36));

        jLabel23.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jLabel23.setText("ESTADO DEL CREDITO");
        JPanel_DatosCredito.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 170, 170, 27));

        txtEstadoCredito.setEditable(false);
        txtEstadoCredito.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        txtEstadoCredito.setBorder(null);
        JPanel_DatosCredito.add(txtEstadoCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 170, 204, 29));

        jLabel24.setBackground(new java.awt.Color(153, 255, 255));
        jLabel24.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("PERIODO DE GARANTIA DEL ARTICULO EN MESES:");
        jLabel24.setOpaque(true);
        JPanel_DatosCredito.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(16, 351, 392, 30));

        JS_Garantia.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        JS_Garantia.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        JPanel_DatosCredito.add(JS_Garantia, new org.netbeans.lib.awtextra.AbsoluteConstraints(412, 351, 72, 30));

        jLabel22.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jLabel22.setText("TELEFONO COBRADOR");
        JPanel_DatosCredito.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 162, 30));

        txtTelCobrador.setEditable(false);
        txtTelCobrador.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        txtTelCobrador.setBorder(null);
        JPanel_DatosCredito.add(txtTelCobrador, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 150, 204, 29));

        JD_FechaInicioPago.setDateFormatString("EEEE dd MMM yyyy");
        JPanel_DatosCredito.add(JD_FechaInicioPago, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 100, 204, -1));

        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/logo_el_progreso_label.png"))); // NOI18N
        jLabel26.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel26.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        JPanel_DatosCredito.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 10, 280, 70));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "HISTORIAL DE CREDITOS", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 1, 16))); // NOI18N

        TB_HISTORIAL_CREDITOS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No. CREDITO", "FECHA EMISION", "ESTADO", "$ CREDITO", "$ SALDO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        TB_HISTORIAL_CREDITOS.setComponentPopupMenu(jPopupMenu2);
        TB_HISTORIAL_CREDITOS.setRowHeight(23);
        jScrollPane1.setViewportView(TB_HISTORIAL_CREDITOS);

        jLabel25.setText("CANTIDAD DE CREDITOS: ");

        LB_CANT_CREDITOS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LB_CANT_CREDITOS.setText("0");

        jLabel27.setText("TOTAL VR CREDITOS: ");

        txtTotalVrHistCredito.setEditable(false);
        txtTotalVrHistCredito.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTotalVrHistCredito.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtTotalVrHistCredito.setText("$ 0");

        jLabel28.setText("TOTAL VR SALDO: ");

        txtTotalVrHistSaldo.setEditable(false);
        txtTotalVrHistSaldo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTotalVrHistSaldo.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtTotalVrHistSaldo.setText("$ 0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LB_CANT_CREDITOS, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotalVrHistCredito)
                            .addComponent(txtTotalVrHistSaldo, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LB_CANT_CREDITOS))
                    .addComponent(txtTotalVrHistCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalVrHistSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DETALLE CREDITO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 1, 18))); // NOI18N
        jPanel4.setPreferredSize(new java.awt.Dimension(889, 50));

        TB_DETALLE_CREDITO.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ITEM", "REFERENCIA", "DESCRIPCIÓN", "CANTIDAD", "PRECIO UND", "CTA. INICIAL", "T. VENTA", "T. SALDO", "T. COMISIÓN", "T. INICIAL", "GARANTIA"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_DETALLE_CREDITO.setComponentPopupMenu(jPopupMenu1);
        TB_DETALLE_CREDITO.setRowHeight(23);
        TB_DETALLE_CREDITO.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(TB_DETALLE_CREDITO);
        if (TB_DETALLE_CREDITO.getColumnModel().getColumnCount() > 0) {
            TB_DETALLE_CREDITO.getColumnModel().getColumn(0).setMaxWidth(50);
            TB_DETALLE_CREDITO.getColumnModel().getColumn(1).setMaxWidth(60);
            TB_DETALLE_CREDITO.getColumnModel().getColumn(2).setMinWidth(150);
            TB_DETALLE_CREDITO.getColumnModel().getColumn(10).setMaxWidth(60);
        }

        btnProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/carrito-de-la-compra-icono-7565-64.png"))); // NOI18N
        btnProducto.setToolTipText("SELECCIONAR PRODUCTO");
        btnProducto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductoActionPerformed(evt);
            }
        });

        jLabel15.setText("REFERENCIA");

        txtReferencia.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        jLabel16.setText("DESCRIPCION");

        txtDescProducto.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        jLabel17.setText("CANTIDAD");

        txtCntProducto.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        txtCntProducto.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCntProducto.setText("0");

        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("PRECIO DE VENTA UND");

        txtPrecioVentaUnd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPrecioVentaUnd.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtPrecioVentaUnd.setText("$ 0");
        txtPrecioVentaUnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioVentaUndActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("VR CUOTA INICIAL UND");

        txtCtaIncialUnd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtCtaIncialUnd.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtCtaIncialUnd.setText("$ 0");
        txtCtaIncialUnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCtaIncialUndActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel21.setText("CANTIDAD DE ARTICULOS:");

        LB_CNT_ARTICULOS.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LB_CNT_ARTICULOS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LB_CNT_ARTICULOS.setText("0");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Download.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel31.setText("REGISTRADO POR:");

        LB_EMP_REGISTRA_CREDITO.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(btnProducto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(txtReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtDescProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtCntProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPrecioVentaUnd, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(txtCtaIncialUnd, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton2))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 863, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel21)
                        .addGap(5, 5, 5)
                        .addComponent(LB_CNT_ARTICULOS, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LB_EMP_REGISTRA_CREDITO, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProducto)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDescProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel15)
                        .addComponent(jLabel16))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(jLabel17))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPrecioVentaUnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCntProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtCtaIncialUnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(LB_EMP_REGISTRA_CREDITO, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LB_CNT_ARTICULOS, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TOTALES", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 1, 16), new java.awt.Color(204, 153, 0))); // NOI18N
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setBackground(new java.awt.Color(255, 255, 0));
        jLabel11.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel11.setText(" TOTAL VALOR CREDITO");
        jLabel11.setOpaque(true);
        jPanel6.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 210, 40));

        txtTotalVrCredito.setEditable(false);
        txtTotalVrCredito.setBackground(new java.awt.Color(255, 255, 0));
        txtTotalVrCredito.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        txtTotalVrCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalVrCredito.setText("$ 0");
        txtTotalVrCredito.setBorder(null);
        jPanel6.add(txtTotalVrCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 20, 220, 40));

        jLabel12.setBackground(new java.awt.Color(255, 255, 0));
        jLabel12.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("TOTAL VALOR CREDITO");
        jLabel12.setOpaque(true);
        jPanel6.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 210, 30));

        jLabel13.setBackground(new java.awt.Color(255, 153, 0));
        jLabel13.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText(" TOTAL VR CTA INICIAL");
        jLabel13.setOpaque(true);
        jPanel6.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 210, 30));

        jLabel14.setBackground(new java.awt.Color(255, 51, 51));
        jLabel14.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText(" TOTAL VALOR SALDO");
        jLabel14.setOpaque(true);
        jPanel6.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 210, 40));

        txtTotalVrCtaInicial.setEditable(false);
        txtTotalVrCtaInicial.setBackground(new java.awt.Color(255, 153, 0));
        txtTotalVrCtaInicial.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        txtTotalVrCtaInicial.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalVrCtaInicial.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalVrCtaInicial.setText("$ 0");
        txtTotalVrCtaInicial.setBorder(null);
        jPanel6.add(txtTotalVrCtaInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 60, 220, 30));

        jLabel18.setBackground(new java.awt.Color(0, 204, 204));
        jLabel18.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel18.setText(" TOTAL VALOR COMISIÓN");
        jLabel18.setOpaque(true);
        jPanel6.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 220, 40));

        txtTotalVrSaldo.setEditable(false);
        txtTotalVrSaldo.setBackground(new java.awt.Color(255, 51, 51));
        txtTotalVrSaldo.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        txtTotalVrSaldo.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalVrSaldo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalVrSaldo.setText("$ 0");
        txtTotalVrSaldo.setBorder(null);
        jPanel6.add(txtTotalVrSaldo, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 90, 220, 40));

        txtTotalVrComision.setEditable(false);
        txtTotalVrComision.setBackground(new java.awt.Color(0, 204, 204));
        txtTotalVrComision.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        txtTotalVrComision.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalVrComision.setText("$ 0");
        txtTotalVrComision.setBorder(null);
        jPanel6.add(txtTotalVrComision, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 130, 220, 40));

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PLAN DE PAGO CREDITO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 1, 16))); // NOI18N
        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TB_PLAN_PAGO.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No. LETRA", "FECHA PAGO", "Vr LETRA", "SALDO", "COMISIÓN", "ESTADO LETRA"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        TB_PLAN_PAGO.setRowHeight(23);
        jScrollPane3.setViewportView(TB_PLAN_PAGO);
        if (TB_PLAN_PAGO.getColumnModel().getColumnCount() > 0) {
            TB_PLAN_PAGO.getColumnModel().getColumn(0).setMaxWidth(60);
        }

        jPanel8.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 424, 140));

        js_Cnt_Letras.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        js_Cnt_Letras.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        jPanel8.add(js_Cnt_Letras, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 40, 60, -1));

        txtVrLetra.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtVrLetra.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtVrLetra.setText("$ 0");
        jPanel8.add(txtVrLetra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 110, -1));

        jLabel29.setText("CANT. LETRAS");
        jPanel8.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("VALOR LETRA");
        jPanel8.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 20, 100, -1));

        jToolBar2.setRollover(true);

        btnSavedPlanPago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar.png"))); // NOI18N
        btnSavedPlanPago.setToolTipText("Registrar Plan de Pagos");
        btnSavedPlanPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSavedPlanPagoActionPerformed(evt);
            }
        });
        jToolBar2.add(btnSavedPlanPago);

        btnAddCuota.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/Add.png"))); // NOI18N
        btnAddCuota.setToolTipText("Ingresar Cuotas Restantes");
        btnAddCuota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddCuotaActionPerformed(evt);
            }
        });
        jToolBar2.add(btnAddCuota);

        btn_prog_cta_inicial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/time-passing.png"))); // NOI18N
        btn_prog_cta_inicial.setToolTipText("Programar Pago Cta Inicial");
        btn_prog_cta_inicial.setFocusable(false);
        btn_prog_cta_inicial.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_prog_cta_inicial.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_prog_cta_inicial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_prog_cta_inicialActionPerformed(evt);
            }
        });
        jToolBar2.add(btn_prog_cta_inicial);

        btnEliminaPlanPago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/delete.png"))); // NOI18N
        btnEliminaPlanPago.setToolTipText("Eliminar Plan de Pagos");
        btnEliminaPlanPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminaPlanPagoActionPerformed(evt);
            }
        });
        jToolBar2.add(btnEliminaPlanPago);

        jPanel8.add(jToolBar2, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 30, 160, 40));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 895, Short.MAX_VALUE)
                    .addComponent(JPanel_DatosCredito, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 476, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(JPanel_DatosCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 397, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1370, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 672, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormListarClientes", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
            JInternalFrame ji = validador.getJInternalFrame(FormListarClientes.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormListarClientes(this, null, null, null);
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormListarClientes.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormListarClientes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        int SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas salir?");
        if (SI_NO == 0) {
            dispose();
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void btnProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductoActionPerformed
        // TODO add your handling code here:
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormListaProductos", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
            JInternalFrame ji = validador.getJInternalFrame(FormListaProductos.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormListaProductos(this);
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormListaProductos.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormListaProductos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_btnProductoActionPerformed

    private void txtPrecioVentaUndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioVentaUndActionPerformed
        // TODO add your handling code here:
        txtCtaIncialUnd.selectAll();
        txtCtaIncialUnd.requestFocus();
    }//GEN-LAST:event_txtPrecioVentaUndActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
//        if (VALIDAR_FORM_CREDITO() != false) {
        credito = CONSULTA_CREDITO(txtNumeroCredito.getText());
        if (credito != null) {
            int SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas realizar la modificacion?");
            if (SI_NO == 0) {
                if (creditoDao.CRUD_CREDITO(DATA_VALUE_CREDITO(1)) != false) {
                    edicion.mensajes(2, "modificacion realizada correctamente.");
                }
            }

        } else {
            edicion.mensajes(1, "el credito aun no hasido registrado.");
        }
//        }
    }//GEN-LAST:event_jButton12ActionPerformed

    private void txtCtaIncialUndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCtaIncialUndActionPerformed
        // TODO add your handling code here:
        GUARDAR_CREDITO();
    }//GEN-LAST:event_txtCtaIncialUndActionPerformed

    private void txtIdentidadClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdentidadClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdentidadClienteActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
        Object numCredito = edicion.msjQuest(2, "INGRESE EL NUMERO DEL CREDITO.");
        BUSCAR_CREDITO(numCredito);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        int numeroCredito = edicion.toNumeroEntero(txtNumeroCredito.getText());
        numeroCredito = numeroCredito - 1;
        if (numeroCredito > 0) {
            BUSCAR_CREDITO(numeroCredito);
        }
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        int numeroCredito = edicion.toNumeroEntero(txtNumeroCredito.getText());
        numeroCredito = numeroCredito + 1;
        if (numeroCredito > 0) {
            BUSCAR_CREDITO(numeroCredito);
        }
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        LIMPIAR_FORMULARIO_CREDITO();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        int SI_NO = (int) edicion.msjQuest(1, "esta seguro que desea eliminar el producto.");
        if (SI_NO == 0) {
            int row = TB_DETALLE_CREDITO.getSelectedRow();
            creditoDao.BORRAR_DETALLE_CREDITO(TB_DETALLE_CREDITO.getValueAt(row, 0));
            DETALLE_CALCULAR_TOTALES_CREDITO();
//            PLAN_DE_PAGO_CREDITO();
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormRelacionCredito", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {

            JInternalFrame ji = validador.getJInternalFrame(FormRelacionCredito.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormRelacionCredito();
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormRelacionCredito.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormRelacionCredito.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        report.GESTIONAR_CREDITO(txtNumeroCredito.getText());
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormPagoCredito",
                Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {

            JInternalFrame ji = validador.getJInternalFrame(FormPagoCredito.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormPagoCredito();
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormPagoCredito.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormPagoCredito.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        GUARDAR_CREDITO();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnSavedPlanPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSavedPlanPagoActionPerformed
        // TODO add your handling code here:
        CALCULAR_PLAN_DE_PAGOS_CREDITO();
    }//GEN-LAST:event_btnSavedPlanPagoActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        // TODO add your handling code here:
        Object[] values = {txtIdentidadCliente.getText(), txtNumeroCredito.getText()};
        report.DATOS_CLIENTE_PARA_PEGAR_A_LETRA(values);

    }//GEN-LAST:event_jButton14ActionPerformed

    private void btnAddCuotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddCuotaActionPerformed
        // TODO add your handling code here:
        try {
            if (TB_PLAN_PAGO.getRowCount() < 1) {
                edicion.mensajes(1, "el plan de pago a un no se ha registrado.");
                return;
            }
            if (edicion.toNumeroEntero(js_Cnt_Letras.getValue().toString()) < 1) {
                edicion.mensajes(1, "por favor seleccione la cantidad de letras adicionales.");
                return;
            }
            if (edicion.toNumeroEntero(txtVrLetra.getText()) < 1) {
                edicion.mensajes(1, "el valor de la letra debe ser mayor a cero(0).");
                return;
            }
            int row = TB_PLAN_PAGO.getRowCount() - 1;
            int cntLetras = edicion.toNumeroEntero(js_Cnt_Letras.getValue().toString());
            int VrLetra = edicion.toNumeroEntero(txtVrLetra.getText());
            int TotalvrSaldo = edicion.toNumeroEntero(TB_PLAN_PAGO.getValueAt(row, 3).toString());
            int totalVrLetra = VrLetra * cntLetras;
            if (totalVrLetra > TotalvrSaldo) {
                edicion.mensajes(3, "el valor total de la letra no puede superar el saldo "
                        + "pendiente.\nValor letra = (Cant. Cuotas x valor Letra) = $ " + formatoTexto.numerico(totalVrLetra) + "\nvalor Saldo = $ " + formatoTexto.numerico(TotalvrSaldo));
                return;
            }
            int VrComision = (VrLetra * empleado.getPorcComision()) / 100;
            Date fechaPago = edicion.CambiarTipoFecha(TB_PLAN_PAGO.getValueAt(row, 1));
            Date fechaPagoAnterior = edicion.CambiarTipoFecha(TB_PLAN_PAGO.getValueAt((row - 1), 1));
            Calendar fecha_De_pago = new GregorianCalendar();
            Calendar fecha_De_pago_Anterior = new GregorianCalendar();
            fecha_De_pago.setTime(fechaPago);
            fecha_De_pago_Anterior.setTime(fechaPagoAnterior);

            int dia = fecha_De_pago.get(Calendar.DAY_OF_MONTH);
            if (dia == 31) {
                edicion.mensajes(1, "por favor selecciona un dia diferente de " + dia + ".");
                return;
            }
            int diaFecha = fecha_De_pago_Anterior.get(Calendar.DAY_OF_MONTH);
            int mes = fecha_De_pago.get(Calendar.MONTH) + 1;
            int anio = fecha_De_pago.get(Calendar.YEAR);
            String modoPago = comboModoPago.getSelectedItem().toString();
            Object[] values = new Object[7];
            String fechaPagoFinal = null, fechaPagoLetra;
            for (int i = 1; i < cntLetras + 1; i++) {

                if (mes == 2 && dia >= 29) {
                    dia = 28;
                } else {
                    dia = diaFecha;
                }

                fechaPagoLetra = "" + edicion.AGREGAR_CEROS_LEFT(dia, 2) + "-" + edicion.AGREGAR_CEROS_LEFT(mes, 2) + "-" + anio + "";

                if ("MENSUAL".equals(modoPago)) {
                    mes = mes + 1;
                    fechaPagoFinal = "" + DateUtil.rollMonths(
                            DateUtil.getDateTime(edicion.CambiarTipoFecha(fechaPagoLetra)), 1);
                    if (mes > 12) {
                        anio++;
                        mes = 1;
                    }
                }

                if ("BIMENSUAL".equals(modoPago)) {
                    mes = mes + 2;
                    fechaPagoFinal = "" + DateUtil.rollMonths(
                            DateUtil.getDateTime(edicion.CambiarTipoFecha(fechaPagoLetra)), 2);
                    if (mes > 12) {
                        anio++;
                        fecha_De_pago.setTime(DateUtil.getDateTime(edicion.CambiarTipoFechaSQL(fechaPagoFinal)));
                        mes = fecha_De_pago.get(Calendar.MONTH) + 1;
                    }
                }

                if ("TRIMESTRAL".equals(modoPago)) {
                    mes = mes + 3;
                    fechaPagoFinal = "" + DateUtil.rollMonths(
                            DateUtil.getDateTime(edicion.CambiarTipoFecha(fechaPagoLetra)), 3);
                    if (mes > 12) {
                        anio++;
                        fecha_De_pago.setTime(DateUtil.getDateTime(edicion.CambiarTipoFechaSQL(fechaPagoFinal)));
                        mes = fecha_De_pago.get(Calendar.MONTH) + 1;
                    }
                }

                TotalvrSaldo = TotalvrSaldo - VrLetra;
                values[0] = txtNumeroCredito.getText();
                values[1] = "'" + fechaPagoFinal + "'";
                values[2] = VrLetra;
                values[3] = TotalvrSaldo;
                values[4] = VrComision;
                values[5] = Variables_Gloabales.EMPLEADO.getIdentificacion();
                values[6] = 1;

                creditoDao.CREATE_PLAN_PAGO_LETRA(values);
            }
            js_Cnt_Letras.setValue(0);
            txtVrLetra.setText("$ 0");
            CARGAR_PLAN_DE_PAGO_CREDITO();
        } catch (Exception e) {

            edicion.mensajes(3, "registre el plan de pago.");
        }
    }//GEN-LAST:event_btnAddCuotaActionPerformed

    private void btnEliminaPlanPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminaPlanPagoActionPerformed
        // TODO add your handling code here:
        if (TB_PLAN_PAGO.getRowCount() < 1) {
            edicion.mensajes(1, "el plan de pago a un no se ha registrado.");
        } else {
            int si_no = (int) edicion.msjQuest(1, "estas seguro que deseas eliminar el plan de pagos.");
            if (si_no == 0) {
                if (creditoDao.BORRAR_PLAN_PAGO_CREDITO(txtNumeroCredito.getText()) != false) {
                    btnSavedPlanPago.setEnabled(true);
                    btn_prog_cta_inicial.setEnabled(true);
                    CARGAR_PLAN_DE_PAGO_CREDITO();
                }
            }
        }
    }//GEN-LAST:event_btnEliminaPlanPagoActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        report.GESTIONAR_CREDITO(TB_HISTORIAL_CREDITOS.getValueAt(TB_HISTORIAL_CREDITOS.getSelectedRow(), 0));
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        int si_no = (int) edicion.msjQuest(1, "ESTAS SEGURO QUE DESEA REALIZAR ESTA OPERACIÓN? RECUERDE QUE NO PODRA RETROCEDER.");
        if (si_no == 0) {
            if (creditoDao.ESTADO_RETIRAR_ARTICULO_DEL_CREDITO(txtNumeroCredito.getText())) {
                Object msjObservacion = edicion.msjQuest(2, "ingrese una breve observación sobre el cliente. " + txtNomApeCliente.getText());
                if (creditoDao.MARCAR_OBSERVACION_CLIENTE_RETIRA_CREDITO("'" + msjObservacion + "'", txtIdentidadCliente.getText())) {
                    edicion.mensajes(1, "la operacion termino con exito. esto a marcado al cliente: " + txtNomApeCliente.getText() + ""
                            + "\npara que en el proximo credito evalue su capacidad de cumplir con la obligación entre otras recomendaciones.");
                }
            }
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void btn_prog_cta_inicialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_prog_cta_inicialActionPerformed
        // TODO add your handling code here:
        JInternalFrame ji = validador.getJInternalFrame(FormProgramarPagoCtaInicial.class.getName());

        if (ji == null || ji.isClosed()) {
            ji = new FormProgramarPagoCtaInicial(edicion.toNumeroEntero(txtTotalVrCredito.getText()),
                    edicion.toNumeroEntero(txtTotalVrCtaInicial.getText()), JD_FechaEmision.getDate(), this);
            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormProgramarPagoCtaInicial.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormProgramarPagoCtaInicial.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btn_prog_cta_inicialActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser JD_FechaEmision;
    private com.toedter.calendar.JDateChooser JD_FechaInicioPago;
    private javax.swing.JPanel JPanel_DatosCredito;
    private javax.swing.JSpinner JS_Garantia;
    private javax.swing.JLabel LB_CANT_CREDITOS;
    private javax.swing.JLabel LB_CNT_ARTICULOS;
    private javax.swing.JLabel LB_EMP_REGISTRA_CREDITO;
    private javax.swing.JTable TB_DETALLE_CREDITO;
    private javax.swing.JTable TB_HISTORIAL_CREDITOS;
    private javax.swing.JTable TB_PLAN_PAGO;
    private javax.swing.JButton btnAddCuota;
    private javax.swing.JButton btnEliminaPlanPago;
    private javax.swing.JButton btnProducto;
    private javax.swing.JButton btnSavedPlanPago;
    public javax.swing.JButton btn_prog_cta_inicial;
    private javax.swing.JComboBox comboCobrador;
    private javax.swing.JComboBox comboModoPago;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenu2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JSpinner js_Cnt_Letras;
    public javax.swing.JTextField txtCntProducto;
    public javax.swing.JTextField txtCtaIncialUnd;
    public javax.swing.JTextField txtDescProducto;
    public javax.swing.JTextField txtDirCliente;
    private javax.swing.JTextField txtEstadoCredito;
    public javax.swing.JTextField txtIdentidadCliente;
    public javax.swing.JTextField txtNomApeCliente;
    private javax.swing.JTextField txtNumeroCredito;
    public javax.swing.JTextField txtPrecioVentaUnd;
    public javax.swing.JTextField txtReferencia;
    public javax.swing.JTextField txtTelCliente;
    private javax.swing.JTextField txtTelCobrador;
    private javax.swing.JTextField txtTotalVrComision;
    private javax.swing.JTextField txtTotalVrCredito;
    private javax.swing.JTextField txtTotalVrCtaInicial;
    private javax.swing.JTextField txtTotalVrHistCredito;
    private javax.swing.JTextField txtTotalVrHistSaldo;
    private javax.swing.JTextField txtTotalVrSaldo;
    private javax.swing.JTextField txtVrLetra;
    // End of variables declaration//GEN-END:variables

    private void CARGAR_COBRADORES() {
        comboCobrador.addItem(null);
        for (Empleado Lista_Cobradores : empleadoDao.LISTAR_EMPLEADOS_POR_CARGO("7, 'GESTOR DE COBRO'")) {
            comboCobrador.addItem(Lista_Cobradores.getNombres() + " " + Lista_Cobradores.getApellidos());
        }
    }

    private void NUEVO_CREDITO() {
        Object[] values = {creditoDao.NUMERO_CREDITO(), Variables_Gloabales.EMPLEADO.getIdentificacion()};
        creditoDao.CREATE_CONSECUTIVO_CREDITO(values);
        txtNumeroCredito.setText(creditoDao.NUMERO_CREDITO());
        JD_FechaEmision.setDate(DateUtil.newDateTime());
        JD_FechaInicioPago.setDate(DateUtil.newDateTime());
    }

    private void ACCIONES_FML_CREDITO() {
        comboCobrador.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    empleado = new Empleado();
                    empleado = empleadoDao.CONSULTAR_EMPLEADO("" + comboCobrador.getSelectedItem() + "");
                    txtTelCobrador.setText(empleado.getTelefono());
                } else {
                    txtTelCobrador.setText("");
                }
            }
        });

        txtIdentidadCliente.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                CONSULTAR_CLIENTE(txtIdentidadCliente.getText());
            }

        });

        txtReferencia.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                CONSULTAR_PRODUCTO("1," + txtReferencia.getText());
            }
        });

        txtPrecioVentaUnd.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                txtPrecioVentaUnd.setText("$ " + formatoTexto.numerico(edicion.toNumeroEntero(txtPrecioVentaUnd.getText())));
            }
        });

        txtVrLetra.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                txtVrLetra.setText("$ " + formatoTexto.numerico(edicion.toNumeroEntero(txtVrLetra.getText())));
            }
        });
        txtCtaIncialUnd.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                txtCtaIncialUnd.setText("$ " + formatoTexto.numerico(edicion.toNumeroEntero(txtCtaIncialUnd.getText())));
            }
        });

    }

    private void CONSULTAR_PRODUCTO(Object referencia) {
        producto = new Producto();
        producto = productoDao.READ_PRODUCTO(referencia);
        if (producto != null) {
            CARGAR_DATA_PRODUCTO();
        } else {
            int SI_NO = (int) edicion.msjQuest(1, "el producto con # de referencia " + txtReferencia.getText() + " no se encuentra registrado."
                    + " deseas registrarlo?");
            if (SI_NO == 0) {
                JInternalFrame ji = validador.getJInternalFrame(FormProducto.class.getName());

                if (ji == null || ji.isClosed()) {
                    ji = new FormProducto();
                    FormProducto.txtreferencia.setText(referencia.toString());
                    ControllerContenedor.getjDesktopPane1().add(ji, 0);
                    validador.addJIframe(FormProducto.class.getName(), ji);
                    ji.setVisible(true);
                } else {
                    ji.show(true);
                    try {
                        ji.setIcon(false);
                    } catch (PropertyVetoException ex) {
                        Logger.getLogger(FormProducto.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    private void CARGAR_DATA_PRODUCTO() {
        txtReferencia.setText(producto.getReferencia());
        txtDescProducto.setText(producto.getDescripcion());
        txtCtaIncialUnd.setText("$ " + formatoTexto.numerico(producto.getCuotaInicial()));
        txtCntProducto.setText("1");
        txtPrecioVentaUnd.selectAll();
        txtPrecioVentaUnd.requestFocus();
    }
    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();

    private boolean VALIDAR_FORM_CREDITO() {

        if (comboCobrador.getSelectedItem() == null) {
            edicion.mensajes(1, "selecciona el cobrador encargado del credito.");
            comboCobrador.grabFocus();
            return false;
        }
        if (JD_FechaInicioPago.getDate() == null) {
            edicion.mensajes(1, "selecciona la fecha en que se inician los pagos del credito.");
            JD_FechaInicioPago.grabFocus();
            return false;
        }
        if (comboModoPago.getSelectedItem().toString().isEmpty() == true | comboModoPago.getSelectedItem() == null) {
            edicion.mensajes(1, "selecciona la modalidad de pago del credito.");
            comboModoPago.grabFocus();
            return false;
        }

        if (txtIdentidadCliente.getText().isEmpty() | txtIdentidadCliente.getText() == null) {
            edicion.mensajes(1, "por favor ingresa el # de documento del cliente.");
            txtIdentidadCliente.grabFocus();
            return false;
        }
        if (edicion.toNumeroEntero(JS_Garantia.getValue().toString()) < 1 && TB_DETALLE_CREDITO.getRowCount() < 1) {
            edicion.mensajes(1, "especifique la cantidad de meses de garantia del producto acreditado.");
            JS_Garantia.grabFocus();
            return false;
        }
        if (txtReferencia.getText().isEmpty() | txtReferencia.getText() == null) {
            edicion.mensajes(1, "por favor selecciona el producto que desea acreditar el cliente " + txtNomApeCliente.getText() + ".");
            return false;
        }

        if (edicion.toNumeroEntero(txtPrecioVentaUnd.getText()) < edicion.toNumeroEntero(txtCtaIncialUnd.getText())) {
            edicion.mensajes(1, "el precio de venta no puede ser menor a la cuota inicial.");
            return false;
        }

        if (edicion.toNumeroEntero(txtPrecioVentaUnd.getText()) < 1 && TB_DETALLE_CREDITO.getRowCount() < 1) {
            edicion.mensajes(1, "el precio de venta debe ser mayor a cero(0).");
            return false;
        }

        if (edicion.toNumeroEntero(txtCntProducto.getText()) < 1 && TB_DETALLE_CREDITO.getRowCount() < 1) {
            edicion.mensajes(1, "la cantidad debe ser mayor a cero(0).");
            return false;
        }

        return true;
    }

    /* private void PLAN_DE_PAGO_CREDITO() {
     edicion.limpiar_tablas(TB_PLAN_PAGO);
     int cntCuotas = edicion.toNumeroEntero(JS_CntCuotas.getValue().toString());
     int totalVrSaldo = edicion.toNumeroEntero(txtTotalVrSaldo.getText());
     int comisionXletra;
     Calendar fechaInicioPago = new GregorianCalendar();
     fechaInicioPago.setTime(JD_FechaInicioPago.getDate());
     DefaultTableModel tableModel = (DefaultTableModel) TB_PLAN_PAGO.getModel();
     int dia = fechaInicioPago.get(Calendar.DAY_OF_MONTH);
     int diaFecha = fechaInicioPago.get(Calendar.DAY_OF_MONTH);
     int mes = fechaInicioPago.get(Calendar.MONTH) + 1;
     int anio = fechaInicioPago.get(Calendar.YEAR);
     String estadoLetra = "PENDIENTE";
     Object[] valuesPlan = new Object[7];
     int valorCuota = totalVrSaldo / cntCuotas;
     for (int i = 1; i <= cntCuotas; i++) {
     totalVrSaldo = totalVrSaldo - valorCuota;
     comisionXletra = (valorCuota * empleado.getPorcComision()) / 100;
     mes = mes + 1;
     if (mes == 13) {
     mes = 1;
     anio++;
     }

     if (mes == 2 && dia == 30 | dia == 29) {
     dia = 28;
     } else {
     dia = diaFecha;
     }
     String fechaPago = "" + anio + "-" + edicion.AGREGAR_CEROS_LEFT(mes, 2) + "-" + edicion.AGREGAR_CEROS_LEFT(dia, 2) + "";
     valuesPlan[0] = edicion.AGREGAR_CEROS_LEFT(i, 2);
     valuesPlan[1] = fechaPago;
     valuesPlan[2] = valorCuota;
     valuesPlan[3] = totalVrSaldo;
     valuesPlan[4] = comisionXletra;
     valuesPlan[5] = estadoLetra;
     valuesPlan[6] = 2;
     tableModel.addRow(valuesPlan);
     }
     DATA_VALUE_PLAN_DE_PAGO_CREDITO();
     }
     */
    private Object[] DATA_VALUE_CREDITO(int opcion) {
        Object[] values = new Object[13];
        values[0] = opcion;
        values[1] = txtNumeroCredito.getText();
        values[2] = "'" + edicion.formatearFechaSQL(JD_FechaEmision.getDate()) + "'";
        values[3] = "'" + edicion.formatearFechaSQL(JD_FechaInicioPago.getDate()) + "'";
        values[4] = 0;
        values[5] = "'" + comboModoPago.getSelectedItem() + "'";
        values[6] = edicion.toNumeroEntero(txtTotalVrCredito.getText());
        values[7] = edicion.toNumeroEntero(txtTotalVrCtaInicial.getText());
        values[8] = edicion.toNumeroEntero(txtTotalVrSaldo.getText());
        values[9] = edicion.toNumeroEntero(txtTotalVrComision.getText());
        values[10] = "'" + txtIdentidadCliente.getText() + "'";
        values[11] = "'" + empleado.getIdentificacion() + "'";
        values[12] = "'" + Variables_Gloabales.EMPLEADO.getIdentificacion() + "'";

        return values;
    }

    private Object[] DATA_VALUE_DETALLE_CREDITO() {
        Object[] values = new Object[8];
        values[0] = edicion.toNumeroEntero(txtNumeroCredito.getText());
        values[1] = "'" + txtReferencia.getText() + "'";
        values[2] = edicion.toNumeroEntero(txtCntProducto.getText());
        values[3] = edicion.toNumeroEntero(txtPrecioVentaUnd.getText());
        values[4] = edicion.toNumeroEntero(txtCtaIncialUnd.getText());
        values[5] = empleado.getPorcComision();
        values[6] = edicion.toNumeroEntero(JS_Garantia.getValue().toString());
        values[7] = Variables_Gloabales.EMPLEADO.getIdentificacion();
        return values;
    }

    private void DATA_VALUE_PLAN_DE_PAGO_CREDITO() {
        Object[] values = new Object[6];
        creditoDao.BORRAR_PLAN_PAGO_CREDITO(txtNumeroCredito.getText());
        for (int i = 0; i < TB_PLAN_PAGO.getRowCount(); i++) {
            values[0] = edicion.toNumeroEntero(txtNumeroCredito.getText());
            values[1] = "'" + TB_PLAN_PAGO.getValueAt(i, 1) + "'";
            values[2] = TB_PLAN_PAGO.getValueAt(i, 2);
            values[3] = TB_PLAN_PAGO.getValueAt(i, 3);
            values[4] = TB_PLAN_PAGO.getValueAt(i, 4);
            values[5] = Variables_Gloabales.EMPLEADO.getIdentificacion();
            creditoDao.CREATE_PLAN_PAGO_LETRA(values);
        }
        CARGAR_PLAN_DE_PAGO_CREDITO();
    }

    public void CONSULTAR_CLIENTE(Object idCliente) {
        cliente = clienteDao.CONSULTAR_CLIENTE("1, " + idCliente);
        if (cliente != null) {
            CARGAR_DATA_CLIENTE();
        } else {
            int SI_NO = (int) edicion.msjQuest(1, "el cliente con # cc " + txtIdentidadCliente.getText() + " no se encuentra registrado."
                    + " deseas registrarlo?");
            if (SI_NO == 0) {

                JInternalFrame ji = validador.getJInternalFrame(FormCliente.class.getName());
                if (ji == null || ji.isClosed()) {
                    ji = new FormCliente();
                    FormCliente.txtidentificacion.setText(txtIdentidadCliente.getText());
                    ControllerContenedor.getjDesktopPane1().add(ji, 0);
                    validador.addJIframe(FormCliente.class.getName(), ji);
                    ji.setVisible(true);
                } else {
                    ji.show(true);
                    try {
                        ji.setIcon(false);
                    } catch (PropertyVetoException ex) {
                        Logger.getLogger(FormCliente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    private void CARGAR_DATA_CLIENTE() {
        txtIdentidadCliente.setText(cliente.getIdentificacion());
        txtNomApeCliente.setText(cliente.getNombres() + " " + cliente.getApellidos());
        txtTelCliente.setText(cliente.getTelefono1());
        txtDirCliente.setText(cliente.getDireccion1());
        CARGAR_HISTORIAL_CREDITO_CLIENTE();
    }

    private void CARGAR_HISTORIAL_CREDITO_CLIENTE() {
        if (cliente != null) {
            edicion.llenarTabla(TB_HISTORIAL_CREDITOS,
                    creditoDao.PLAN_DE_PAGO_CREDITO("5," + cliente.getIdentificacion()));
        } else {
            edicion.limpiar_tablas(TB_HISTORIAL_CREDITOS);
        }

        edicion.calcula_total(TB_HISTORIAL_CREDITOS, LB_CANT_CREDITOS, txtTotalVrHistCredito, 3);
        edicion.calcula_total(TB_HISTORIAL_CREDITOS, LB_CANT_CREDITOS, txtTotalVrHistSaldo, 4);
    }

    private void DETALLE_CALCULAR_TOTALES_CREDITO() {
        edicion.llenarTabla(TB_DETALLE_CREDITO, creditoDao.DETALLE_CREDITO("3, " + txtNumeroCredito.getText()));
        edicion.calcula_total(TB_DETALLE_CREDITO, LB_CNT_ARTICULOS, txtTotalVrCredito, 6);
        edicion.calcula_total(TB_DETALLE_CREDITO, LB_CNT_ARTICULOS, txtTotalVrSaldo, 7);
        edicion.calcula_total(TB_DETALLE_CREDITO, LB_CNT_ARTICULOS, txtTotalVrComision, 8);
        edicion.calcula_total(TB_DETALLE_CREDITO, LB_CNT_ARTICULOS, txtTotalVrCtaInicial, 9);
    }

    private void BUSCAR_CREDITO(Object ID_CREDITO) {
        credito = CONSULTA_CREDITO(ID_CREDITO);
        if (credito != null) {
            LOAD_COMPONET_CREDITO();
        }
    }

    private void LOAD_COMPONET_CREDITO() {
        txtNumeroCredito.setText(edicion.AGREGAR_CEROS_LEFT(credito.getNumeroCredito(), 6));
        JD_FechaEmision.setDate(credito.getFechaEmision());
        JD_FechaInicioPago.setDate(credito.getFechaFinal());
        if (credito.getEmpleadoRegistra() != null) {
            LB_EMP_REGISTRA_CREDITO.setText(credito.getEmpleadoRegistra().getNombres() + " " + credito.getEmpleadoRegistra().getApellidos());
        }
        if (credito.getEstadoCredito() != null) {
            txtEstadoCredito.setText(credito.getEstadoCredito().getDescripcion());
            if (credito.getEstadoCredito().getIdEstadoCredito() == 1) {
                ESTADO_BLOQUEO_COMPONET = true;
            } else {
                ESTADO_BLOQUEO_COMPONET = false;
            }
        }
        if (credito.getCobrador() != null) {
            comboCobrador.setSelectedItem(credito.getCobrador().getNombres() + " " + credito.getCobrador().getApellidos());
        }
        comboModoPago.setSelectedItem(credito.getModalidadPago());
        cliente = credito.getCliente();
        if (cliente != null) {
            CARGAR_DATA_CLIENTE();
        }

        DETALLE_CALCULAR_TOTALES_CREDITO();
        CARGAR_PLAN_DE_PAGO_CREDITO();
        BLOQUEAR_COMPONENTES(ESTADO_BLOQUEO_COMPONET);
        if (TB_PLAN_PAGO.getRowCount() > 1) {
            btnSavedPlanPago.setEnabled(false);
        }
    }

    private void CARGAR_PLAN_DE_PAGO_CREDITO() {
        edicion.llenarTabla(TB_PLAN_PAGO, creditoDao.PLAN_DE_PAGO_CREDITO("6, " + txtNumeroCredito.getText()));
    }

    private void LIMPIAR_FORMULARIO_CREDITO() {
        if (VALIDAR_PLAN_DE_PAGOS() != false) {

            credito = new Credito();
            producto = new Producto();
            ESTADO_BLOQUEO_COMPONET = true;
            LOAD_COMPONET_CREDITO();
            txtIdentidadCliente.setText("");
            txtNomApeCliente.setText("");
            txtTelCliente.setText("");
            txtDirCliente.setText("");
            comboCobrador.setSelectedItem(null);
            txtEstadoCredito.setText("");
            JS_Garantia.setValue(0);
            js_Cnt_Letras.setValue(0);
            txtVrLetra.setText("$ 0");
            txtReferencia.setText(producto.getReferencia());
            txtDescProducto.setText(producto.getDescripcion());
            txtCntProducto.setText("0");
            txtPrecioVentaUnd.setText("$ " + producto.getPrecio_venta());
            txtCtaIncialUnd.setText("$ " + producto.getCuotaInicial());
            edicion.limpiar_tablas(TB_PLAN_PAGO);
            CARGAR_HISTORIAL_CREDITO_CLIENTE();
            NUEVO_CREDITO();
            btnSavedPlanPago.setEnabled(true);
            LB_EMP_REGISTRA_CREDITO.setText(null);
        }
    }

    private void CRUD_DETALLE_CREDITO() {
        if (creditoDao.CREATE_DETALLE_CREDITO(DATA_VALUE_DETALLE_CREDITO()) != false) {
            BUSCAR_CREDITO(txtNumeroCredito.getText());
        }

    }

    private void BLOQUEAR_COMPONENTES(boolean estadoComponet) {
        btnProducto.setEnabled(estadoComponet);
        txtReferencia.setEnabled(estadoComponet);
        jMenuItem1.setEnabled(estadoComponet);
        JD_FechaEmision.setEnabled(estadoComponet);
        JD_FechaInicioPago.setEnabled(estadoComponet);
        btnSavedPlanPago.setEnabled(estadoComponet);
        btnAddCuota.setEnabled(estadoComponet);
        btnEliminaPlanPago.setEnabled(estadoComponet);
        btn_prog_cta_inicial.setEnabled(estadoComponet);
    }

    private Credito CONSULTA_CREDITO(Object ID_CREDITO) {
        return creditoDao.CONSULTA_CREDITO("2, " + ID_CREDITO);
    }

    private void GUARDAR_CREDITO() {
        if (VALIDAR_FORM_CREDITO() != false) {
            int si_no = (int) edicion.msjQuest(1, "ESTAS SEGURO QUE DESEA REGISTRAR EL CREDITO?");
            if (si_no == 0) {
                credito = CONSULTA_CREDITO(txtNumeroCredito.getText());
                if (credito == null) {
                    if (creditoDao.CRUD_CREDITO(DATA_VALUE_CREDITO(0)) != false) {
                        CRUD_DETALLE_CREDITO();
                    }
                } else {
                    CRUD_DETALLE_CREDITO();
                }
                txtReferencia.setText("");
                txtDescProducto.setText("");
                txtCntProducto.setText("0");
                txtPrecioVentaUnd.setText("$ 0");
                txtCtaIncialUnd.setText("$ 0");
            }
        }
    }

    public void CALCULAR_PLAN_DE_PAGOS_CREDITO() {
        if (edicion.toNumeroEntero(js_Cnt_Letras.getValue().toString()) < 1) {
            edicion.mensajes(1, "por favor seleccione la cantidad de letras adicionales.");
            return;
        }
        if (edicion.toNumeroEntero(txtVrLetra.getText()) < 1) {
            edicion.mensajes(1, "el valor de la letra debe ser mayor a cero(0).");
            return;
        }
        int TotalVrCuotas = edicion.toNumeroEntero(js_Cnt_Letras.getValue().toString()) * edicion.toNumeroEntero(txtVrLetra.getText());
        if (TotalVrCuotas > edicion.toNumeroEntero(txtTotalVrSaldo.getText())) {
            edicion.mensajes(3, "el valor total de las cuotas establecidas "
                    + "no puede ser mayor al saldo.\nTotal Vr Cuotas  =  (Cant. Cuotas x valor Letra) = $ " + formatoTexto.numerico(TotalVrCuotas)
                    + "\nTotal vr Saldo = $ " + formatoTexto.numerico(edicion.toNumeroEntero(txtTotalVrSaldo.getText())));
            return;
        }
        int row = TB_PLAN_PAGO.getRowCount() - 1;
        int cntLetras = edicion.toNumeroEntero(js_Cnt_Letras.getValue().toString());
        int VrLetra = edicion.toNumeroEntero(txtVrLetra.getText());
        if (row > 0) {
            int TotalvrSaldoAcum = edicion.toNumeroEntero(TB_PLAN_PAGO.getValueAt(row, 3).toString());
            int totalVrLetra = VrLetra * cntLetras;
            if (totalVrLetra > TotalvrSaldoAcum) {
                edicion.mensajes(3, "el valor total de la letra no puede superar el saldo "
                        + "pendiente.\nValor letra = (Cant. Cuotas x valor Letra) = $ " + formatoTexto.numerico(totalVrLetra) + "\nvalor Saldo = $ " + formatoTexto.numerico(TotalvrSaldoAcum));
                return;
            }
        }

        int TotalvrSaldo = edicion.toNumeroEntero(txtTotalVrSaldo.getText());
        int VrComision = (VrLetra * empleado.getPorcComision()) / 100;

        Calendar fechaInicioPago = new GregorianCalendar();
        fechaInicioPago.setTime(JD_FechaInicioPago.getDate());
        int dia = fechaInicioPago.get(Calendar.DAY_OF_MONTH);
        if (dia == 31) {
            edicion.mensajes(1, "por favor selecciona un dia diferente de " + dia + ".");
            return;
        }
        int diaFecha = fechaInicioPago.get(Calendar.DAY_OF_MONTH);
        int mes = fechaInicioPago.get(Calendar.MONTH) + 1;
        int anio = fechaInicioPago.get(Calendar.YEAR);
        String modoPago = comboModoPago.getSelectedItem().toString();
        Object[] values = new Object[7];
        String fechaPago, fechaPagoFinal = null;
        for (int i = 1; i < cntLetras + 1; i++) {

            if (mes == 2 && dia >= 29) {
                dia = 28;
            } else {
                fechaInicioPago.setTime(JD_FechaInicioPago.getDate());
                dia = fechaInicioPago.get(Calendar.DAY_OF_MONTH);
            }

            fechaPago = "" + edicion.AGREGAR_CEROS_LEFT(dia, 2) + "-" + edicion.AGREGAR_CEROS_LEFT(mes, 2) + "-" + anio + "";

            if ("MENSUAL".equals(modoPago)) {
                mes = mes + 1;
                fechaPagoFinal = "" + DateUtil.rollMonths(
                        DateUtil.getDateTime(edicion.CambiarTipoFecha(fechaPago)), 1);
                if (mes > 12) {
                    anio++;
                    mes = 1;
                }
            }

            if ("BIMENSUAL".equals(modoPago)) {
                mes = mes + 2;
                fechaPagoFinal = "" + DateUtil.rollMonths(
                        DateUtil.getDateTime(edicion.CambiarTipoFecha(fechaPago)), 2);
                if (mes > 12) {
                    anio++;
                    fechaInicioPago.setTime(DateUtil.getDateTime(edicion.CambiarTipoFechaSQL(fechaPagoFinal)));
                    mes = fechaInicioPago.get(Calendar.MONTH) + 1;
                }
            }

            if ("TRIMESTRAL".equals(modoPago)) {
                mes = mes + 3;
                fechaPagoFinal = "" + DateUtil.rollMonths(
                        DateUtil.getDateTime(edicion.CambiarTipoFecha(fechaPago)), 3);
                if (mes > 12) {
                    anio++;
                    fechaInicioPago.setTime(DateUtil.getDateTime(edicion.CambiarTipoFechaSQL(fechaPagoFinal)));
                    mes = fechaInicioPago.get(Calendar.MONTH) + 1;
                }
            }

            TotalvrSaldo = TotalvrSaldo - VrLetra;

            values[0] = txtNumeroCredito.getText();
            values[1] = "'" + fechaPagoFinal + "'";
            values[2] = VrLetra;
            values[3] = TotalvrSaldo;
            values[4] = VrComision;
            values[5] = Variables_Gloabales.EMPLEADO.getIdentificacion();
            values[6] = 1;

            creditoDao.CREATE_PLAN_PAGO_LETRA(values);
        }
        btnSavedPlanPago.setEnabled(false);
        js_Cnt_Letras.setValue(0);

        txtVrLetra.setText("$ 0");
        CARGAR_PLAN_DE_PAGO_CREDITO();

    }

    private boolean VALIDAR_PLAN_DE_PAGOS() {
        credito = creditoDao.CONSULTA_CREDITO("2, " + txtNumeroCredito.getText());
        if (credito != null && TB_PLAN_PAGO.getRowCount() < 1) {
            edicion.mensajes(1, "por favor crea un plan de pagos para el credito.");
            return false;
        }

        return true;
    }

    public void PROGRAMAR_PAGO_CUOTA_INICIAL(Date fechapago, int valorCta) {
        int valorSaldo = edicion.toNumeroEntero(txtTotalVrCredito.getText()) - valorCta;
        Object[] values = new Object[7];
        values[0] = txtNumeroCredito.getText();
        values[1] = "'" + edicion.formatearFechaSQL(fechapago) + "'";
        values[2] = valorCta;
        values[3] = valorSaldo;
        values[4] = 0;
        values[5] = Variables_Gloabales.EMPLEADO.getIdentificacion();
        values[6] = 2;
        creditoDao.CREATE_PLAN_PAGO_LETRA(values);
        CARGAR_PLAN_DE_PAGO_CREDITO();
    }

}

class validador {

    public static java.util.HashMap<String, javax.swing.JInternalFrame> jIframes = new java.util.HashMap<>();

    public static void addJIframe(String key, javax.swing.JInternalFrame jiframe) {
        jIframes.put(key, jiframe);
    }

    public static javax.swing.JInternalFrame getJInternalFrame(String key) {
        return jIframes.get(key);
    }

}
