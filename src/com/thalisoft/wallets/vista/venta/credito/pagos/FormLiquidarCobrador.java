package com.thalisoft.wallets.vista.venta.credito.pagos;

import com.thalisoft.wallets.model.cartera.pago.PagosClienteDao;
import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.util.report.Manager_Report;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 *
 * @author ThaliSoft
 */
public class FormLiquidarCobrador extends javax.swing.JInternalFrame {

    EmpleadoDao empleadoDao;
    PagosClienteDao pagosClienteDao;
    Edicion edicion = new Edicion();
    Empleado RECAUDADOR;

    public FormLiquidarCobrador() {
        empleadoDao = new EmpleadoDao();
        pagosClienteDao = new PagosClienteDao();
        RECAUDADOR = new Empleado();
        initComponents();
        Edicion.CENTRAR_VENTANA(this);
        CARGAR_RECAUDADORES();
        JD_FECHA1.setDate(DateUtil.newDateTime());
        JD_FECHA2.setDate(DateUtil.newDateTime());

        comboRecaudador.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    RECAUDADOR = empleadoDao.CONSULTAR_EMPLEADO("" + comboRecaudador.getSelectedItem() + "");
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JD_FECHA1 = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        comboRecaudador = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        LB_CNT_LETRAS = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        TXTTOTALRECAUDO = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        TXTTOTALCOMISION = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        JD_FECHA2 = new com.toedter.calendar.JDateChooser();

        setIconifiable(true);
        setTitle("Liquidar Gestor de Cobro");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LIQUIDACION DEL GESTOR DE COBRO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel1.setText("FECHA DE LIQUIDACIÓN");

        JD_FECHA1.setDateFormatString("EEEE dd MMMM yyyy");

        jLabel2.setText("SELECIONAR GESTOR DE COBRO");

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/logo_el_progreso_label.png"))); // NOI18N

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No. Comprobante", "Fecha Pago", "Cliente", "No. Credito / No. Letra", "Articulos", "Vr. Letra", "Vr. Recaudo", "Vr. Comision", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setRowHeight(22);
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(2).setMinWidth(300);
            jTable1.getColumnModel().getColumn(3).setMinWidth(120);
            jTable1.getColumnModel().getColumn(4).setMinWidth(220);
            jTable1.getColumnModel().getColumn(8).setMinWidth(150);
        }

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TOTALES", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(51, 153, 255))); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("CANTIDAD DE LETRAS A LIQUIDAR");

        LB_CNT_LETRAS.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LB_CNT_LETRAS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LB_CNT_LETRAS.setText("0");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("TOTAL VR RECAUDO");

        TXTTOTALRECAUDO.setEditable(false);
        TXTTOTALRECAUDO.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        TXTTOTALRECAUDO.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        TXTTOTALRECAUDO.setText("$ 0");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("TOTAL VR COMISIÓN");

        TXTTOTALCOMISION.setEditable(false);
        TXTTOTALCOMISION.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        TXTTOTALCOMISION.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        TXTTOTALCOMISION.setText("$ 0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LB_CNT_LETRAS, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(27, 27, 27))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(24, 24, 24)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(TXTTOTALCOMISION, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                    .addComponent(TXTTOTALRECAUDO))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(TXTTOTALRECAUDO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(TXTTOTALCOMISION, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LB_CNT_LETRAS, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/search.png"))); // NOI18N
        jButton1.setText("Consultar");
        jButton1.setFocusable(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/de-pago-con-tarjeta-de-credito-icono-3855-48.png"))); // NOI18N
        jButton2.setText("Liquidar");
        jButton2.setFocusable(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/printer.png"))); // NOI18N
        jButton3.setText("Comprobante");
        jButton3.setFocusable(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton4.setText("Salir");
        jButton4.setFocusable(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        JD_FECHA2.setDateFormatString("EEEE dd MMMM yyyy");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(216, 216, 216)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1167, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(comboRecaudador, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(JD_FECHA1, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(JD_FECHA2, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(JD_FECHA1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(JD_FECHA2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(comboRecaudador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(42, 42, 42)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {

            if (comboRecaudador.getSelectedItem() != "" | comboRecaudador.getSelectedItem() != null) {
                String values = "3, '" + edicion.formatearFechaSQL(JD_FECHA1.getDate()) + "', "
                        + "'" + edicion.formatearFechaSQL(JD_FECHA2.getDate()) + "'," + RECAUDADOR.getIdentificacion();
                CARGAR_COMPROBANTES_PARA_LIQUIDAR(values);
                if (jTable1.getRowCount() < 1) {
                    edicion.mensajes(1, "el " + RECAUDADOR.getCargo() + " " + RECAUDADOR.getNombres() + " " + RECAUDADOR.getApellidos() + " "
                            + "no tiene comprobantes pendientes para liquidar\nen el periodo de fechas establecido: " + edicion.formatearFecha(JD_FECHA1.getDate()) + " al " + edicion.formatearFecha(JD_FECHA2.getDate()));
                }
            }

        } catch (Exception e) {
            edicion.mensajes(1, "seleccione un gestor de cobro.");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        try {

            if (!comboRecaudador.getSelectedItem().equals("") | comboRecaudador.getSelectedItem() != null) {
                if (jTable1.getRowCount() < 1) {
                    edicion.mensajes(1, "el " + RECAUDADOR.getCargo() + " " + RECAUDADOR.getNombres() + " " + RECAUDADOR.getApellidos() + " "
                            + "no tiene comprobantes pendientes para liquidar\nen el periodo de fechas establecido: " + edicion.formatearFecha(JD_FECHA1.getDate()) + " al " + edicion.formatearFecha(JD_FECHA2.getDate()));
                } else {
                    int si_no = (int) edicion.msjQuest(1, "ESTAS SEGURO QUE DESEAS LIQUIDAR AL " + RECAUDADOR.getCargo() + " " + RECAUDADOR.getNombres() + " " + RECAUDADOR.getApellidos());
                    if (si_no == 0) {
                        String values = " '" + edicion.formatearFechaSQL(JD_FECHA1.getDate()) + "', "
                                + "'" + edicion.formatearFechaSQL(JD_FECHA2.getDate()) + "'," + RECAUDADOR.getIdentificacion();
                        pagosClienteDao.LIQUIDAR_PAGOS_CREDITO("4," + values);
                        CARGAR_COMPROBANTES_PARA_LIQUIDAR("3," + values);
                        edicion.mensajes(2, "liquidacion realizada correctamente.");
                    }
                }
            }
        } catch (Exception e) {
            edicion.mensajes(1, "seleccione un gestor de cobro.");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        Manager_Report report = new Manager_Report();
        Object[] key = {JD_FECHA1.getDate(), JD_FECHA2.getDate(), RECAUDADOR.getIdentificacion(),
            Variables_Gloabales.EMPLEADO.getNombres() + " " + Variables_Gloabales.EMPLEADO.getApellidos()};
        report.COMPROBANTE_DE_LIQUIDACION_DE_LETRAS_PARA_GESTOR_DE_COBRO(key);
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser JD_FECHA1;
    private com.toedter.calendar.JDateChooser JD_FECHA2;
    private javax.swing.JLabel LB_CNT_LETRAS;
    private javax.swing.JTextField TXTTOTALCOMISION;
    private javax.swing.JTextField TXTTOTALRECAUDO;
    private javax.swing.JComboBox comboRecaudador;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables

    private void CARGAR_RECAUDADORES() {
        comboRecaudador.removeAllItems();
        comboRecaudador.addItem(null);
        for (Empleado Lista_Cobradores : empleadoDao.LISTAR_EMPLEADOS_POR_CARGO("7, 'COBRADOR'")) {
            comboRecaudador.addItem(Lista_Cobradores.getNombres() + " " + Lista_Cobradores.getApellidos());
        }
    }

    private void CARGAR_COMPROBANTES_PARA_LIQUIDAR(String values) {
        edicion.llenarTabla(jTable1, pagosClienteDao.DETALLE_LIQUIDACION_PAGOS_CREDITO(values));
        edicion.calcula_total(jTable1, LB_CNT_LETRAS, TXTTOTALRECAUDO, 6);
        edicion.calcula_total(jTable1, LB_CNT_LETRAS, TXTTOTALCOMISION, 7);
    }
}
