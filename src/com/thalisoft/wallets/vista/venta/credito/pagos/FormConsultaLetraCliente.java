package com.thalisoft.wallets.vista.venta.credito.pagos;

import com.thalisoft.wallets.main.controller.ControllerContenedor;
import com.thalisoft.wallets.model.maestros.cliente.Cliente;
import com.thalisoft.wallets.model.maestros.cliente.ClienteDao;
import com.thalisoft.wallets.model.venta.credito.CreditoDao;
import com.thalisoft.wallets.util.CambiaFormatoTexto;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.vista.maestros.cliente.FormListarClientes;
import com.thalisoft.wallets.vista.venta.credito.FormPlanillaCobroCredito;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;

/**
 *
 * @author ThaliSoft
 */
public class FormConsultaLetraCliente extends javax.swing.JInternalFrame {

    Edicion edicion = new Edicion();
    CreditoDao creditoDao;
    String ID_CLIENTE = null;
    FormPagoCredito formPagoCredito;
    FormPlanillaCobroCredito formPlanillaCobroCredito;
    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();
    Cliente cliente;
    String ARTICULO_CREDITO;

    public FormConsultaLetraCliente() {
        initComponents();
        Edicion.CENTRAR_VENTANA(this);
        CARGAR_CREDITOS_CLIENTE();
        ACCIONES_FORMULARIO();
    }

    public FormConsultaLetraCliente(FormPagoCredito formPagoCredito, String ID_CLIENTE,
            FormPlanillaCobroCredito formPlanillaCobroCredito) {
        this.formPagoCredito = formPagoCredito;
        this.ID_CLIENTE = ID_CLIENTE;
        this.formPlanillaCobroCredito = formPlanillaCobroCredito;
        initComponents();
        Edicion.CENTRAR_VENTANA(this);
        ACCIONES_FORMULARIO();
        CONSULTA_CLIENTE(ID_CLIENTE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_CREDITOS_CLIENTE = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        LB_CNT_CREDITOS = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        TXTTOTALCREDITOS = new javax.swing.JTextField();
        TXTTOTALSALDO = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        TXTTOTALABONO = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TB_LETRAS_CREDITO = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        BTN_PLANILLA = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        TXTAYUDAPLANTILLA = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        txtIdentificacion = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtCliente = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("Letras Cliente");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CREDITOS & LETRAS CLIENTE", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Arial Black", 1, 24), new java.awt.Color(204, 0, 102))); // NOI18N

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CREDITOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial Black", 0, 18))); // NOI18N

        TB_CREDITOS_CLIENTE.setFont(new java.awt.Font("Consolas", 0, 12)); // NOI18N
        TB_CREDITOS_CLIENTE.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No. CREDITO", "PRODUCTO", "CANT. UNDS", "VR CREDITO", "VR SALDO", "VR ABONO", "VR CTA INICIAL", "RECAUDADOR", "FECHA CREDITO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_CREDITOS_CLIENTE.setRowHeight(24);
        TB_CREDITOS_CLIENTE.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(TB_CREDITOS_CLIENTE);
        if (TB_CREDITOS_CLIENTE.getColumnModel().getColumnCount() > 0) {
            TB_CREDITOS_CLIENTE.getColumnModel().getColumn(0).setMaxWidth(80);
            TB_CREDITOS_CLIENTE.getColumnModel().getColumn(1).setMinWidth(330);
            TB_CREDITOS_CLIENTE.getColumnModel().getColumn(2).setMaxWidth(60);
            TB_CREDITOS_CLIENTE.getColumnModel().getColumn(6).setMinWidth(100);
            TB_CREDITOS_CLIENTE.getColumnModel().getColumn(7).setMinWidth(200);
            TB_CREDITOS_CLIENTE.getColumnModel().getColumn(8).setMinWidth(110);
        }

        jLabel3.setText("CANTIDAD DE CREDITOS");

        LB_CNT_CREDITOS.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LB_CNT_CREDITOS.setText("0");

        jLabel5.setText("TOTAL CREDITOS");

        jLabel6.setText("TOTAL SALDOS");

        TXTTOTALCREDITOS.setEditable(false);
        TXTTOTALCREDITOS.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        TXTTOTALCREDITOS.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        TXTTOTALCREDITOS.setText("$ 0");

        TXTTOTALSALDO.setEditable(false);
        TXTTOTALSALDO.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        TXTTOTALSALDO.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        TXTTOTALSALDO.setText("$ 0");

        jLabel8.setText("TOTAL ABONOS");

        TXTTOTALABONO.setEditable(false);
        TXTTOTALABONO.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        TXTTOTALABONO.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        TXTTOTALABONO.setText("$ 0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LB_CNT_CREDITOS, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TXTTOTALCREDITOS, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(TXTTOTALABONO, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(TXTTOTALSALDO, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1062, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(TXTTOTALABONO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(LB_CNT_CREDITOS, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(TXTTOTALCREDITOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(TXTTOTALSALDO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PLAN DE PAGO CREDITO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 18))); // NOI18N

        TB_LETRAS_CREDITO.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "No. LETRA", "FECHA PAGO", "VR LETRA", "VR ABONO", "VR SALDO", "ESTADO", "ID_LETRA"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_LETRAS_CREDITO.setRowHeight(22);
        TB_LETRAS_CREDITO.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(TB_LETRAS_CREDITO);

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 204, 102));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/get-money.png"))); // NOI18N
        jButton1.setText("PAGAR LETRA");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        BTN_PLANILLA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Nuevo_2.png"))); // NOI18N
        BTN_PLANILLA.setText("AGREGAR LETRA A LA PLANILLA");
        BTN_PLANILLA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTN_PLANILLAActionPerformed(evt);
            }
        });

        TXTAYUDAPLANTILLA.setEditable(false);
        TXTAYUDAPLANTILLA.setBackground(new java.awt.Color(255, 204, 102));
        TXTAYUDAPLANTILLA.setColumns(20);
        TXTAYUDAPLANTILLA.setFont(new java.awt.Font("Monospaced", 1, 13)); // NOI18N
        TXTAYUDAPLANTILLA.setLineWrap(true);
        TXTAYUDAPLANTILLA.setRows(5);
        TXTAYUDAPLANTILLA.setText("MANTEN PULSADO LA TECLA CONTROL \"CTRL\" Y CON EL CLICK DEL MOUSE VE SELECCIONANDO LAS LETRAS QUE DESEAS AGREGAR A LA PLANILLA. ");
        TXTAYUDAPLANTILLA.setWrapStyleWord(true);
        jScrollPane3.setViewportView(TXTAYUDAPLANTILLA);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 498, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 55, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addGap(26, 26, 26)
                        .addComponent(BTN_PLANILLA)
                        .addGap(225, 225, 225))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BTN_PLANILLA, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jLabel1.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel1.setText("IDENTIFICACIÓN");

        txtIdentificacion.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        txtIdentificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdentificacionActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/logo_el_progreso_label.png"))); // NOI18N

        jLabel7.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel7.setText("CLIENTE");

        txtCliente.setEditable(false);
        txtCliente.setFont(new java.awt.Font("Arial Narrow", 1, 12)); // NOI18N
        txtCliente.setBorder(null);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/team.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton4.setText("SALIR");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(79, 79, 79)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtIdentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(66, 66, 66))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton4))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(141, 141, 141))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIdentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jButton4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtIdentificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdentificacionActionPerformed
        // TODO add your handling code here:
        CONSULTA_CLIENTE(txtIdentificacion.getText());
    }//GEN-LAST:event_txtIdentificacionActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            if (formPagoCredito != null) {

                int row_TB_Credito = TB_CREDITOS_CLIENTE.getSelectedRow();
                int row_TB_Letra = TB_LETRAS_CREDITO.getSelectedRow();
                Object NUMERO_CREDITO = TB_CREDITOS_CLIENTE.getValueAt(row_TB_Credito, 0);
                Object NUMERO_LETRA = TB_LETRAS_CREDITO.getValueAt(row_TB_Letra, 0);
                Object COBRADOR = TB_CREDITOS_CLIENTE.getValueAt(row_TB_Credito, 6);
                Object FECHA_PAGO = TB_LETRAS_CREDITO.getValueAt(row_TB_Letra, 1);
                Object VALOR_LETRA = formatoTexto.numerico(TB_LETRAS_CREDITO.getValueAt(row_TB_Letra, 2).toString());
                Object VALOR_ABONO = formatoTexto.numerico(TB_LETRAS_CREDITO.getValueAt(row_TB_Letra, 3).toString());
                Object VALOR_SALDO = formatoTexto.numerico(TB_LETRAS_CREDITO.getValueAt(row_TB_Letra, 4).toString());
                formPagoCredito.VALOR_ABONO = edicion.toNumeroEntero(VALOR_ABONO.toString());
                formPagoCredito.VALOR_SALDO_LETRA = edicion.toNumeroEntero(VALOR_SALDO.toString());
                formPagoCredito.txtNumCredito.setText(NUMERO_CREDITO.toString());
                formPagoCredito.txtNumLetra.setText(NUMERO_LETRA.toString());
                formPagoCredito.JD_FECHA_PAGO_LETRA.setDate(edicion.CambiarTipoFecha(FECHA_PAGO));
                formPagoCredito.ID_LETRA = TB_LETRAS_CREDITO.getValueAt(row_TB_Letra, 6);
                formPagoCredito.txtVrLetra.setText("$ " + VALOR_LETRA);
                formPagoCredito.txtVrSaldo.setText("$ " + VALOR_SALDO);
                formPagoCredito.txtVrPagado.requestFocus();
                formPagoCredito.CONSULTA_CLIENTE(txtIdentificacion.getText());
                formPagoCredito.CARGAR_HISTORIAL_PAGOS_CREDITO();
                formPagoCredito.CARGAR_LETRAS_PENDIENTES_PAGO(NUMERO_CREDITO);
                dispose();
            }
        } catch (Exception e) {
            edicion.mensajes(3, "seleccionar una letra de la lista.");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        JInternalFrame ji = validador.getJInternalFrame(FormListarClientes.class.getName());

        if (ji == null || ji.isClosed()) {
            ji = new FormListarClientes(null, null, this, null);
            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormListarClientes.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormListarClientes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void BTN_PLANILLAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTN_PLANILLAActionPerformed
        // TODO add your handling code here:
        try {

            int[] rowLetra = TB_LETRAS_CREDITO.getSelectedRows();
            String acumLetra = "", estadoLetra;
            int totalLetra = 0;
            for (int i = 0; i < rowLetra.length; i++) {
                totalLetra = totalLetra + edicion.toNumeroEntero(TB_LETRAS_CREDITO.getValueAt(rowLetra[i], 4).toString());
                acumLetra = acumLetra + "," + edicion.AGREGAR_CEROS_LEFT(edicion.toNumeroEntero(TB_LETRAS_CREDITO.getValueAt(rowLetra[i], 0).toString()), 2);
            }

            CONSULTA_CLIENTE(txtIdentificacion.getText());
            formPlanillaCobroCredito.CLIENTE = cliente;
            formPlanillaCobroCredito.ARTICULOS = ARTICULO_CREDITO;
            if (rowLetra.length > 0) {
                formPlanillaCobroCredito.LETRAS = acumLetra.substring(1);
                formPlanillaCobroCredito.VALOR_LETRAS = totalLetra;
            }

            formPlanillaCobroCredito.MOSTRAR_DATOS_PLANILLA();
             dispose();
        } catch (Exception e) {
            edicion.mensajes(3, "por favor seleccionar letras");
        }
       
    }//GEN-LAST:event_BTN_PLANILLAActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton4ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTN_PLANILLA;
    private javax.swing.JLabel LB_CNT_CREDITOS;
    private javax.swing.JTable TB_CREDITOS_CLIENTE;
    private javax.swing.JTable TB_LETRAS_CREDITO;
    private javax.swing.JTextArea TXTAYUDAPLANTILLA;
    private javax.swing.JTextField TXTTOTALABONO;
    private javax.swing.JTextField TXTTOTALCREDITOS;
    private javax.swing.JTextField TXTTOTALSALDO;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtIdentificacion;
    // End of variables declaration//GEN-END:variables

    private void CARGAR_CREDITOS_CLIENTE() {
        if (!txtIdentificacion.getText().isEmpty()) {
            creditoDao = new CreditoDao();
            edicion.limpiar_tablas(TB_LETRAS_CREDITO);
            edicion.llenarTabla(TB_CREDITOS_CLIENTE, creditoDao.CREDITOS_CLIENTE("7," + txtIdentificacion.getText()));
            edicion.calcula_total(TB_CREDITOS_CLIENTE, LB_CNT_CREDITOS, TXTTOTALCREDITOS, 3);
            edicion.calcula_total(TB_CREDITOS_CLIENTE, LB_CNT_CREDITOS, TXTTOTALSALDO, 4);
            edicion.calcula_total(TB_CREDITOS_CLIENTE, LB_CNT_CREDITOS, TXTTOTALABONO, 5);
        }
    }

    public void CONSULTA_CLIENTE(String idCliente) {
        if (idCliente != null | !" ".equals(idCliente)) {
            cliente = new ClienteDao().CONSULTAR_CLIENTE("1, " + idCliente);
            if (cliente != null) {
                txtIdentificacion.setText(cliente.getIdentificacion());
                txtCliente.setText(cliente.getNombres() + " " + cliente.getApellidos() + " - Tel: " + cliente.getTelefono1());
                CARGAR_CREDITOS_CLIENTE();
            }
        }

    }

    private void ACCIONES_FORMULARIO() {

        if (formPagoCredito == null) {
            jButton1.setVisible(false);
        }
        if (formPlanillaCobroCredito == null) {
            BTN_PLANILLA.setVisible(false);
            TXTAYUDAPLANTILLA.setVisible(false);
        }

        TB_CREDITOS_CLIENTE.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                int row = TB_CREDITOS_CLIENTE.getSelectedRow();
                ARTICULO_CREDITO = TB_CREDITOS_CLIENTE.getValueAt(row, 1).toString();
                CARGAR_LETRAS_CREDITO(row);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

            private void CARGAR_LETRAS_CREDITO(int row) {
                edicion.llenarTabla(TB_LETRAS_CREDITO,
                        creditoDao.PLAN_DE_PAGO_CREDITO("8, " + TB_CREDITOS_CLIENTE.getValueAt(row, 0)));
            }
        });
    }

}
