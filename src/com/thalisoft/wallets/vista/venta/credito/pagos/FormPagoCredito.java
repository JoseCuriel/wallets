package com.thalisoft.wallets.vista.venta.credito.pagos;

import com.thalisoft.wallets.main.controller.ControllerContenedor;
import com.thalisoft.wallets.model.cartera.pago.PagosCliente;
import com.thalisoft.wallets.model.cartera.pago.PagosClienteDao;
import com.thalisoft.wallets.model.maestros.cliente.Cliente;
import com.thalisoft.wallets.model.maestros.cliente.ClienteDao;
import com.thalisoft.wallets.model.maestros.empleados.Empleado;
import com.thalisoft.wallets.model.maestros.empleados.EmpleadoDao;
import com.thalisoft.wallets.model.maestros.empleados.privilegios.PrivilegioEmpleadoDao;
import com.thalisoft.wallets.model.venta.credito.CreditoDao;
import com.thalisoft.wallets.model.venta.credito.DetalleLetras;
import com.thalisoft.wallets.util.CambiaFormatoTexto;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.util.report.Manager_Report;
import com.thalisoft.wallets.vista.maestros.cliente.FormListarClientes;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;

/**
 *
 * @author ThaliSoft
 */
public class FormPagoCredito extends javax.swing.JInternalFrame {

    public Object ID_LETRA;
    public int VALOR_ABONO;
    Edicion edicion = new Edicion();
    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();
    Manager_Report report = new Manager_Report();
    Empleado RECAUDADOR;
    EmpleadoDao empleadoDao;

    PagosCliente pagosCliente;
    PagosClienteDao pagosClienteDao;
    public int VALOR_SALDO_LETRA;

    PrivilegioEmpleadoDao privilegioAcceso = new PrivilegioEmpleadoDao();

    public FormPagoCredito() {
        empleadoDao = new EmpleadoDao();
        pagosClienteDao = new PagosClienteDao();
        initComponents();
        Edicion.CENTRAR_VENTANA(this);
        CARGAR_RECAUDADORES();
        NUEVO_PAGO_CREDITO();
        ACCIONES_FML_PAGO_CREDITO();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        TXTNUMCOMPROBANTE = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        JD_FECHA_EMISION = new com.toedter.calendar.JDateChooser();
        jToolBar1 = new javax.swing.JToolBar();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtNumCredito = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtNumLetra = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtVrLetra = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtVrSaldo = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtVrComision = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtVrPagado = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        JD_FECHA_PAGO_LETRA = new com.toedter.calendar.JDateChooser();
        comboRecaudador = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtIdentidadCliente = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtNomApeCliente = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtTelCliente = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtDirCliente = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jCheck_FIJAR_CLIENTE = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jCheck_FIJAR_FECHA_PAGO = new javax.swing.JCheckBox();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_HISTORIAL_PAGOS_CREDITO = new javax.swing.JTable();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/printer.png"))); // NOI18N
        jMenuItem1.setText("IMPRIMIR COMPROBANTE");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        setClosable(true);
        setIconifiable(true);
        setTitle("Pagos Credito");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PAGOS CREDITO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 1, 24), new java.awt.Color(0, 153, 204))); // NOI18N

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/logo_el_progreso_label.png"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel2.setText("No. COMPROBANTE");

        TXTNUMCOMPROBANTE.setEditable(false);
        TXTNUMCOMPROBANTE.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        TXTNUMCOMPROBANTE.setHorizontalAlignment(javax.swing.JTextField.TRAILING);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("FECHA DE EMISIÓN");

        JD_FECHA_EMISION.setDateFormatString("EEEE dd MMMM yyyy");

        jToolBar1.setRollover(true);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/add-page.png"))); // NOI18N
        jButton3.setToolTipText("NUEVO COMPROBANTE");
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/save.png"))); // NOI18N
        jButton4.setToolTipText("GUARDAR COMPROBANTE");
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/printer.png"))); // NOI18N
        jButton6.setToolTipText("IMPRIMIR COMPROBANTE");
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton6);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/receipt.png"))); // NOI18N
        jButton5.setToolTipText("RELACION DE PAGOS");
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton5.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/presentation.png"))); // NOI18N
        jButton2.setToolTipText("LIQUIDACION COBRADOR");
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/investigacion-icono-8491-48.png"))); // NOI18N
        jButton8.setToolTipText("Consultar Comprobante");
        jButton8.setFocusable(false);
        jButton8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton8.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton8);

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Back.png"))); // NOI18N
        jButton9.setToolTipText("Anterior Comprobante");
        jButton9.setFocusable(false);
        jButton9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton9.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton9);

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Next.png"))); // NOI18N
        jButton10.setToolTipText("Siguiente Comprobante");
        jButton10.setFocusable(false);
        jButton10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton10.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton10);

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton7.setToolTipText("SALIR");
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton7.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton7);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(TXTNUMCOMPROBANTE, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(JD_FECHA_EMISION, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TXTNUMCOMPROBANTE, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JD_FECHA_EMISION, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel1))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel4.setText("No. CREDITO");

        txtNumCredito.setEditable(false);
        txtNumCredito.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel5.setText("No. LETRA");

        txtNumLetra.setEditable(false);
        txtNumLetra.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel6.setText("VALOR LETRA");

        txtVrLetra.setEditable(false);
        txtVrLetra.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtVrLetra.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtVrLetra.setText("$ 0");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel11.setText("VALOR SALDO");

        txtVrSaldo.setEditable(false);
        txtVrSaldo.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtVrSaldo.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtVrSaldo.setText("$ 0");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel12.setText("VALOR COMISIÓN");

        txtVrComision.setEditable(false);
        txtVrComision.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtVrComision.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtVrComision.setText("$ 0");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setText("VALOR ABONO");

        txtVrPagado.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtVrPagado.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtVrPagado.setText("$ 0");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel16.setText("FECHA DE PAGO");

        JD_FECHA_PAGO_LETRA.setDateFormatString("dd-MM-yyyy");
        JD_FECHA_PAGO_LETRA.setEnabled(false);
        JD_FECHA_PAGO_LETRA.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        comboRecaudador.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel14.setText("RECAUDADOR");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNumLetra, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                            .addComponent(txtNumCredito))
                        .addGap(33, 33, 33)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtVrLetra, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                            .addComponent(txtVrComision))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel13))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(txtVrSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtVrPagado, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 19, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JD_FECHA_PAGO_LETRA, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboRecaudador, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboRecaudador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNumCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNumLetra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtVrLetra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(txtVrSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtVrPagado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel12)
                                .addComponent(txtVrComision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addComponent(JD_FECHA_PAGO_LETRA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CLIENTE", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.BELOW_TOP, new java.awt.Font("Consolas", 1, 16), new java.awt.Color(204, 51, 0))); // NOI18N
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("No. IDENTIFICACION");
        jPanel5.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 33, 143, -1));

        txtIdentidadCliente.setFont(new java.awt.Font("Arial", 1, 15)); // NOI18N
        txtIdentidadCliente.setToolTipText("DIGITA EL # DE DOCUMENTO Y PRESIONA \"ENTER\"");
        txtIdentidadCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdentidadClienteActionPerformed(evt);
            }
        });
        jPanel5.add(txtIdentidadCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 53, 140, -1));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("NOMBRES & APELLIDOS");
        jPanel5.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(131, 33, 270, -1));

        txtNomApeCliente.setEditable(false);
        txtNomApeCliente.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jPanel5.add(txtNomApeCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(161, 53, 280, 20));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("TELEFONO");
        jPanel5.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 88, 143, -1));

        txtTelCliente.setEditable(false);
        txtTelCliente.setFont(new java.awt.Font("Arial", 1, 15)); // NOI18N
        jPanel5.add(txtTelCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 108, 143, 30));

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("DIRECCION");
        jPanel5.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(163, 83, 260, -1));

        txtDirCliente.setEditable(false);
        txtDirCliente.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jPanel5.add(txtDirCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(159, 108, 350, 30));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/team.png"))); // NOI18N
        jButton1.setToolTipText("CLICK PARA CONSULTAR CLIENTES");
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 20, 60, 60));

        jCheck_FIJAR_CLIENTE.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jCheck_FIJAR_CLIENTE.setText("FIJAR CLIENTE");
        jCheck_FIJAR_CLIENTE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheck_FIJAR_CLIENTEActionPerformed(evt);
            }
        });
        jPanel5.add(jCheck_FIJAR_CLIENTE, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, -1, -1));

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LETRAS PENDIENTES POR PAGAR", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jTable1.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No. LETRA", "FECHA PAGO", "VALOR LETRA", "VALOR SALDO", "ID. LETRA"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jTable1.setRowHeight(22);
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(4).setMaxWidth(50);
        }

        jCheck_FIJAR_FECHA_PAGO.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jCheck_FIJAR_FECHA_PAGO.setText("FIJAR FECHA DE PAGO COMO LA FECHA DE EMISIÓN");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(jCheck_FIJAR_FECHA_PAGO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheck_FIJAR_FECHA_PAGO)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "HISTORIAL DE PAGOS CREDITO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        TB_HISTORIAL_PAGOS_CREDITO.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        TB_HISTORIAL_PAGOS_CREDITO.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No. COMPROBANTE", "No. LETRA", "FECHA PAGO", "FECHA HORA REG", "VALOR LETRA", "VALOR PAGADO", "VALOR SALDO", "VALOR COMISIÓN"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_HISTORIAL_PAGOS_CREDITO.setComponentPopupMenu(jPopupMenu1);
        TB_HISTORIAL_PAGOS_CREDITO.setRowHeight(22);
        TB_HISTORIAL_PAGOS_CREDITO.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(TB_HISTORIAL_PAGOS_CREDITO);
        if (TB_HISTORIAL_PAGOS_CREDITO.getColumnModel().getColumnCount() > 0) {
            TB_HISTORIAL_PAGOS_CREDITO.getColumnModel().getColumn(3).setMinWidth(150);
        }

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 850, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 105, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 516, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 32, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 185, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 609, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtIdentidadClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdentidadClienteActionPerformed
        // TODO add your handling code here:
        CARGAR_FORM_LETRAS_CLIENTE(txtIdentidadCliente.getText());
        CONSULTA_CLIENTE(txtIdentidadCliente.getText());
    }//GEN-LAST:event_txtIdentidadClienteActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        JInternalFrame ji = validador.getJInternalFrame(FormListarClientes.class.getName());

        if (ji == null || ji.isClosed()) {
            ji = new FormListarClientes(null, this, null, null);
            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormListarClientes.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormListarClientes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        int SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas salir?");
        if (SI_NO == 0) {
            dispose();
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        if (VALIDAR_FML_PAGOS_CREDITO() != false) {
            int SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas registrar el pago?");
            if (SI_NO == 0) {
                pagosCliente = new PagosCliente();
                pagosCliente = pagosClienteDao.SELECT_PAGOS_CLIENTE("2, " + TXTNUMCOMPROBANTE.getText());
                if (pagosCliente == null) {
                    if (pagosClienteDao.CREATE_PAGO_CLIENTE(DATA_PAGO_CREDITO(1))) {
                        CARGAR_HISTORIAL_PAGOS_CREDITO();
                        CARGAR_LETRAS_PENDIENTES_PAGO(txtNumCredito.getText());
                        edicion.mensajes(2, "pago registrado correctamente.");
                    }
                } else {
                    edicion.mensajes(3, "el comprobante # " + edicion.AGREGAR_CEROS_LEFT(pagosCliente.getIdPagosCliente()) + " se encuentra registrado.");
                }
            }
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        NUEVO_PAGO_CREDITO();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        Object ID_Comprobante = edicion.msjQuest(2, "ingresa el # del comprobante.");
        CONSULTAR_COMPROBANTE_PAGO(ID_Comprobante);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
        int comprobante = edicion.toNumeroEntero(TXTNUMCOMPROBANTE.getText());
        comprobante--;
        CONSULTAR_COMPROBANTE_PAGO(comprobante);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        int comprobante = edicion.toNumeroEntero(TXTNUMCOMPROBANTE.getText());
        comprobante++;
        CONSULTAR_COMPROBANTE_PAGO(comprobante);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:

        report.COMPROBANTE_DE_PAGO_CREDITO(TXTNUMCOMPROBANTE.getText());
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        int row = TB_HISTORIAL_PAGOS_CREDITO.getSelectedRow();
        report.COMPROBANTE_DE_PAGO_CREDITO(TB_HISTORIAL_PAGOS_CREDITO.getValueAt(row, 0));
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormRelacionPagosCredito", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
            JInternalFrame ji = validador.getJInternalFrame(FormRelacionPagosCredito.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormRelacionPagosCredito();
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormRelacionPagosCredito.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormRelacionPagosCredito.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormLiquidarCobrador", Variables_Gloabales.EMPLEADO.getIdentificacion()) == true) {
            JInternalFrame ji = validador.getJInternalFrame(FormLiquidarCobrador.class.getName());

            if (ji == null || ji.isClosed()) {
                ji = new FormLiquidarCobrador();
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormLiquidarCobrador.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormLiquidarCobrador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jCheck_FIJAR_CLIENTEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheck_FIJAR_CLIENTEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheck_FIJAR_CLIENTEActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_jTable1MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser JD_FECHA_EMISION;
    public com.toedter.calendar.JDateChooser JD_FECHA_PAGO_LETRA;
    private javax.swing.JTable TB_HISTORIAL_PAGOS_CREDITO;
    private javax.swing.JTextField TXTNUMCOMPROBANTE;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox comboRecaudador;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JCheckBox jCheck_FIJAR_CLIENTE;
    private javax.swing.JCheckBox jCheck_FIJAR_FECHA_PAGO;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    public javax.swing.JTextField txtDirCliente;
    public javax.swing.JTextField txtIdentidadCliente;
    public javax.swing.JTextField txtNomApeCliente;
    public javax.swing.JTextField txtNumCredito;
    public javax.swing.JTextField txtNumLetra;
    public javax.swing.JTextField txtTelCliente;
    public javax.swing.JTextField txtVrComision;
    public javax.swing.JTextField txtVrLetra;
    public javax.swing.JTextField txtVrPagado;
    public javax.swing.JTextField txtVrSaldo;
    // End of variables declaration//GEN-END:variables

    public void CARGAR_FORM_LETRAS_CLIENTE(String idCliente) {
        if (privilegioAcceso.VERIFICAR_PRIVILEGIO_EMPLEADO("FormConsultaLetraCliente", Variables_Gloabales.EMPLEADO.getIdentificacion())) {
            
            JInternalFrame ji = validador.getJInternalFrame(FormConsultaLetraCliente.class.getName());
            if (ji == null || ji.isClosed()) {
                ji = new FormConsultaLetraCliente(this, idCliente, null);
                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                validador.addJIframe(FormConsultaLetraCliente.class.getName(), ji);
                ji.setVisible(true);
            } else {
                ji.show(true);
                try {
                    ji.setIcon(false);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(FormConsultaLetraCliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            edicion.mensajes(3, Variables_Gloabales.EMPLEADO.getNombres() + " "
                    + Variables_Gloabales.EMPLEADO.getApellidos() + " acceso denegado! No tienes los permisos necesarios para entrar.");
        }
    }

    public void CONSULTA_CLIENTE(String idCliente) {
        if (!idCliente.isEmpty() | idCliente != null) {
            Cliente cliente = new ClienteDao().CONSULTAR_CLIENTE("1, " + idCliente);
            if (cliente != null) {
                MOSTRAR_CLIENTE(cliente);
            }
        }

    }

    private void NUEVO_PAGO_CREDITO() {
        TXTNUMCOMPROBANTE.setText(NUMERO_COMPROBANTE().toString());
        if (!jCheck_FIJAR_CLIENTE.isSelected()) {
            MOSTRAR_CLIENTE(new Cliente());
            txtNomApeCliente.setText("");
            txtNumCredito.setText(null);
            edicion.limpiar_tablas(TB_HISTORIAL_PAGOS_CREDITO);
            comboRecaudador.setSelectedItem(null);
            edicion.limpiar_tablas(jTable1);
        }
        if (!jCheck_FIJAR_FECHA_PAGO.isSelected()) {
            JD_FECHA_EMISION.setDate(DateUtil.newDateTime());
        }
        txtVrComision.setText("$ 0");
        txtVrPagado.setText("$ 0");
        txtVrLetra.setText("$ 0");
        txtVrSaldo.setText("$ 0");
        txtNumLetra.setText(null);
        JD_FECHA_PAGO_LETRA.setDate(null);

    }

    private void CARGAR_RECAUDADORES() {
        comboRecaudador.removeAllItems();
        comboRecaudador.addItem(null);
        for (Empleado Lista_Cobradores : empleadoDao.LISTAR_EMPLEADOS_POR_CARGO("7, 'COBRADOR'")) {
            comboRecaudador.addItem(Lista_Cobradores.getNombres() + " " + Lista_Cobradores.getApellidos());
        }

    }

    private void ACCIONES_FML_PAGO_CREDITO() {
        comboRecaudador.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    RECAUDADOR = empleadoDao.CONSULTAR_EMPLEADO(comboRecaudador.getSelectedItem());
                    if (RECAUDADOR != null) {
                        if (edicion.toNumeroEntero(txtVrPagado.getText()) > 0) {
                            CALCULAR_COMISION_ABONOS_SALDOS_LETRA();
                        }
                    }
                }
            }
        }
        );
        txtVrPagado.addKeyListener(
                new KeyAdapter() {
                    @Override
                    public void keyReleased(KeyEvent e
                    ) {
                        txtVrPagado.setText("$ " + formatoTexto.numerico(edicion.toNumeroEntero(txtVrPagado.getText())));
                        CALCULAR_COMISION_ABONOS_SALDOS_LETRA();
                    }
                }
        );

        jTable1.addMouseListener(
                new MouseListener() {

                    @Override
                    public void mouseClicked(MouseEvent e
                    ) {
                        if (e.getClickCount() == 1) {
                            int row = jTable1.getSelectedRow();
                            if (jCheck_FIJAR_FECHA_PAGO.isSelected()) {
                                JD_FECHA_EMISION.setDate(DateUtil.getDateTime(jTable1.getValueAt(row, 1)));
                            } else {
                                JD_FECHA_EMISION.setDate(DateUtil.newDateTime());
                            }
                            ID_LETRA = jTable1.getValueAt(row, 4);
                            txtNumLetra.setText(jTable1.getValueAt(row, 0).toString());
                            JD_FECHA_PAGO_LETRA.setDate(DateUtil.getDateTime(jTable1.getValueAt(row, 1)));
                            txtVrLetra.setText("$ " + formatoTexto.numerico(jTable1.getValueAt(row, 2).toString()));
                            txtVrSaldo.setText("$ " + formatoTexto.numerico(jTable1.getValueAt(row, 3).toString()));
                            VALOR_SALDO_LETRA = edicion.toNumeroEntero(jTable1.getValueAt(row, 3).toString());
                            CALCULAR_COMISION_ABONOS_SALDOS_LETRA();
                            txtVrPagado.requestFocus();
                        }
                    }

                    @Override
                    public void mousePressed(MouseEvent e
                    ) {
                    }

                    @Override
                    public void mouseReleased(MouseEvent e
                    ) {
                    }

                    @Override
                    public void mouseEntered(MouseEvent e
                    ) {
                    }

                    @Override
                    public void mouseExited(MouseEvent e
                    ) {
                    }
                });
    }

    private void CALCULAR_COMISION_ABONOS_SALDOS_LETRA() {

        int Vr_Pagado = edicion.toNumeroEntero(txtVrPagado.getText());
        int VrSaldo = VALOR_SALDO_LETRA - Vr_Pagado;
        int comision = 0;
        if (RECAUDADOR != null) {
            comision = (Vr_Pagado * RECAUDADOR.getPorcComision()) / 100;
        }
        if (VrSaldo < 0) {
            VrSaldo = 0;
        }
        txtVrSaldo.setText("$ " + formatoTexto.numerico(VrSaldo));
        txtVrComision.setText("$ " + formatoTexto.numerico(comision));

    }

    private Object[] DATA_PAGO_CREDITO(int opcion) {
        Object[] VALUES = {opcion, NUMERO_COMPROBANTE(), "'" + edicion.formatearFechaSQL(JD_FECHA_EMISION.getDate()) + "'",
            ID_LETRA, edicion.toNumeroEntero(txtVrLetra.getText()), edicion.toNumeroEntero(txtVrPagado.getText()),
            edicion.toNumeroEntero(txtVrSaldo.getText()), edicion.toNumeroEntero(txtVrComision.getText()),
            Variables_Gloabales.EMPLEADO.getIdentificacion(), txtNumCredito.getText(), RECAUDADOR.getIdentificacion()};

        return VALUES;
    }

    private Object NUMERO_COMPROBANTE() {
        return pagosClienteDao.NUMERO_PAGOS_CREDITO();
    }

    private boolean VALIDAR_FML_PAGOS_CREDITO() {
        if (edicion.toNumeroEntero(txtVrLetra.getText()) < 1) {
            edicion.mensajes(1, "el valor de la letra debe ser mayor a cero(0), seleccione una letra para continuar.");
            return false;
        }

        if (edicion.toNumeroEntero(txtVrPagado.getText()) < 1) {
            edicion.mensajes(1, "el valor del abono  debe ser mayor a cero(0).");
            return false;
        }
        if (edicion.toNumeroEntero(txtVrPagado.getText()) > edicion.toNumeroEntero(txtVrLetra.getText())) {
            edicion.mensajes(1, "el valor del abono no puede ser mayor al valor de la letra.");
            return false;
        }

        if (edicion.toNumeroEntero(txtVrPagado.getText()) > VALOR_SALDO_LETRA) {
            edicion.mensajes(1, "el valor del abono no puede ser mayor al valor del saldo."
                    + "\nVALOR SALDO = $ " + formatoTexto.numerico(VALOR_SALDO_LETRA) + ""
                    + "\nVALOR ABONO = $ " + formatoTexto.numerico(edicion.toNumeroEntero(txtVrPagado.getText())));
            return false;
        }
        if (comboRecaudador.getSelectedItem() == "" | comboRecaudador.getSelectedItem() == null) {
            edicion.mensajes(1, "por favor seleccionar el recaudador del pago.");
            comboRecaudador.grabFocus();
            return false;
        }
        return true;
    }

    public void CARGAR_HISTORIAL_PAGOS_CREDITO() {
        edicion.llenarTabla(TB_HISTORIAL_PAGOS_CREDITO, pagosClienteDao.DETALLE_PAGOS_CREDITO("3," + txtNumCredito.getText()));

    }

    private void MOSTRAR_CLIENTE(Cliente cliente) {
        txtIdentidadCliente.setText(cliente.getIdentificacion());
        txtNomApeCliente.setText(cliente.getNombres() + " " + cliente.getApellidos());
        txtTelCliente.setText(cliente.getTelefono1());
        txtDirCliente.setText(cliente.getDireccion1());
    }

    public void CONSULTAR_COMPROBANTE_PAGO(Object key) {
        pagosCliente = new PagosCliente();
        pagosCliente = pagosClienteDao.SELECT_PAGOS_CLIENTE("2," + key);
        if (pagosCliente != null) {
            MOSTRAR_COMPROBANTE_PAGO(pagosCliente);
        }
    }

    private void MOSTRAR_COMPROBANTE_PAGO(PagosCliente pagosCliente) {
        CreditoDao creditoDao = new CreditoDao();
        DetalleLetras detalleLetras = creditoDao.CONSULTA_LETRA_CREDITO("9, " + pagosCliente.getIdLetra());
        TXTNUMCOMPROBANTE.setText(edicion.AGREGAR_CEROS_LEFT(pagosCliente.getIdPagosCliente(), 6));
        JD_FECHA_EMISION.setDate(pagosCliente.getFechaEmision());
        MOSTRAR_CLIENTE(detalleLetras.getCredito().getCliente());
        txtNumCredito.setText(edicion.AGREGAR_CEROS_LEFT(detalleLetras.getCredito().getNumeroCredito(), 6));
        txtNumLetra.setText(edicion.AGREGAR_CEROS_LEFT(detalleLetras.getNumeroLetra(), 6));
        JD_FECHA_PAGO_LETRA.setDate(detalleLetras.getFechaPago());
        comboRecaudador.setSelectedItem(detalleLetras.getCredito().getCobrador().getNombres() + " " + detalleLetras.getCredito().getCobrador().getApellidos());
        txtVrLetra.setText("$ " + formatoTexto.numerico(pagosCliente.getTotalVrLetra()));
        txtVrSaldo.setText("$ " + formatoTexto.numerico(pagosCliente.getTotalVrSaldo()));
        txtVrComision.setText("$ " + formatoTexto.numerico(pagosCliente.getTotalVrComision()));
        txtVrPagado.setText("$ " + formatoTexto.numerico(pagosCliente.getTotalVrAbono()));
        comboRecaudador.setSelectedItem(pagosCliente.getRecaudador().getNombres() + " " + pagosCliente.getRecaudador().getApellidos());
        CARGAR_HISTORIAL_PAGOS_CREDITO();
        CARGAR_LETRAS_PENDIENTES_PAGO(txtNumCredito.getText());
    }

    public void CARGAR_LETRAS_PENDIENTES_PAGO(Object ID_KEY) {
        edicion.llenarTabla(jTable1, pagosClienteDao.LETRAS_PENDIENTES_CREDITO(ID_KEY));
    }
}

class validador {

    public static java.util.HashMap<String, javax.swing.JInternalFrame> jIframes = new java.util.HashMap<>();

    public static void addJIframe(String key, javax.swing.JInternalFrame jiframe) {
        jIframes.put(key, jiframe);
    }

    public static javax.swing.JInternalFrame getJInternalFrame(String key) {
        return jIframes.get(key);
    }
}
