package com.thalisoft.wallets.vista.venta.credito;

import com.thalisoft.wallets.util.CambiaFormatoTexto;
import com.thalisoft.wallets.util.Edicion;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Date;

/**
 *
 * @author ThaliSoft
 */
public class FormProgramarPagoCtaInicial extends javax.swing.JInternalFrame {

    int VALOR_CREDITO, VALOR_CTA_INICIAL;
    Date FECHA_CREDITO;
    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();
    Edicion edicion = new Edicion();
    FormCredito formCredito;

    public FormProgramarPagoCtaInicial(int valorCredito, int VALOR_CTA_INICIAL, Date FechaCredito, FormCredito formCredito) {
        this.VALOR_CREDITO = valorCredito;
        this.VALOR_CTA_INICIAL = VALOR_CTA_INICIAL;
        this.FECHA_CREDITO = FechaCredito;
        this.formCredito = formCredito;
        initComponents();
        Edicion.CENTRAR_VENTANA(this);
        txtValorCredito.setText("$ " + formatoTexto.numerico(VALOR_CREDITO));
        JD_FechaCredito.setDate(FECHA_CREDITO);
        txtValorCtaInicial.setText("$ " + formatoTexto.numerico(VALOR_CTA_INICIAL));
        txtValorCtaInicial.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                txtValorCtaInicial.setText("$ " + formatoTexto.numerico(edicion.toNumeroEntero(txtValorCtaInicial.getText())));
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JD_FechaCredito = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        txtValorCredito = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtValorCtaInicial = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        JD_FechaPago = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("Cuota Inicial");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PROGRAMAR PAGO CUOTA INICIAL", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 18))); // NOI18N
        jPanel1.setToolTipText("");

        jLabel1.setText("FECHA DEL CREDITO");

        JD_FechaCredito.setDateFormatString("EEEE dd MMMM yyyy");
        JD_FechaCredito.setEnabled(false);
        JD_FechaCredito.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel2.setText("VALOR TOTAL CREDITO");

        txtValorCredito.setEditable(false);
        txtValorCredito.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtValorCredito.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtValorCredito.setText("$ 0");

        jLabel4.setText("VALOR CUOTA INICIAL");

        txtValorCtaInicial.setEditable(false);
        txtValorCtaInicial.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtValorCtaInicial.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtValorCtaInicial.setText("$ 0");

        jLabel5.setText("FECHA DE PAGO");

        JD_FechaPago.setDateFormatString("EEEE dd MMMM yyyy");
        JD_FechaPago.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/fingerprint-scanning-process.png"))); // NOI18N
        jButton1.setText("ACEPTAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton2.setText("CANCELAR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtValorCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(JD_FechaCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtValorCtaInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(8, 8, 8)
                                        .addComponent(JD_FechaPago, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jButton1)
                        .addGap(10, 10, 10)
                        .addComponent(jButton2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValorCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValorCtaInicial))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(JD_FechaCredito, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(JD_FechaPago, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if (VALIDAR_FML_PROG_CTA_INICIAL() != false) {
            int sino = (int) edicion.msjQuest(1, "estas seguro que deseas aplicar esta programacion?");
            if (sino == 0) {
                formCredito.PROGRAMAR_PAGO_CUOTA_INICIAL(JD_FechaPago.getDate(),
                        edicion.toNumeroEntero(txtValorCtaInicial.getText()));
                formCredito.btn_prog_cta_inicial.setEnabled(false);
                dispose();
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser JD_FechaCredito;
    private com.toedter.calendar.JDateChooser JD_FechaPago;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtValorCredito;
    private javax.swing.JTextField txtValorCtaInicial;
    // End of variables declaration//GEN-END:variables

    /*private void CALCULAR_FECHA_PAGO() {
     Calendar fecha_Credito = new GregorianCalendar();
     fecha_Credito.setTime(FECHA_CREDITO);
     LocalDate fechaCredito = LocalDate.of(fecha_Credito.get(Calendar.YEAR),
     fecha_Credito.get(Calendar.MONTH) + 1, fecha_Credito.get(Calendar.DAY_OF_MONTH));
     fechaCredito = fechaCredito.plusDays(Integer.parseInt(JS_DiasProgramados.getValue().toString()));
     String fecha_de_pago = "" + edicion.AGREGAR_CEROS_LEFT(fechaCredito.getDayOfMonth(), 2) + "-" + edicion.AGREGAR_CEROS_LEFT(fechaCredito.getMonthValue(), 2) + "-"
     + fechaCredito.getYear();
     JD_FechaPago.setDate(edicion.CambiarTipoFecha(fecha_de_pago));
     }*/
    private boolean VALIDAR_FML_PROG_CTA_INICIAL() {
        if (JD_FechaPago.getDate() == null) {
            edicion.mensajes(1, "por favor selecciona la fecha de pago de la cuota inicial.");
            JD_FechaPago.grabFocus();
            return false;
        }
        if (JD_FechaPago.getDate().before(FECHA_CREDITO)) {
            edicion.mensajes(1, "la fecha de pago no puede ser menor a la fecha del credito.");
            txtValorCtaInicial.grabFocus();
            return false;
        }
        return true;
    }
}
