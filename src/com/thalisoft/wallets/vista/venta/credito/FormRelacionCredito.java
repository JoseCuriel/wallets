package com.thalisoft.wallets.vista.venta.credito;

import com.thalisoft.wallets.model.venta.credito.CreditoDao;
import com.thalisoft.wallets.util.DateUtil;
import com.thalisoft.wallets.util.Edicion;
import com.thalisoft.wallets.util.Variables_Gloabales;
import com.thalisoft.wallets.util.report.Manager_Report;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ThaliSoft
 */
public class FormRelacionCredito extends javax.swing.JInternalFrame {

    CreditoDao creditoDao;
    Edicion edicion = new Edicion();
    private TableRowSorter trsFiltro;
    Manager_Report report = new Manager_Report();

    public FormRelacionCredito() {
        creditoDao = new CreditoDao();
        initComponents();
        Edicion.CENTRAR_VENTANA(this);
        JD_FECHA1.setDate(DateUtil.newDateTime());
        JD_FECHA2.setDate(DateUtil.newDateTime());
        FILTRAR_DATOS_RELACION();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JD_FECHA1 = new com.toedter.calendar.JDateChooser();
        JD_FECHA2 = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        txtFiltro = new javax.swing.JTextField();
        radioCliente = new javax.swing.JRadioButton();
        radioEstado = new javax.swing.JRadioButton();
        radioCobrador = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_RELACION_CREDITOS = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtTotalVrCredito = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtTotalVrCtaInicial = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtTotalVrSaldo = new javax.swing.JTextField();
        txtTotalVrComision = new javax.swing.JTextField();
        LB_CNT_CREDITO = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtTotalVrAbono = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setTitle("Relación de Creditos");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "RELACIÓN DE CREDITOS", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 1, 24))); // NOI18N

        jLabel1.setText("SELECCIONAR PERIODO ");

        JD_FECHA1.setDateFormatString("EEEE dd MMMM yyyy");

        JD_FECHA2.setDateFormatString("EEEE dd MMMM yyyy");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/search.png"))); // NOI18N
        jButton1.setText("CONSULTAR");
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/printer.png"))); // NOI18N
        jButton2.setText("IMPRIMIR");
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton3.setText("SALIR");
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("FILTRAR"));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtFiltro.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
        jPanel3.add(txtFiltro, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 270, 30));

        buttonGroup1.add(radioCliente);
        radioCliente.setText("CLIENTE");
        jPanel3.add(radioCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 16, -1, -1));

        buttonGroup1.add(radioEstado);
        radioEstado.setText("ESTADO DEL CREDITO");
        jPanel3.add(radioEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(75, 16, -1, -1));

        buttonGroup1.add(radioCobrador);
        radioCobrador.setText("COBRADOR");
        jPanel3.add(radioCobrador, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 16, -1, -1));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/logo_el_progreso_label.png"))); // NOI18N
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(96, 96, 96)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JD_FECHA1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JD_FECHA2, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(92, 92, 92))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(JD_FECHA2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(JD_FECHA1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton1))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
        );

        TB_RELACION_CREDITOS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No. CREDITO", "FECHA EMISIÓN", "PAGOS/CUOTAS", "MODO PAGO", "CLIENTE", "$ Vr CREDITO", "$ Vr CTA INICIAL", "$ Vr SALDO", "$ Vr COMISION", "$ Vr ABONOS", "COBRADOR", "ESTADO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_RELACION_CREDITOS.setRowHeight(23);
        TB_RELACION_CREDITOS.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(TB_RELACION_CREDITOS);
        if (TB_RELACION_CREDITOS.getColumnModel().getColumnCount() > 0) {
            TB_RELACION_CREDITOS.getColumnModel().getColumn(0).setMaxWidth(75);
            TB_RELACION_CREDITOS.getColumnModel().getColumn(2).setMinWidth(60);
            TB_RELACION_CREDITOS.getColumnModel().getColumn(3).setMaxWidth(70);
            TB_RELACION_CREDITOS.getColumnModel().getColumn(4).setMinWidth(250);
            TB_RELACION_CREDITOS.getColumnModel().getColumn(10).setMinWidth(150);
        }

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TOTALES", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 1, 16), new java.awt.Color(204, 153, 0))); // NOI18N
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setBackground(new java.awt.Color(255, 255, 0));
        jLabel11.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel11.setText(" TOTAL VALOR CREDITO");
        jLabel11.setOpaque(true);
        jPanel6.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 20, 210, 40));

        txtTotalVrCredito.setEditable(false);
        txtTotalVrCredito.setBackground(new java.awt.Color(255, 255, 0));
        txtTotalVrCredito.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        txtTotalVrCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalVrCredito.setText("$ 0");
        txtTotalVrCredito.setBorder(null);
        jPanel6.add(txtTotalVrCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 20, 220, 40));

        jLabel12.setBackground(new java.awt.Color(255, 153, 153));
        jLabel12.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("CANTIDAD DE CREDITOS");
        jLabel12.setOpaque(true);
        jPanel6.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 210, 60));

        jLabel13.setBackground(new java.awt.Color(255, 153, 0));
        jLabel13.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText(" TOTAL VR CTA INICIAL");
        jLabel13.setOpaque(true);
        jPanel6.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 60, 210, 40));

        jLabel14.setBackground(new java.awt.Color(255, 51, 51));
        jLabel14.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText(" TOTAL VALOR SALDO");
        jLabel14.setOpaque(true);
        jPanel6.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 20, 210, 40));

        txtTotalVrCtaInicial.setEditable(false);
        txtTotalVrCtaInicial.setBackground(new java.awt.Color(255, 153, 0));
        txtTotalVrCtaInicial.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        txtTotalVrCtaInicial.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalVrCtaInicial.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalVrCtaInicial.setText("$ 0");
        txtTotalVrCtaInicial.setBorder(null);
        jPanel6.add(txtTotalVrCtaInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 60, 220, 40));

        jLabel18.setBackground(new java.awt.Color(0, 204, 204));
        jLabel18.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel18.setText(" TOTAL VALOR COMISIÓN");
        jLabel18.setOpaque(true);
        jPanel6.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 60, 220, 40));

        txtTotalVrSaldo.setEditable(false);
        txtTotalVrSaldo.setBackground(new java.awt.Color(255, 51, 51));
        txtTotalVrSaldo.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        txtTotalVrSaldo.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalVrSaldo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalVrSaldo.setText("$ 0");
        txtTotalVrSaldo.setBorder(null);
        jPanel6.add(txtTotalVrSaldo, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 20, 220, 40));

        txtTotalVrComision.setEditable(false);
        txtTotalVrComision.setBackground(new java.awt.Color(0, 204, 204));
        txtTotalVrComision.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        txtTotalVrComision.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalVrComision.setText("$ 0");
        txtTotalVrComision.setBorder(null);
        jPanel6.add(txtTotalVrComision, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 60, 220, 40));

        LB_CNT_CREDITO.setBackground(new java.awt.Color(255, 153, 153));
        LB_CNT_CREDITO.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LB_CNT_CREDITO.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LB_CNT_CREDITO.setText("0");
        LB_CNT_CREDITO.setOpaque(true);
        jPanel6.add(LB_CNT_CREDITO, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 30, 70, 60));

        jLabel4.setBackground(new java.awt.Color(153, 0, 255));
        jLabel4.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText(" TOTAL VALOR ABONOS");
        jLabel4.setOpaque(true);
        jPanel6.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, 220, 40));

        txtTotalVrAbono.setEditable(false);
        txtTotalVrAbono.setBackground(new java.awt.Color(153, 0, 255));
        txtTotalVrAbono.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        txtTotalVrAbono.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalVrAbono.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalVrAbono.setText("$ 0");
        txtTotalVrAbono.setBorder(null);
        jPanel6.add(txtTotalVrAbono, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 100, 210, 40));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 1177, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1257, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        CARGAR_RELACION_DE_CREDITOS();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        CARGAR_RELACION_DE_CREDITOS();
        Object[] Parametros = {JD_FECHA1.getDate(), JD_FECHA2.getDate(),
             txtFiltro.getText() , Variables_Gloabales.EMPLEADO.getNombres() + " " + Variables_Gloabales.EMPLEADO.getApellidos()};
        report.RELACION_DE_CREDITOS(Parametros);
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser JD_FECHA1;
    private com.toedter.calendar.JDateChooser JD_FECHA2;
    private javax.swing.JLabel LB_CNT_CREDITO;
    private javax.swing.JTable TB_RELACION_CREDITOS;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton radioCliente;
    private javax.swing.JRadioButton radioCobrador;
    private javax.swing.JRadioButton radioEstado;
    private javax.swing.JTextField txtFiltro;
    private javax.swing.JTextField txtTotalVrAbono;
    private javax.swing.JTextField txtTotalVrComision;
    private javax.swing.JTextField txtTotalVrCredito;
    private javax.swing.JTextField txtTotalVrCtaInicial;
    private javax.swing.JTextField txtTotalVrSaldo;
    // End of variables declaration//GEN-END:variables

    private void CALCULAR_TOTALES_RELACION() {
        edicion.calcula_total(TB_RELACION_CREDITOS, LB_CNT_CREDITO, txtTotalVrCredito, 5);
        edicion.calcula_total(TB_RELACION_CREDITOS, LB_CNT_CREDITO, txtTotalVrCtaInicial, 6);
        edicion.calcula_total(TB_RELACION_CREDITOS, LB_CNT_CREDITO, txtTotalVrSaldo, 7);
        edicion.calcula_total(TB_RELACION_CREDITOS, LB_CNT_CREDITO, txtTotalVrComision, 8);
        edicion.calcula_total(TB_RELACION_CREDITOS, LB_CNT_CREDITO, txtTotalVrAbono, 9);

    }

    private void FILTRAR_DATOS_RELACION() {
        txtFiltro.addKeyListener(new KeyAdapter() {
            private int opcionFiltro;

            @Override
            public void keyReleased(final KeyEvent e) {
                String cadenafiltra = (txtFiltro.getText());
                txtFiltro.setText(cadenafiltra.toUpperCase());
                repaint();
                filtro();
            }

            private void filtro() {
                if (radioCliente.isSelected()) {
                    opcionFiltro = 4;
                }
                if (radioCobrador.isSelected()) {
                    opcionFiltro = 10;
                }
                if (radioEstado.isSelected()) {
                    opcionFiltro = 11;
                }
                if (radioCliente.isSelected() | radioCobrador.isSelected() | radioEstado.isSelected()) {
                    trsFiltro.setRowFilter(RowFilter.regexFilter(txtFiltro.getText(), opcionFiltro));
                    LB_CNT_CREDITO.setText("" + TB_RELACION_CREDITOS.getRowCount());
                    CALCULAR_TOTALES_RELACION();
                } else {
                    edicion.mensajes(1, "selecciona una opcion para filtrar.");
                }
            }
        });
        trsFiltro = new TableRowSorter(TB_RELACION_CREDITOS.getModel());
        TB_RELACION_CREDITOS.setRowSorter(trsFiltro);
        txtFiltro.requestFocus();
    }

    private void CARGAR_RELACION_DE_CREDITOS() {
        Object key = "1,'" + edicion.formatearFechaSQL(JD_FECHA1.getDate()) + "','" + edicion.formatearFechaSQL(JD_FECHA2.getDate()) + "',0";
        edicion.llenarTabla(TB_RELACION_CREDITOS, creditoDao.RELACION_DE_CREDITOS(key));
        CALCULAR_TOTALES_RELACION();
    }
}
