SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `db_wallets` DEFAULT CHARACTER SET utf8 ;
USE `db_wallets` ;

-- -----------------------------------------------------
-- Table `db_wallets`.`empleados`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`empleados` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`empleados` (
  `idusuarios` INT(11) NOT NULL AUTO_INCREMENT,
  `identificacion` CHAR(20) NOT NULL,
  `apellidos` VARCHAR(45) NULL,
  `nombres` VARCHAR(45) NULL DEFAULT NULL,
  `direccion` VARCHAR(45) NULL DEFAULT NULL,
  `telefono` VARCHAR(45) NULL DEFAULT NULL,
  `pasword` TEXT NULL DEFAULT NULL,
  `cargo` VARCHAR(45) NULL DEFAULT NULL,
  `salariobasico` INT(11) NULL DEFAULT NULL,
  `porcComision` INT NULL DEFAULT 0,
  `rol` INT(1) NULL DEFAULT NULL,
  `estado` INT(1) NULL DEFAULT NULL,
  `fechahoraingreso` DATETIME NULL DEFAULT NULL,
  `empleadoRegistra` VARCHAR(20) NULL,
  PRIMARY KEY (`idusuarios`, `identificacion`),
  INDEX `identificacion` (`identificacion` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 8;


-- -----------------------------------------------------
-- Table `db_wallets`.`categorias`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`categorias` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`categorias` (
  `idcategorias` INT(11) NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NULL DEFAULT NULL,
  `empleado` CHAR(20) NOT NULL,
  `fechahoraingreso` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`idcategorias`),
  INDEX `fk_categorias_empleados1_idx` (`empleado` ASC),
  CONSTRAINT `categorias_ibfk_1`
    FOREIGN KEY (`empleado`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `db_wallets`.`clientes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`clientes` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`clientes` (
  `idclientes` INT(11) NOT NULL AUTO_INCREMENT,
  `identificacion` CHAR(20) NOT NULL,
  `apellidos` VARCHAR(45) NULL DEFAULT NULL,
  `nombres` VARCHAR(45) NULL,
  `telefonoPrincipal` VARCHAR(20) NULL DEFAULT NULL,
  `telefonoSecundario` VARCHAR(20) NULL,
  `direccionPrincipal` VARCHAR(75) NULL DEFAULT NULL,
  `direccionSecundaria` VARCHAR(75) NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `nombreReferencia` VARCHAR(45) NULL,
  `telefonoReferencia` VARCHAR(45) NULL,
  `nombreConyugue` VARCHAR(45) NULL,
  `telefonoConyugue` VARCHAR(45) NULL,
  `fechahoraingreso` DATETIME NULL,
  `empleado` CHAR(20) NOT NULL,
  PRIMARY KEY (`idclientes`, `identificacion`),
  INDEX `fk_clientes_empleados1_idx` (`empleado` ASC),
  INDEX `identificacion` (`identificacion` ASC),
  CONSTRAINT `clientes_ibfk_1`
    FOREIGN KEY (`empleado`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 986
DELAY_KEY_WRITE = 1;


-- -----------------------------------------------------
-- Table `db_wallets`.`empresas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`empresas` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`empresas` (
  `Nit` CHAR(30) NOT NULL,
  `razonsocial` VARCHAR(50) NOT NULL,
  `Telefono` VARCHAR(20) NOT NULL,
  `Direccion` VARCHAR(50) NOT NULL,
  `mail` VARCHAR(100) NOT NULL,
  `gerente` CHAR(50) NOT NULL,
  `Eslogan` VARCHAR(100) NOT NULL,
  `Ciudad` CHAR(50) NOT NULL,
  `celular` VARCHAR(30) NOT NULL,
  `estado` INT(1) NOT NULL DEFAULT '1',
  `redsocial` VARCHAR(255) NULL,
  `idEmpresa` INT NOT NULL AUTO_INCREMENT,
  `msjDocumentos` VARCHAR(70) NULL,
  `empleadoRegistra` CHAR(20) NOT NULL,
  PRIMARY KEY (`idEmpresa`, `Nit`),
  INDEX `fk_empresas_empleados1_idx` (`empleadoRegistra` ASC),
  CONSTRAINT `fk_empresas_empleados1`
    FOREIGN KEY (`empleadoRegistra`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_wallets`.`subcategorias`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`subcategorias` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`subcategorias` (
  `idsubcategorias` INT NOT NULL AUTO_INCREMENT,
  `DescripcionSubcategoria` VARCHAR(45) NULL,
  `categorias_idcategorias` INT(11) NOT NULL,
  INDEX `fk_subcategorias_categorias1_idx` (`categorias_idcategorias` ASC),
  PRIMARY KEY (`idsubcategorias`),
  CONSTRAINT `fk_subcategorias_categorias1`
    FOREIGN KEY (`categorias_idcategorias`)
    REFERENCES `db_wallets`.`categorias` (`idcategorias`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_wallets`.`productos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`productos` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`productos` (
  `idproductos` INT(11) NOT NULL AUTO_INCREMENT,
  `referencia` VARCHAR(45) NOT NULL,
  `descripcion` TEXT NULL DEFAULT NULL,
  `stockactual` INT(11) NULL DEFAULT NULL,
  `costo` INT(11) NULL DEFAULT NULL,
  `precioventa` INT(11) NULL DEFAULT NULL,
  `utilidad` INT(11) NULL DEFAULT NULL,
  `totalvalorcuotainicial` INT NULL DEFAULT 0,
  `modelo` CHAR(50) NULL DEFAULT NULL,
  `color` VARCHAR(15) NULL DEFAULT NULL,
  `marca` VARCHAR(45) NULL DEFAULT NULL,
  `proveedor` INT(11) NULL,
  `categoria` VARCHAR(30) NULL DEFAULT NULL,
  `fechahoraingreso` DATETIME NULL DEFAULT NULL,
  `empleadoRegProducto` CHAR(20) NOT NULL,
  `subcategoria` INT NOT NULL,
  PRIMARY KEY (`idproductos`, `referencia`, `empleadoRegProducto`),
  UNIQUE INDEX `referencia` USING HASH (`referencia` ASC),
  INDEX `fk_productos_proveedores_idx` (`proveedor` ASC),
  INDEX `fk_productos_categorias1_idx` (`categoria` ASC),
  INDEX `fk_productos_empleados1_idx` (`empleadoRegProducto` ASC),
  INDEX `fk_productos_subcategorias1_idx` (`subcategoria` ASC),
  CONSTRAINT `productos_ibfk_1`
    FOREIGN KEY (`empleadoRegProducto`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_productos_subcategorias1`
    FOREIGN KEY (`subcategoria`)
    REFERENCES `db_wallets`.`subcategorias` (`idsubcategorias`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 21141;


-- -----------------------------------------------------
-- Table `db_wallets`.`estadosLetra`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`estadosLetra` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`estadosLetra` (
  `idestadosLetra` INT NOT NULL AUTO_INCREMENT,
  `descripcionEstadoLetra` VARCHAR(45) NULL,
  PRIMARY KEY (`idestadosLetra`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_wallets`.`estadosCredito`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`estadosCredito` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`estadosCredito` (
  `idestadosCredito` INT NOT NULL AUTO_INCREMENT,
  `descripcionestadoCredito` VARCHAR(45) NULL,
  PRIMARY KEY (`idestadosCredito`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_wallets`.`creditos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`creditos` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`creditos` (
  `idcreditos` INT NOT NULL,
  `fechaemision` DATE NULL,
  `fechafinal` DATE NULL,
  `numeroCuota` INT NULL,
  `modalidadpago` VARCHAR(45) NULL,
  `totalvalorcredito` INT NULL,
  `totalvalorctainicial` INT NULL,
  `totalvalorsaldo` INT NULL,
  `totalvalorcomision` INT NULL,
  `fechahoraregistro` DATETIME NULL,
  `estadosCredito` INT NOT NULL,
  `clientes_identificacion` CHAR(20) NOT NULL,
  `empleadoCobrador` CHAR(20) NOT NULL,
  `empleadoRegistra` CHAR(20) NOT NULL,
  PRIMARY KEY (`idcreditos`),
  INDEX `fk_creditos_estadosCredito1_idx` (`estadosCredito` ASC),
  INDEX `fk_creditos_clientes1_idx` (`clientes_identificacion` ASC),
  INDEX `fk_creditos_empleados1_idx` (`empleadoCobrador` ASC),
  INDEX `fk_creditos_empleados2_idx` (`empleadoRegistra` ASC),
  CONSTRAINT `fk_creditos_estadosCredito1`
    FOREIGN KEY (`estadosCredito`)
    REFERENCES `db_wallets`.`estadosCredito` (`idestadosCredito`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_creditos_clientes1`
    FOREIGN KEY (`clientes_identificacion`)
    REFERENCES `db_wallets`.`clientes` (`identificacion`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_creditos_empleados1`
    FOREIGN KEY (`empleadoCobrador`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_creditos_empleados2`
    FOREIGN KEY (`empleadoRegistra`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_wallets`.`detalleCreditos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`detalleCreditos` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`detalleCreditos` (
  `iddetalleCreditos` INT NOT NULL AUTO_INCREMENT,
  `cantidad` INT NULL,
  `costounidad` INT NULL,
  `precioventaund` INT NULL,
  `utilidadund` INT NULL,
  `comisionund` INT NULL,
  `totalcosto` INT NULL,
  `totalventa` INT NULL,
  `totalutilidad` INT NULL,
  `totalcomision` INT NULL,
  `fechahoraregistro` DATETIME NULL,
  `credito` INT NOT NULL,
  `producto` VARCHAR(45) NOT NULL,
  `empleadoregistra` CHAR(20) NOT NULL,
  PRIMARY KEY (`iddetalleCreditos`),
  INDEX `fk_detalleCreditos_creditos1_idx` (`credito` ASC),
  INDEX `fk_detalleCreditos_productos1_idx` (`producto` ASC),
  INDEX `fk_detalleCreditos_empleados1_idx` (`empleadoregistra` ASC),
  CONSTRAINT `fk_detalleCreditos_creditos1`
    FOREIGN KEY (`credito`)
    REFERENCES `db_wallets`.`creditos` (`idcreditos`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_detalleCreditos_productos1`
    FOREIGN KEY (`producto`)
    REFERENCES `db_wallets`.`productos` (`referencia`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_detalleCreditos_empleados1`
    FOREIGN KEY (`empleadoregistra`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_wallets`.`detalleLetras`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`detalleLetras` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`detalleLetras` (
  `iddetalleLetras` INT NOT NULL AUTO_INCREMENT,
  `numeroLetra` INT NULL,
  `fechaPago` DATE NULL,
  `totalvalorletra` INT NULL,
  `totalvalorsaldoletra` INT NULL,
  `fechahoraregistro` DATETIME NULL,
  `credito` INT NOT NULL,
  `empleadoregistra` CHAR(20) NOT NULL,
  `estadosLetra` INT NOT NULL,
  PRIMARY KEY (`iddetalleLetras`),
  INDEX `fk_detalleLetras_creditos1_idx` (`credito` ASC),
  INDEX `fk_detalleLetras_empleados1_idx` (`empleadoregistra` ASC),
  INDEX `fk_detalleLetras_estadosLetra1_idx` (`estadosLetra` ASC),
  CONSTRAINT `fk_detalleLetras_creditos1`
    FOREIGN KEY (`credito`)
    REFERENCES `db_wallets`.`creditos` (`idcreditos`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_detalleLetras_empleados1`
    FOREIGN KEY (`empleadoregistra`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_detalleLetras_estadosLetra1`
    FOREIGN KEY (`estadosLetra`)
    REFERENCES `db_wallets`.`estadosLetra` (`idestadosLetra`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_wallets`.`pagosClientes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`pagosClientes` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`pagosClientes` (
  `idpagosClientes` INT NOT NULL AUTO_INCREMENT,
  `fechaemision` DATE NULL,
  `totalvalorabono` INT NULL,
  `totalvalorsaldo` INT NULL,
  `totalvalorcomision` INT NULL,
  `totalefectivorecibido` INT NULL,
  `totalefectivoencaja` INT NULL,
  `totalvalortarjeta` INT NULL,
  `totalvalorcheque` INT NULL,
  `totalvalorcambio` INT NULL,
  `mediopago` VARCHAR(45) NULL,
  `numerorecibo` VARCHAR(45) NULL,
  `fechahoraregistro` DATETIME NULL,
  `Letra` INT NOT NULL,
  `empleadoRegistra` CHAR(20) NOT NULL,
  PRIMARY KEY (`idpagosClientes`),
  INDEX `fk_pagosClientes_detalleLetras1_idx` (`Letra` ASC),
  INDEX `fk_pagosClientes_empleados1_idx` (`empleadoRegistra` ASC),
  CONSTRAINT `fk_pagosClientes_detalleLetras1`
    FOREIGN KEY (`Letra`)
    REFERENCES `db_wallets`.`detalleLetras` (`iddetalleLetras`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pagosClientes_empleados1`
    FOREIGN KEY (`empleadoRegistra`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_wallets`.`facturasVentas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`facturasVentas` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`facturasVentas` (
  `numerofactura` INT NOT NULL,
  `fechafactura` DATE NULL,
  `mediopago` VARCHAR(45) NULL,
  `numerorecibo` VARCHAR(45) NULL,
  `totalvalorfactura` INT NULL,
  `totalvalorefectivo` INT NULL,
  `totalvalortarjeta` INT NULL,
  `totalvalorencaja` INT NULL,
  `totalvalorcambio` INT NULL,
  `fechahoraregistro` DATETIME NULL,
  `cliente` CHAR(20) NOT NULL,
  `empleadoRegistra` CHAR(20) NOT NULL,
  PRIMARY KEY (`numerofactura`),
  INDEX `fk_facturasVentas_clientes1_idx` (`cliente` ASC),
  INDEX `fk_facturasVentas_empleados1_idx` (`empleadoRegistra` ASC),
  CONSTRAINT `fk_facturasVentas_clientes1`
    FOREIGN KEY (`cliente`)
    REFERENCES `db_wallets`.`clientes` (`identificacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_facturasVentas_empleados1`
    FOREIGN KEY (`empleadoRegistra`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_wallets`.`detalleVentas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_wallets`.`detalleVentas` ;

CREATE TABLE IF NOT EXISTS `db_wallets`.`detalleVentas` (
  `iddetalleVentas` INT NOT NULL AUTO_INCREMENT,
  `cantidad` INT NULL,
  `totalcostounidad` INT NULL,
  `totalprecioventaund` INT NULL,
  `totalutilidadund` INT NULL,
  `totalcosto` INT NULL,
  `totalventa` INT NULL,
  `totalutilidad` INT NULL,
  `fechahoraregistro` DATETIME NULL,
  `facturasVenta` INT NOT NULL,
  `empleadoRegistra` CHAR(20) NOT NULL,
  PRIMARY KEY (`iddetalleVentas`),
  INDEX `fk_detalleVentas_facturasVentas1_idx` (`facturasVenta` ASC),
  INDEX `fk_detalleVentas_empleados1_idx` (`empleadoRegistra` ASC),
  CONSTRAINT `fk_detalleVentas_facturasVentas1`
    FOREIGN KEY (`facturasVenta`)
    REFERENCES `db_wallets`.`facturasVentas` (`numerofactura`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_detalleVentas_empleados1`
    FOREIGN KEY (`empleadoRegistra`)
    REFERENCES `db_wallets`.`empleados` (`identificacion`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
